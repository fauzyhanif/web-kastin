$(function () {
    // format rupiah
    $(".rupiah").mask("000.000.000", {reverse:true});

    // datatable
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});

// function select
function select2() {
    $('.select2').css('width', '100%');;
    $('.select2').select2();
}

// clone form item paket
$(".btn-add-item").click(function() {
    console.log("test")
    var form = $(".form-content:last").clone(true).addClass("after-clone").appendTo(".form-wrapper");
    form.find(".btn-delete-item").css("display", "");
    form.find('select').val("");
    form.find('input').val("");
});

// remove form item paket
$(".btn-delete-item").click(function(e) {
    $(this).closest(".form-content").remove();
});

// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var url_reload = "{{ url('m_menu_paket/index/1') }}";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#modal-create').modal('hide');
                $('#modal-update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }

                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });

                clearForm(form);

                $(".after-clone").remove();
            }
        });

        e.preventDefault();
    });
})();

// clear form
function clearForm(form) {
    $(form).closest('form').find("input[type=text], select").val("");
}

// show form update
function formUpdate(idPaket, nmPaket, harga, hargaModal,idUnit) {
    var form = $('form').attr('action', '{{ url('m_menu_paket/update/') }}' + idPaket);
    form.find('[name="id_unit"]').val(idUnit);
    form.find('[name="nm_paket"]').val(nmPaket);
    form.find('[name="harga"]').val(harga);
    form.find('[name="harga_modal"]').val(hargaModal);

    $.ajax({
        type: 'GET',
        url: '{{ url('m_menu_paket/getItem/') }}' + idPaket,
        dataType:'html',
        success: function(response){
            $('.list-view-item').html(response);
        }
    });
}

// show form delete paket
function formDelete(idPaket, nmPaket) {
    $('input#id').val(idPaket);
    $('#nama').text(nmPaket);
}

// delete item on paket
function deleteItemPaket(idPaket) {
    var urel = '{{ url("m_menu_paket/deleteItem") }}/' + idPaket;
    (new PNotify({
        title: 'Konfirmasi',
        text: 'Apakah Anda Yakin menghapus item ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: urel,
            success: function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });

                $('div[data-id="'+ idPaket + '"]').fadeOut();
            }
        });
    }).on('pnotify.cancel', function () {
        console.log('batal');
    });
}