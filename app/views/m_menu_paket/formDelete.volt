<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hapus Menu Paket</h4>
            </div>
        
            <form method="POST" action="{{ url('M_menu_paket/delete') }}" data-remote="data-remote">
                <input type="hidden" name="id_paket" id="id">
                <div class="modal-body">
                    <div class="form-group">
                        <p>Anda yakin ingin menghapus menu paket <b id="nama"></b>?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Batal
                    </button>
                    <button type="submit" class="btn btn-danger">
                        Hapus
                    </button>
                </div>
            </form>
        
        </div>
    </div>
</div>