<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Form Tambah Menu Paket</h4>
            </div>

            <form method="POST" action="{{ url('M_menu_paket/create') }}" data-remote="data-remote">
                <input type="hidden" name="id_unit" value="{{ id_unit }}">
                <div class="modal-body col-md-12">
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            <label>Nama Paket</label>
                            <input type="text" class="form-control" name="nm_paket">
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label>Harga Modal</label>
                            <input type="text" class="form-control rupiah text-right" name="harga_modal">
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label>Harga Jual</label>
                            <input type="text" class="form-control rupiah text-right" name="harga">
                        </div>
                    </div>

                    <div class="form-wrapper col-md-12">
                        <label class="col-md-8">Item</label>
                        <label class="col-md-4">Tambah / Hapus</label>

                        <div class="form-content">
                            <div class="col-md-8" style="margin-bottom: 10px;">
                                <select class="form-control" name="id_menu[]">
                                    <option value="">** Pilih Item</option>
                                    {% for x in dt_menu %}
                                    <option value="{{ x.id }}">{{ x.nama }}</option>
                                    {% endfor %}
                                </select>
                            </div>

                            <div class="col-md-4">
                                <button type="button" class="btn btn-default btn-add-item">
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-delete-item" style="display: none;">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">
                        Batal
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>