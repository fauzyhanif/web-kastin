<!-- Header content -->
<section class="content-header">
    <h1>
        Manajemen Paket
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Manajemen Paket</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Menu Paket</h3>
                    <div class="pull-right">
                        <button class="btn btn-success" data-toggle="modal" data-target="#modal-create">
                            <i class="fa fa-plus-circle"></i> Tambah Paket
                        </button>
                    </div>
                </div>
                <div class="box-body" id="list-view">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Nama Paket</th>
                                <th class="text-right" width="10%">Harga</th>
                                <th class="text-center" width="5%">Aktif</th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %}
                            {% for x in dt_paket %}
                            <tr>
                                <td>{{ no }}.</td>
                                <td>{{ x.nm_paket }}</td>
                                <td class="text-right">{{ this.helper.formatRupiah(x.harga) }}</td>
                                <td class="text-center">
                                    {% if x.aktif is 'Y' %}
                                    <span class="badge bg-green">{{ x.aktif }}</span>
                                    {% else %} 
                                    <span class="badge bg-red">{{ x.aktif }}</span>
                                    {% endif %}
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-update" onclick="formUpdate('{{ x.id_paket }}', '{{ x.nm_paket }}', '{{ x.harga }}', '{{ x.harga_modal }}', '{{ x.id_unit }}')">
                                        <i class="fa fa-edit"></i> Edit
                                    </button>
                                    
                                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete" onclick="formDelete('{{ x.id_paket }}', '{{ x.nm_paket }}')">
                                        <i class="fa fa-trash"></i> Hapus
                                    </button>
                                </td>
                            </tr>
                            {% set no = no + 1 %}
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
{% include "m_menu_paket/formCreate.volt" %}
{% include "m_menu_paket/formUpdate.volt" %}
{% include "m_menu_paket/formDelete.volt" %}

<!-- include js file -->
<script>{% include "m_menu_paket/assets/main.js" %}</script>