<?php if (count($dt_paket_item) > 0) { ?>
    {% for pkt in dt_paket_item %}
    <div data-id="{{ pkt.id_paket_item }}">
        <div class="col-md-8" style="margin-bottom: 10px;">
            <select class="form-control" name="id_menu[{{ pkt.id_paket_item }}]">
                <option value="">** Pilih Item</option>
                {% for x in dt_menu %}
                <option value="{{ x.id }}" {% if pkt.id_menu is x.id %} selected {% endif %}>{{ x.nama }}</option>
                {% endfor %}
            </select>
        </div>

        <div class="col-md-4">
            <button type="button" class="btn btn-danger btn-delete-item" onclick="deleteItemPaket('{{ pkt.id_paket_item }}')">
                <i class="fa fa-trash"></i>
            </button>
        </div>
    </div>
    {% endfor %}
<?php } else { ?>
    <div>
        <div class="col-md-8" style="margin-bottom: 10px;">
            -
        </div>

        <div class="col-md-4">
            -
        </div>
    </div>
<?php } ?>
