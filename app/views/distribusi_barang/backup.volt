<div class="form-wrapper col-md-12" style="margin-bottom: 20px;">
    <label class="col-md-6">Item</label>
    <label class="col-md-3">Jumlah</label>
    <label class="col-md-3">Tambah / Hapus</label>

    <div class="form-content col-md-12">
        <div class="col-md-6" style="margin-bottom: 10px;">
            <select name="id_menu[]" class="form-control" required>
                <option value="">** Pilih Barang</option>
                {% for x in dt_barang %}
                <option value="{{ x.id }}" <?php if($x->stok <= 0){ ?> disabled="" style="background-color: #EC644B; color: white" <?php } ?>>{{ x.nama }} (stok : {{ x.stok }})</option>
                {% endfor %}
            </select>
        </div>
        
        <div class="col-md-3">
            <input type="number" class="form-control" name="stok[]" value="0" required>
        </div>
        
        <div class="col-md-3">
            <button type="button" class="btn btn-success btn-add-item">
                <i class="fa fa-plus-circle"></i>
            </button>
            <button type="button" class="btn btn-danger btn-delete-item" style="display: none;">
                <i class="fa fa-trash"></i>
            </button>
        </div>
    </div>
</div>  