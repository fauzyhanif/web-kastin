<table class="table table-striped data-table">
    <thead>
        <tr>
            <th width="15%">Tanggal</th>
            <th width="15%">Kelas</th>
            <th>Barang</th>
            <th width="10%">Stok</th>
            <th width="15%">Admin</th>
        </tr>
    </thead>
    <tbody>
        {% set no = 1 %} {% for x in dt_history %}
        <tr>
            <td>{{ this.helper.dateBahasaIndo(x.created_at) }}</td>
            <td>{{ x.nm_kelas }}</td>
            <td>{{ x.nm_menu }}</td>
            <td>{{ x.stok }}</td>
            <td>{{ x.admin }}</td>
        </tr>
        {% set no = no + 1 %} {% endfor %}
    </tbody>
</table>