$(function () {
	$('#daterangepicker').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });

	$('.data-table').DataTable( {
		"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
	});

});

// clone form item paket
$(".btn-add-item").click(function() {
    console.log("test")
    var form = $(".form-content:last").clone(true).addClass("after-clone").appendTo(".form-wrapper");
    form.find(".btn-delete-item").css("display", "");
    form.find('select').val("");
    form.find('input').val("");
});

// remove form item paket
$(".btn-delete-item").click(function(e) {
    $(this).closest(".form-content").remove();
});

// SAVE / UPDATE / DELETE
(function() {

    $('form[name="filter"]').on('submit', function(e) {
        var form      	= $(this);
        var url       	= form.prop('action');
        var data = {
            "created_at" : $('input[name="created_at"]').val(),
	        "id_kelas" : $('select[name="id_kelas"]').val()
	    };

        $.ajax({
            type: 'POST',
            url: url,
		    dataType: "html",
            data: data,
            success: function(response){
                $('#list-view').html(response);
            }	
        });

        e.preventDefault();
    });

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var url_reload = "{{ url('distribusi_barang/createDistribusi') }}";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#modal-create').modal('hide');
                $('#modal-update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }

                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });

                $(".after-clone").remove();
            }
        });

        e.preventDefault();
    });
})();

// clear form
function clearForm(form) {
    $(form).closest('form').find("input[type=text], select").val("");
}

$('input[name="jumlah_dus"]').keyup(function() {
    var content            = $(this).closest(".form-content");
    var jumlah_dus         = content.find('input[name="jumlah_dus"]').val();
    var isi_dus            = content.find('input[name="isi_dus"]').val();
    var qty                = 0;

    if (jumlah_dus <= 0) {
        content.find("input[name='stok[]']").val(isi_dus);
    } else {
        qty = isi_dus * jumlah_dus;
        content.find("input[name='stok[]']").val(qty);
    }

});

$('input[name="isi_dus"]').keyup(function() {
    var content            = $(this).closest(".form-content");
    var jumlah_dus         = content.find('input[name="jumlah_dus"]').val();
    var isi_dus            = content.find('input[name="isi_dus"]').val();
    var qty                = 0;

    if (jumlah_dus <= 0) {
        content.find("input[name='stok[]']").val(isi_dus);
    } else {
        qty = isi_dus * jumlah_dus;
        content.find("input[name='stok[]']").val(qty);
    }

});
