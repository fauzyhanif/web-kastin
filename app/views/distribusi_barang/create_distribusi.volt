<!-- Header content -->
<section class="content-header">
    <h1>
        Distribusi Barang
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li>Distribusi Barang</li>
        <li class="active">Create Distribusi</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Menu</h3>
                </div>
                <!-- /.box-header -->
                <form id="form_input" method="POST" action="{{ url('distribusi_barang/createDistribusi') }}" data-remote>
                    <div class="box-body pad">
                        
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label>Kelas</label>
                                <select name="id_kelas" class="form-control" required>
                                    <option value="">** Pilih Kelas</option>
                                    {% for x in dt_kelas %}
                                    <option value="{{ x.id }}">{{ x.nama }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>

                        <div class="form-wrapper col-md-12" style="margin-bottom: 20px;">
                            <label class="col-md-4">Item</label>
                            <label class="col-md-2">Jml Dus</label>
                            <label class="col-md-2">Isi per Dus</label>
                            <label class="col-md-2">Jml Barang</label>
                            <label class="col-md-2">Tambah / Hapus</label>
                        
                            <div class="form-content col-md-12">
                                <div class="col-md-4" style="margin-bottom: 10px;">
                                    <select name="id_menu[]" class="form-control" required>
                                        <option value="">** Pilih Barang</option>
                                        {% for x in dt_barang %}
                                        <option value="{{ x.id }}" <?php if($x->stok <= 0){ ?> disabled="" style="background-color: #EC644B; color: white" <?php } ?>>{{ x.nama }} (stok : {{ x.stok }})</option>
                                        {% endfor %}
                                    </select>
                                </div>
                                
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="jumlah_dus" value="0" required>
                                </div>
                                
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="isi_dus" value="0" required>
                                </div>
                                
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="stok[]" value="0" required>
                                </div>
                                
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-add-item">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-delete-item" style="display: none;">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>                        
                        
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="reset" class="btn btn-default" onclick="return reload_page2('distribusi_barang/createDistribusi')">
                                    Batal
                                </button>
                                <button type="submit" class="btn btn-primary" id="submit">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>        
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>{% include "distribusi_barang/asset/main.js" %}</script>
