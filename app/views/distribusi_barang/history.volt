<!-- Header content -->
<section class="content-header">
    <h1>
        History Distribusi
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li>Distribusi Barang</li>
        <li class="active">History Distribusi</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <form name="filter" action="{{ url('distribusi_barang/filterHistory') }}" method="POST">
                        <div class="form-group col-md-4" style="margin-bottom: 0px;">
                            <input type="text" name="created_at" class="form-control" id="daterangepicker">
                        </div>
                        
                        <div class="form-group col-md-2" style="margin-bottom: 0px;">
                            <select name="id_kelas" class="form-control">
                                <option value="">** Pilih Kelas</option>
                                {% for x in dt_kelas %}
                                <option value="{{ x.id }}">{{ x.nama }}</option>
                                {% endfor %}
                            </select>
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-success btn-block">Filter</button>
                        </div>
                    </form>
                </div>
                <div class="box-body" id="list-view">
                    <table class="table table-striped data-table">
                        <thead>
                            <tr>
                                <th width="15%">Tanggal</th>
                                <th width="15%">Kelas</th>
                                <th>Barang</th>
                                <th width="10%">Stok</th>
                                <th width="15%">Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %} {% for x in dt_history %}
                            <tr>
                                <td>{{ this.helper.dateBahasaIndo(x.created_at) }}</td>
                                <td>{{ x.nm_kelas }}</td>
                                <td>{{ x.nm_menu }}</td>
                                <td>{{ x.stok }}</td>
                                <td>{{ x.admin }}</td>
                            </tr>
                            {% set no = no + 1 %} {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>{% include "distribusi_barang/asset/main.js" %}</script>