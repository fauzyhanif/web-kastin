$(function () {
    
    // get value datepicker
    var tgl_transaksi = $('#set_file_name_pdf').val();

	$('#daterangepicker').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });

	$('.data-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                },
                title: 'Detail Rekap Penjualan' + tgl_transaksi
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                },
                title: 'Detail Rekap Penjualan' + tgl_transaksi,
                customize : function(doc){
                    var colCount = new Array();
                    $('.data-table-1').find('tbody tr:first-child td').each(function(){
                        if($(this).attr('colspan')){
                            for(var i=1;i<=$(this).attr('colspan');$i++){
                                colCount.push('*');
                            }
                        }else{ colCount.push('*'); }
                    });
                    doc.content[1].table.widths = colCount;
                }
            }
        ],
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
    })
    $('.data-table-2').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2 ]
                },
                title: 'Rekap Penjualan ' + tgl_transaksi,

            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2 ]
                },
                title: 'Rekap Penjualan ' + tgl_transaksi,
                orientation:'potrait',
                customize : function(doc){
                    var colCount = new Array();
                    $('.data-table-2').find('tbody tr:first-child td').each(function(){
                        if($(this).attr('colspan')){
                            for(var i=1;i<=$(this).attr('colspan');$i++){
                                colCount.push('*');
                            }
                        }else{ colCount.push('*'); }
                    });
                    doc.content[1].table.widths = colCount;
                }
            }
        ],
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
	});

});

(function() {

    $('form[name="filter"]').on('submit', function(e) {
        var form      	= $(this);
        var url       	= form.prop('action');
        var data = {
	      "id_unit": $('input[name="id_unit"]').val(),
	      "tgl_transaksi": $('input[name="tgl_transaksi"]').val()
        };
        
        
        var dateAr          = $('input[name="tgl_transaksi"]').val().split(' - ');
        var dateAr1         = dateAr[0].split('-');
        var dateAr2         = dateAr[1].split('-');
        var date_start      = dateAr1[2] + '-' + dateAr1[1] + '-' + dateAr1[0];
        var date_end        = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];


        // set filename pdf
        $('#set_file_name_pdf').val($('input[name="tgl_transaksi"]').val());
        $('#tgl-filter').text(date_start + ' s/d ' + date_end);

        $.ajax({
            type: 'POST',
            url: url,
		    dataType: "html",
            data: data,
            success: function(response){
                $('#list-view').html(response);
            }	
        });

        e.preventDefault();
    });
})();

function detail(id_transaksi) {
    $("#id_transaksi").text("#" + id_transaksi);
    $.ajax({
        type: 'GET',
        url: '{{ url('list_penjualan/detail/') }}' + id_transaksi,
        dataType: "html",
        success: function(response){
            $('#view-detail-penjualan').html(response);
        }	
    });
}