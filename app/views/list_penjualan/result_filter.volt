<table class="table  table-striped data-table">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="20%">Waktu</th>
            <th width="20%">Kelas</th>
            <th width="15%">ID Transaksi</th>
            <th class="text-right" width="20%">Total Belanja</th>
            <th width="20%">Admin</th>
            <th width="5%">Action</th>
        </tr>
    </thead>
    <tbody>
        {% set no = 1 %}
        {% set total = 0 %}
        {% for x in dt_record %}
        <tr>
            <td>{{ no }}.</td>
            <td>{{ x.tgl_transaksi }}</td>
            <td>{{ x.nm_kelas }}</td>
            <td>{{ x.id_transaksi }}</td>
            <td class="text-right">{{ this.helper.formatRupiah(x.total) }}</td>
            <td>{{ x.nama }}</td>
            <td>
                <button 
                    type="button"
                    class="btn btn-primary btn-xs"
                    data-toggle="modal" 
                    data-target="#myModal"
                    onclick="detail('{{ x.id_transaksi }}')"
                >
                    <i class="fa fa-list-ul"></i> Detail
                </button>
            </td>
        </tr>
        {% set no = no + 1 %}
        {% set total = total + x.total %}
        {% endfor %}

        <tr>
            <td colspan="4" class="text-right"><h3><b>Total:</b></h3></td> 
            <td style="display : none;"></td>
            <td style="display : none;"></td>
            <td style="display : none;"></td>
            <td class="text-right"><h3><b>Rp. {{ this.helper.formatRupiah(total) }}</b></h3></td> 
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

<!-- include js file -->
<script>
$(function () {
    
    // get value datepicker
    var tgl_transaksi = $('#set_file_name_pdf').val();

	$('.data-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                },
                title: 'listpenjualan_' + tgl_transaksi
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                },
                title: 'listpenjualan_' + tgl_transaksi
            }
        ],
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
	});    

});

</script>