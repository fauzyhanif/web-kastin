<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Penjualan <b id="id_transaksi">#1290</b></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="view-detail-penjualan">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="55%">Item</th>
                                    <th width="10%">Qty</th>
                                    <th class="text-right" width="15%">Harga satuan</th>
                                    <th class="text-right" width="15%">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"></td>
                                    <td class="text-right"></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="4">Total</th>
                                    <th class="text-right">Rp. 0,-</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    
    </div>
</div>
