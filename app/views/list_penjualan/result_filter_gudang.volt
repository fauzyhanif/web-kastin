<table id="data_table" class="table table-striped data-table">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="20%">Kelas</th>
            <th class="text-right" width="20%">Total Belanja</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        {% set no = 1 %}
        {% set total = 0 %}
        {% for x in dt_record %}
        <tr>
            <td>{{ no }}.</td>
            <td>{{ x.nm_kelas }}</td>
            <td class="text-right">Rp. {{ this.helper.formatRupiah(x.total) }}</td>
            <td>
                <button type="button" class="btn btn-primary btn-xs" onclick="return go_page('list_penjualan/listPenjualanPerKantin/{{ x.kelas }}')">
                    <i class="fa fa-list-ul"></i> Detail
                </button>
            </td>
        </tr>
        {% set no = no + 1 %}
        {% set total = total + x.total %}
        {% endfor %}

        <tr>
            <td colspan="2" class="text-right"><h3><b>Total:</b></h3></td> 
            <td style="display : none;"></td>
            <td class="text-right"><h3><b>Rp. {{ this.helper.formatRupiah(total) }}</b></h3></td> 
            <td></td>
        </tr>
    </tbody>
</table>

<!-- include js file -->
<script>{% include "list_penjualan/assets/main.js" %}</script>