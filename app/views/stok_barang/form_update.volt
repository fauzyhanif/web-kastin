<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Menu Cafe <span id="text-nama-menu"></span></h4>
      </div>

      <form name="stok_barang" method="POST" data-remote="data-remote">
        <div class="modal-body">

          <div class="form-group">
            <label>Stok</label>
            <input type="number" name="stok" class="form-control" placeholder=" Stok"> 
          </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal">
            Batal
          </button>
          <button type="submit" class="btn btn-primary">
            Simpan Perubahan
          </button>
        </div>
      </form>

    </div>
  </div>
</div>