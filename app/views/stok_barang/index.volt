<!-- Header content -->
<section class="content-header">
    <h1>
        Stok Barang
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Stok Barang</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-sm-5">
                        <select class="form-control" onchange="filter(this.value)">
                            <option value="">** Filter Menu</option>
                            <option {% if jenis is 'Makanan' %} selected {% endif %}>Makanan</option>
                            <option {% if jenis is 'Minuman' %} selected {% endif %}>Minuman</option>
                            <option {% if jenis is 'Snack' %} selected {% endif %}>Snack</option>
                            <option {% if jenis is 'Tiket' %} selected {% endif %}>Tiket</option>
                        </select>
                    </div>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Nama Menu</th>
                                <th width="15%">Jenis</th>
                                <th width="10%">Stok</th>
                                <th class="text-right" width="15%">Harga</th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %} {% for x in dt_stok %}
                            <tr>
                                <td>{{ no }}.</td>
                                <td>{{ x.nama }}</td>
                                <td>{{ x.jenis }}</td>
                                <td>{{ x.stok }}</td>
                                <td class="text-right">{{ this.helper.formatRupiah(x.harga) }}</td>
                                <td> 
                                    <button class="btn btn-primary btn-xs" 
                                        onclick="edit_data('{{ x.id }}','{{ x.stok }}','{{ x.nama }}')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="fa fa-edit"></i> Edit Stok
                                    </button>
                                </td>
                            </tr>
                            {% set no = no + 1 %} {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
{% include "stok_barang/form_update.volt" %}

<!-- include js file -->
<script>{% include "stok_barang/asset/main.js" %}</script>