$(function () {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    $('#data_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'stokbarang_' + dd + mm + yyyy
            },
            {
                extend: 'pdfHtml5',
                title: 'stokbarang_' + dd + mm + yyyy
            }
        ],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});

function filter(jenis){
    var url     = "{{ url('stok_barang/index/') }}" + jenis;
    reload_page2(url);
}


function edit_data(id, stok, nama) {
    var form = $('form[name="stok_barang"]').attr('action', '{{ url('stok_barang/updateStok/') }}' + id);
    $('#text-nama-menu').text(nama);
    form.find('[name="stok"]').val(stok);
}

(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form        = $(this);
        var url         = form.prop('action');
        var url_reload  = "{{ url('stok_barang/index') }}";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();