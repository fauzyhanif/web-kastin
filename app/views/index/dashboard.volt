<div id="on-load">
  <section class="content-header">
    <h1>
      Dashboard
      <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#">Menu Utama</a></li>
      <li><a href="#">Dashboard</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    
    <!-- Summary order this day -->
    <div class="row">  
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Total Pendapatan Hari Ini</span>
            <span class="info-box-number" style="font-size:35px;">
              {% if dt_hari_ini[0].jml_pendapatan is not null %}
              Rp. {{ this.helper.formatRupiah(dt_hari_ini[0].jml_pendapatan) }}
              {% else %}
              Rp. 0
              {% endif %}
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-cart-plus"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Total Transaksi Hari Ini</span>
            <span class="info-box-number" style="font-size:35px;">
            {% if dt_hari_ini[0].jml_transaksi is not null %}
            {{dt_hari_ini[0].jml_transaksi}}
            {% else %}
            0
            {% endif %}
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-list"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Transaksi Belum Diproses Hari Ini</span>
            <span class="info-box-number" style="font-size:35px;">
            {% if dt_hari_ini[0].waiting_list is not null %}
            {{dt_hari_ini[0].waiting_list}}
            {% else %}
            0
            {% endif %}
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

    </div>
    <!-- End of Summary -->

    <!-- List order -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-default">

          <div class="box-header with-border" style="height : 46px;">
            <h3 class="box-title"><b>Antrian Order</b></h3>
            <div class="box-tools pull-right">
              <button onclick="return reload_page2('dashboard/home')" class="btn btn-primary">
                <i class="fa fa-refresh"></i>&nbsp;&nbsp; Refresh
              </button>
            </div>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>ID Order</th>
                  <th>Waktu</th>
                  <th>User</th>
                  <th class="text-right">Total Transaksi</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="list_view">
                  {% for x in dt_record %}
                  <tr>
                    <td>#{{ x.id_transaksi }}</td>
                    <td>{{ x.tgl_transaksi }}</td>
                    <td>{{ x.nama }}</td>
                    <td class="text-right">Rp. {{ this.helper.formatRupiah(x.total) }}</td>
                    {% if x.is_proses is 0 %}
                      <td><span class="label label-default">Belum diproses</span></td>
                    {% elseif x.is_proses is 1 %}
                      <td><span class="label label-primary">Sedang diproses</span></td>
                    {% else %}
                      <td><span class="label label-success">Sudah diproses</span></td>
                    {% endif %}
                    <td>
                      <button type="button" class="btn btn-info btn-xs" onclick="return go_page('dashboard/detail/{{ x.id_transaksi }}')">
                        <i class="fa fa-list"></i> Detail
                      </button>

                      {% if x.is_proses is 0 %}
                        <button class="btn btn-primary btn-xs" onclick="proses(1, '{{ x.id_transaksi }}')">
                          <i class="fa fa-check"></i> Proses
                        </button>
                      {% elseif x.is_proses is 1 %}
                        <button class="btn btn-success btn-xs" onclick="proses(2, '{{ x.id_transaksi }}')">
                          <i class="fa fa-check"></i> Selesai
                      </button>
                      {% else %}
                        <button class="btn btn-success btn-xs" disabled>
                          <i class="fa fa-check"></i> Selesai
                        </button>
                      {% endif %}
                      
                    </td>
                  </tr>
                  {% endfor %}
                
                </tbody>
              </table>
            </div>
          </div>
         
        </div>
      </div>
    </div>

  </section>
</div>



<script>{% include "index/assets/main.js" %}</script>
