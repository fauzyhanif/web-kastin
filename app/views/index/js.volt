<script type="text/javascript">
  new Chart(document.getElementById("salesChart1"), {
    type: 'bar',
    data: {
      labels: [
        <?php
          foreach ($this->helper->bulan() as $key => $value) {
          echo "'".$value."',";
        }
        ?>
      ],
      datasets: [
        {
          label: "Pengunjung",
          backgroundColor: "#3e95cd",
          data: [{% for x in data %}{{ x.p_bandungan }},{% endfor %}]
        }, {
          label: "Pendapatan",
          backgroundColor: "#8e5ea2",
          data: [{% for x in data %}<?= substr($x->bandungan,0,-3) ?>,{% endfor %}]
        }
      ]
    },
    options: {

    }
  });

  new Chart(document.getElementById("salesChart2"), {
    type: 'bar',
    data: {
      labels: [
        <?php
          foreach ($this->helper->bulan() as $key => $value) {
          echo "'".$value."',";
        }
        ?>
      ],
      datasets: [
        {
          label: "Pengunjung",
          backgroundColor: "#3e95cd",
          data: [{% for x in data %}{{ x.p_kopeng }},{% endfor %}]
        }, {
          label: "Pendapatan",
          backgroundColor: "#8e5ea2",
          data: [{% for x in data %}<?= substr($x->kopeng,0,-3) ?>,{% endfor %}]
        }
      ]
    },
    options: {

    }
  });

  function filter(thn) {
    var url = "{{ url('dashboard/home/') }}"+ thn;
    go_page(url);
  }

</script>