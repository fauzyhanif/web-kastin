{% for x in dt_record %}
<tr>
    <td>#{{ x.id_transaksi }}</td>
    <td>{{ x.tgl_transaksi }}</td>
    <td>{{ x.nama }}</td>
    <td class="text-right">Rp. {{ this.helper.formatRupiah(x.total) }}</td>
    {% if x.is_proses is 0 %}
        <td><span class="label label-default">Belum diproses</span></td>
    {% elseif x.is_proses is 1 %}
        <td><span class="label label-primary">Sedang diproses</span></td>
    {% else %}
        <td><span class="label label-success">Sudah diproses</span></td>
    {% endif %}
    <td>
        <button type="button" class="btn btn-info btn-xs" onclick="return go_page('dashboard/detail/{{ x.id_transaksi }}')">
            <i class="fa fa-list"></i> Detail
        </button>

        {% if x.is_proses is 0 %}
            <button class="btn btn-primary btn-xs" onclick="proses(1, '{{ x.id_transaksi }}')">
            <i class="fa fa-check"></i> Proses
            </button>
        {% elseif x.is_proses is 1 %}
            <button class="btn btn-success btn-xs" onclick="proses(2, '{{ x.id_transaksi }}')">
            <i class="fa fa-check"></i> Selesai
        </button>
        {% else %}
            <button class="btn btn-success btn-xs" disabled>
            <i class="fa fa-check"></i> Selesai
            </button>
        {% endif %}
    
    </td>
</tr>
{% endfor %}