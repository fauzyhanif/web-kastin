function detail(id_transaksi) {
    $("#id_transaksi").text("#" + id_transaksi);
    $.ajax({
        type: 'GET',
        url: '{{ url('list_penjualan/detail/') }}' + id_transaksi,
        dataType: "html",
        success: function(response){
            $('#view-detail-penjualan').html(response);
        }	
    });
}
  
// proses ubah status order
function proses(status, id_transaksi) {
    var data = {
        "is_proses": status,
        "id_transaksi": id_transaksi
    };
  
    $.ajax({
        type: 'POST',
        url: "{{ url('dashboard/proses') }}",
        dataType:'json',
        data: data,
        success: function(response){
    
            list_view();
    
        }
    })
}
  
// load view after proses
function list_view() {  
    $.ajax({
        type: 'GET',
        url: "{{ url('dashboard/listView') }}",
        dataType: 'html',
        success: function(response){
            $('#list_view').html(response)
        }
    })
}



