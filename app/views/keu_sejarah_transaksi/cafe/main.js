
$(function () {
	$(".select2").select2();
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable( {
        "paging": false,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
        dom: 'Bfrtip',
        buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        searching: true
    });

});

// get harga dri db
$('select').on('change', function() {
 	var content = $(this).closest(".content");
	$.ajax({
        type: 'POST',
        url: '{{ url('keu_sejarah_transaksi/hargaMenuCafe/') }}'+this.value,
        dataType:'json',
        data: "id="+this.value,
        success: function(data){
        	content.find(".harga_satuan").val(data);
        	qty 	= content.find(".qty").val();
        	hasil 	= qty * data;
        	content.find(".harga_total").val(hasil);
        	total();
        }
    });
});

$(function() {
    $('.harga_satuan').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.qty').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.harga_total').keyup(function() {
        total();
    });
});

function total(){
    var sum = 0;
    $(".harga_total").each(function(){
        sum += +$(this).val();
    });
    $("#total").text(sum);
    $("#hargaTotal").val(sum);
}

(function() {

    $('form[name="editTransaksi"]').on('submit', function(e) {
        var form        = $(this);
        var url         = form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var idTransaksi = $('input[name="id_transaksi"]').val();
        var urlReload   = '{{ url('keu_sejarah_transaksi/formEdit/') }}'+idUnit+"/7/"+idTransaksi;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });

    $('form[name="batalTransaksi"]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var urlReload 	= '{{ url('keu_sejarah_transaksi/index/') }}'+idUnit+"/7";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "{{ url('keu_sejarah_transaksi/index/') }}" + id_unit + "/7/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

// function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,hargaSatuan,qty,hargaDiskon,hargaTotal) {
// 	$('#idTransaksi').html(": "+idTransaksi);	
// 	$('#nmUser').html(": "+nmUser);
// 	$('#tglTransaksi').html(": "+tglTransaksi);
// 	$('#meja').html(": "+meja);
// 	$('#hargaDiskon').html(": Rp."+hargaDiskon);
// 	$('#hargaSatuan').html(": Rp."+hargaSatuan);
// 	$('#qty').html(": "+qty);
// 	$('#hargaTotal').html(": Rp."+hargaTotal);
// }
