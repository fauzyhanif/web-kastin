<style>{% include "keu_laporan_unit/assets/main.css" %}</style>
<input type="hidden" id="id_unit" value="{{ id_unit }}">
<!-- Header content -->
<section class="content-header">
  <h1>
    Edit Transaksi Souvenir
  </h1>
  <ol class="breadcrumb">
    <li>
      <button class="btn bg-navy btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/{{ id_unit }}/6')" style="margin-top: -11px;">
        <i class="fa  fa-chevron-circle-left"></i>
        Back
      </button>
    </li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header  with-border">
          <div class="col-md-12">
            <div class="col-md-3" style="font-size: 22px">Nomor Struk</div>
            <div class="col-md-4" style="font-size: 22px">: {{ data[0].a.id_transaksi }}.</div>
            <div class="col-md-2" style="font-size: 22px">Last update</div>
            <div class="col-md-3" style="font-size: 22px">: {{ data[0].a.updated_at }}.</div>

            <div class="col-md-3" style="font-size: 22px">Kasir</div>
            <div class="col-md-4" style="font-size: 22px">: {{ data[0].nm_user }}.</div>
            <div class="col-md-2" style="font-size: 22px">Oleh</div>
            <div class="col-md-3" style="font-size: 22px">: {{ data[0].user_update }}.</div>
            
            <div class="col-md-3" style="font-size: 22px">Tgl Transaksi</div>
            <div class="col-md-9" style="font-size: 22px">: {{ data[0].a.tgl_transaksi }}.</div>
          </div>
        </div>

        <div class="box-body" style="overflow: auto;" id="listView">
          <form name="editTransaksi" id="souvenir" method="POST" action="{{ url('keu_sejarah_transaksi/editTransaksi') }}">
            <input type="hidden" name="id_unit" value="{{ id_unit }}">
            <input type="hidden" name="id_transaksi" value="{{ data[0].a.id_transaksi }}">
            <input type="hidden" name="user_update" value="<?= $this->session->get('id_user') ?>">
            <input type="hidden" name="item" value="6">
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" width="5%">No</th>
                  <th class="text-center" width="30%">Item</th>
                  <th class="text-center" width="5%">Qty</th>
                  <th class="text-center" width="10%">Harga Satuan</th>
                  <th class="text-center" width="10%">Jumlah</th>
                </tr>
              </thead>
              <tbody>
                {% set total = 0 %}
                {% set no = 1 %}
                {% for x in dataRinci %}

                <tr class="content"> 
                  <input type="hidden" name="id_item_lama[]" value="{{ x.a.id_item }}">
                  <td class="text-center">{{ no }}.</td>
                  <td>
                    <select class="form-control" name="id_item[]">
                      {% for v in souvenir %}
                        <option value="{{ v.id }}" {% if x.a.id_item is v.id %} selected="" {% endif %}>{{ v.nama }}</option>
                      {% endfor %}
                    </select>
                  </td>
                  <td>
                    <input type="number" name="qty[]" class="form-control qty" value="{{ x.a.qty }}" style="width: 60px;">
                  </td>
                  <td class="text-right"> 
                    <input type="number" name="harga_satuan[]" class="form-control harga_satuan" value="{{ x.a.harga_satuan }}" style="width: 100px;">
                  </td>
                  <td class="text-right"> 
                    <input type="number" name="harga_total[]" class="form-control harga_total" value="{{ x.a.harga_total }}" style="width: 100px;">
                  </td>
                </tr>

                {% set total = total + x.a.harga_total %}
                {% set no = no + 1 %}
                {% endfor %}

              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-center"><h4><b>Total</b></h4></td>
                  <td class="text-right"><h4><b id="total">{{ total }}</b></h4></td>
                  <input type="hidden" name="total" id="hargaTotal" value="{{ total }}">
                </tr>
              </tfoot>
            </table>

            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Save Change</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<script>{% include "keu_sejarah_transaksi/souvenir/main.js" %}</script>

<!-- include popup -->
