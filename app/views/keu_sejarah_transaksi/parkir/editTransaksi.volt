<style>{% include "keu_laporan_unit/assets/main.css" %}</style>
<input type="hidden" id="id_unit" value="{{ id_unit }}">
<!-- Header content -->
<section class="content-header">
  <h1>
    Edit Transaksi Parkir
  </h1>
  <ol class="breadcrumb">
    <li>
      <button class="btn bg-navy btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/{{ id_unit }}/2')">
        <i class="fa  fa-chevron-circle-left"></i>
        Back
      </button>
    </li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-6 col-md-offset-3">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Edit Transaksi Parkir</h3>
        </div>

        <div class="box-body">
          <div class="col-md-12">
            <div class="col-md-4">Terakhir update</div>
            <div class="col-md-8">: {{ data[0].a.updated_at }}</div>
            <div class="col-md-4">Oleh</div>
            <div class="col-md-8">: {{ data[0].user_update }}</div>
            <hr>
            <form name="editTransaksi" id="parkir" method="POST" data-remote action="{{ url('keu_sejarah_transaksi/editTransaksi') }}">
              <input type="hidden" name="id_unit" value="{{ id_unit }}">
              <input type="hidden" name="id_transaksi" value="{{ data[0].a.id_transaksi }}">
              <input type="hidden" name="user_update" value="<?= $this->session->get('id_user') ?>">
              <input type="hidden" name="item" value="2">
              <div class="modal-body">
                <div class="form-group col-md-12">
                  <label>id Transaksi</label>
                  <input type="text" name="id_transaksi"  class="form-control" disabled="" id="idTransaksi" value="{{ data[0].a.id_transaksi }}">
                </div>

                <div class="form-group col-md-12">
                    <label>User</label>
                    <input type="text"  class="form-control" disabled="" id="nmUser" value="{{ data[0].nm_user }}"> 
                </div>

                <div class="form-group col-md-12">
                    <label>Tgl Transaksi</label>
                    <input type="text" class="form-control" disabled="" id="tglTransaksi" value="{{ data[0].a.tgl_transaksi }}"> 
                </div>

                <div class="form-group col-md-12">
                  <label>Item</label>
                  <select class="form-control" name="id_item">
                    {% for x in parkir %}

                      <option value="{{ x.id }}" {% if data[0].a.id_item is x.id %} selected="" {% endif %}>{{ x.nama }}</option>
                    {% endfor %}
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label>No Polisi</label>
                  <input type="text" name="no_polisi" class="form-control" id="no_polisi" value="{{ data[0].a.no_polisi }}">
                </div>

                <div class="form-group col-md-12">
                  <label>Quantity</label>
                  <input type="text" name="qty" class="form-control" id="qty" value="{{ data[0].a.qty }}">
                </div>

                <div class="form-group col-md-12">
                  <label>Harga Satuan</label>
                  <input type="text" name="harga_satuan" id="harga_satuan" class="form-control tarif" value="{{ data[0].a.harga_satuan }}">
                </div>

                <div class="form-group col-md-12">
                  <label>Total Harga</label>
                  <input type="text" name="harga_total" id="harga_total" class="form-control tarif" value="{{ data[0].a.harga_total }}">
                </div>
              </div>
              <div class="modal-footer" style="border-top: none;">
                <a class="btn btn-danger btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/{{ id_unit }}/2')">
                  <i class="fa fa-remove"></i> Cancel
                </a>
                <button type="submit" class="btn btn-primary btn-flat">
                <i class="fa fa-send"></i> Save Change
                </button>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>{% include "keu_sejarah_transaksi/parkir/main.js" %}</script>