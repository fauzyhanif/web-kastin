<style>{% include "keu_laporan_unit/assets/main.css" %}</style>
<input type="hidden" id="id_unit" value="{{ id_unit }}">
<!-- Header content -->
<section class="content-header">
  <h1>
    Sejarah Transaksi Parkir
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-edit"></i> Admin</a></li>
    <li class="active">Sejarah Transaksi</li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Data Transaksi Parkir</h3>
          <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-filter"></i> Filter
          </button>

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" role="document"">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Filter Transaksi</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                    </div> 
                  </div>

                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="box-body" style="overflow: auto;" id="listView">
          <table id="data_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" width="10%">Id Transaksi</th>
                <th class="text-center" width="10%">User</th>
                <th class="text-center" width="15%">Tgl Transaksi</th>
                <th class="text-center" width="20%">Item</th>
                <th class="text-center" width="5%">Qty</th>
                <th class="text-center" width="8%">Jumlah</th>
                <th class="text-center" width="9%">Action</th>
              </tr>
            </thead>
            <tbody>

              {% for x in parkir %}
              <tr>
                <td>{{ x.a.id_transaksi }}</td>
                <td>{{ x.nm_user }}</td>
                <td>{{ x.a.tgl_transaksi }}</td>
                <!-- <td>{{ this.helper.konversi_tgl(x.a.tgl_transaksi) }}</td> -->
                <td>{{ x.item }}</td>
                <td>{{ x.a.qty }}</td>
                <td class="text-right">{{ this.helper.formatRupiah(x.a.harga_total) }}</td>
                <td class="text-center">
                  <button class="btn btn-default btn-flat btn-xs" title="Detail" data-toggle="modal" data-target="#detailTransaksi" onclick="detailTransaksi('{{ id_unit }}','{{ x.a.id_transaksi }}','{{ x.nm_user }}','{{ x.a.tgl_transaksi }}','{{ x.item }}','{{ x.a.no_polisi }}','{{ this.helper.formatRupiah(x.a.harga_satuan) }}','{{ x.a.qty }}','{{ this.helper.formatRupiah(x.a.harga_total) }}')">
                    <i class="fa fa-bars"></i>
                  </button>

                  <button class="btn btn-primary btn-flat btn-xs" title="Edit"  onclick="return go_page('keu_sejarah_transaksi/formEdit/{{ id_unit }}/2/{{ x.a.id_transaksi }}')">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger btn-flat btn-xs" title="Batalkan Transaksi" data-toggle="modal" data-target="#batalTransaksi{{ x.a.id_transaksi }}" >
                    <i class="fa fa-trash"></i>
                  </button>

                  <div class="modal fade batalTransaksi" id="batalTransaksi{{ x.a.id_transaksi }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">Pembatalan Transaksi</h4>
                        </div>

                        <form name="batalTransaksi" method="POST" action="{{ url('keu_sejarah_transaksi/batalTransaksi') }}" data-remote>
                          <input type="hidden" name="id_unit" value="{{ id_unit }}">
                          <input type="hidden" name="id_transaksi" value="{{ x.a.id_transaksi }}">
                          <input type="hidden" name="item" value="2">
                          <input type="hidden" name="id_user" value="<?= $this->session->get('id_user') ?>">
                          <div class="modal-body">
                            <div class="form-group">
                              <label>Anda yakin ingin membatalkan transaksi dengan id transaksi <span id="id-transaksi">{{ x.a.id_transaksi }}</span>?</label>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-danger btn-flat" data-dismiss="modal">
                              <i class="fa fa-remove"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary btn-flat">
                            <i class="fa fa-trash"></i> Batalkan
                            </button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              {% endfor %}

            </tbody>
        </div>
      </div>
    </div>
  </div>
</section>

<script>{% include "keu_sejarah_transaksi/parkir/main.js" %}</script>

<!-- include popup -->
{% include "keu_sejarah_transaksi/parkir/detailTransaksi.volt" %}
