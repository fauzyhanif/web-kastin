<table class="table table-striped">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="55%">Item</th>
            <th width="10%">Qty</th>
            <th width="15%">Harga satuan</th>
            <th width="15%">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        
        
        <?php 
        $no = 1;
        $total = 0;
        for ($i=0; $i < count($dt_record); $i++) { ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $dt_record[$i]['nama']; ?></td>
                <td><?= $dt_record[$i]['qty']; ?> pcs</td>
                <td class="text-right"><?= $this->helper->formatRupiah($dt_record[$i]['harga_satuan']); ?></td>
                <td class="text-right"><?= $this->helper->formatRupiah($dt_record[$i]['harga_total']); ?></td>
            </tr>
        
        <?php 
        $no = $no + 1;
        $total = $total + $dt_record[$i]['harga_total'];
        } ?>

        
    </tbody>
    <tfoot>
        <tr>
            <th class="text-right" colspan="4">Total</th>
            <th class="text-right">Rp. {{ this.helper.formatRupiah(total) }},-</th>
        </tr>
    </tfoot>
</table>