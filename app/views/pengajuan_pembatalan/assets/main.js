$(function () {
	$('#daterangepicker').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });

	$('#data_table').DataTable( {
		"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
	});

});

(function() {

    $('form[name="filter"]').on('submit', function(e) {
        var form      	= $(this);
        var url       	= form.prop('action');
        var data = {
	      "id_unit": $('input[name="id_unit"]').val(),
	      "tgl_transaksi": $('input[name="tgl_transaksi"]').val()
	    };

        $.ajax({
            type: 'POST',
            url: url,
		    dataType: "html",
            data: data,
            success: function(response){
                $('#list-view').html(response);
            }	
        });

        e.preventDefault();
    });
})();

function detail(id_transaksi) {
    $("#id_transaksi").text("#" + id_transaksi);
    $.ajax({
        type: 'GET',
        url: '{{ url('list_penjualan/detail/') }}' + id_transaksi,
        dataType: "html",
        success: function(response){
            $('#view-detail-penjualan').html(response);
        }	
    });
}

// proses ubah status order
function proses(status, id_transaksi) {
    var data = {
        "stts_transaksi": status,
        "id_transaksi": id_transaksi
    };
  
    $.ajax({
        type: 'POST',
        url: "{{ url('pengajuan_pembatalan/proses') }}",
        dataType:'json',
        data: data,
        success: function(response){
    
            list_view();
            new PNotify({
                title: response.title,
                text: response.text,
                type: response.type
            });
    
        }
    })
}
  
// load view after proses
function list_view() {  
    $.ajax({
        type: 'GET',
        url: "{{ url('pengajuan_pembatalan/listView') }}",
        dataType: 'html',
        success: function(response){
            $('#list-view').html(response)
        }
    })
}