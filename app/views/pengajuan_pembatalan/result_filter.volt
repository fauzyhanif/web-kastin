<table id="data_table" class="table  table-striped">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="20%">Waktu</th>
            <th width="15%">ID Transaksi</th>
            <th width="10%">Total Belanja</th>
            <th width="20%">Admin</th>
            <th width="10%">Status</th>
            <th width="20%">Action</th>
            <th width="5%">Detail</th>
        </tr>
    </thead>
    <tbody>
        {% set no = 1 %}
        {% for x in dt_record %}
        <tr>
            <td>{{ no }}.</td>
            <td>{{ x.tgl_transaksi }}</td>
            <td>{{ x.id_transaksi }}</td>
            <td class="text-right">{{ this.helper.formatRupiah(x.total) }}</td>
            <td>{{ x.nama }}</td>
            {% if x.stts_transaksi is 1 %}
            <td><span class="label label-success">berhasil</span></td>
            {% elseif x.stts_transaksi is 2 %}
            <td><span class="label label-primary">Pengajuan pembatalan</span></td>
            {% else %}
            <td><span class="label label-danger">Sudah dibatalkan</span></td>
            {% endif %}
            <td>
                <button class="btn btn-default btn-xs" onclick="proses(1, '{{ x.id_transaksi }}')">
                    <i class="fa fa-close"></i> Urungkan
                </button>

                <button class="btn btn-danger btn-xs" onclick="proses(3, '{{ x.id_transaksi }}')">
                    <i class="fa fa-check"></i> Batalkan
                </button>
            </td>
            <td>
                <button 
                    type="button"
                    class="btn btn-info btn-xs"
                    data-toggle="modal" 
                    data-target="#myModal"
                    onclick="detail('{{ x.id_transaksi }}')"
                >
                    <i class="fa fa-list-ul"></i> Detail
                </button>
            </td>
        </tr>
        {% set no = no + 1 %}
        {% endfor %}
    </tbody>
</table>