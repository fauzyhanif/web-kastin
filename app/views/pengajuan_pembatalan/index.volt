<!-- Header content -->
<section class="content-header">
    <h1>
        Pengajuan Pembatalan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Pengajuan Pembatalan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <form name="filter" action="{{ url('pengajuan_pembatalan/filter') }}" method="POST">
                        <input type="hidden" name="id_unit" id="id_unit" value="{{ id_unit }}">
                        <div class="form-group col-md-4" style="margin-bottom: 0px;">
                            <input type="" name="tgl_transaksi" class="form-control" id="daterangepicker">
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-success btn-block">Filter</button>
                        </div>
                    </form>
                </div>
                <div class="box-body" id="list-view">
                    <table id="data_table" class="table  table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="20%">Waktu</th>
                                <th width="15%">ID Transaksi</th>
                                <th class="text-right" width="10%">Total Belanja</th>
                                <th width="20%">Admin</th>
                                <th width="10%">Status</th>
                                <th width="20%">Action</th>
                                <th width="5%">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %}
                            {% for x in dt_record %}
                            <tr>
                                <td>{{ no }}.</td>
                                <td>{{ x.tgl_transaksi }}</td>
                                <td>{{ x.id_transaksi }}</td>
                                <td class="text-right">{{ this.helper.formatRupiah(x.total) }}</td>
                                <td>{{ x.nama }}</td>
                                {% if x.stts_transaksi is 1 %}
                                <td><span class="label label-success">berhasil</span></td>
                                {% elseif x.stts_transaksi is 2 %}
                                <td><span class="label label-primary">Pengajuan pembatalan</span></td>
                                {% else %}
                                <td><span class="label label-danger">Sudah dibatalkan</span></td>
                                {% endif %}
                                <td>
                                    <button class="btn btn-default btn-xs" onclick="proses(1, '{{ x.id_transaksi }}')">
                                        <i class="fa fa-close"></i> Urungkan
                                    </button>

                                    <button class="btn btn-danger btn-xs" onclick="proses(3, '{{ x.id_transaksi }}')">
                                        <i class="fa fa-check"></i> Batalkan
                                    </button>
                                </td>
                                <td>
                                    <button 
                                        type="button"
                                        class="btn btn-info btn-xs"
                                        data-toggle="modal" 
                                        data-target="#myModal"
                                        onclick="detail('{{ x.id_transaksi }}')"
                                    >
                                        <i class="fa fa-list-ul"></i> Detail
                                    </button>
                                </td>
                            </tr>
                            {% set no = no + 1 %}
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->
{% include "pengajuan_pembatalan/modal_detail_pengajuan.volt" %}

<!-- include js file -->
<script>{% include "pengajuan_pembatalan/assets/main.js" %}</script>
    