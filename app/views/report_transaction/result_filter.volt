<table id="data_table" class="table table-striped data-table">
    <thead>
        <tr>
            <th width="5%">Tanggal</th>
            {% for x in data_header %}
            <th><?= date_format(date_create($x->tgl), "d-M"); ?></th>
            {% endfor %}
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        {% set grandTotal = [] %}
        {% for row_kelas in data_kelas %}
        {% set total = 0 %}
        <tr>
            <td>{{ row_kelas.nama }}</td>
            {% for x in data_header %}
                {% set nama = "" %}
                <td>Rp. 
                    {% for data in data_row %}
                    {% set dataCell = ((data.tgl == x.tgl and data.sesi == x.sesi and data.kelas == row_kelas.id) ? data.total : 0) %}
                    {{ dataCell != 0 ? this.helper.formatRupiah(data.total) : ''}}
                    {% set nama = (nama == "" and dataCell != 0) ? data.nama : nama %}
                    {% set total = total + dataCell %}
                    {% set grandTotal[x.tgl + x.sesi] = grandTotal[x.tgl + x.sesi] + ((data.tgl == x.tgl and data.sesi == x.sesi and data.kelas == row_kelas.id) ? data.total : 0) %}
                    {% endfor %}
                    {% if nama != "" %}
                    <b>( {{ nama }} )</b>
                    {% endif %}
                </td>
            {% endfor %}
            <td><b>Rp. {{ this.helper.formatRupiah(total) }}</b></td>
            {% set grandTotal['grandTotal'] = grandTotal['grandTotal'] + total %}
        </tr>
        {% endfor %}
        <tr>
            <td><b>Setoran</b></td>
            {% for x in data_header %}
                <td><b>Rp. {{ this.helper.formatRupiah(grandTotal[x.tgl + x.sesi]) }}</b></td>
            {% endfor %}
            <td><b>Rp. {{ this.helper.formatRupiah(grandTotal['grandTotal']) }}</b></td>
        </tr>
    </tbody>
</table>

<!-- include js file -->
<script>
$(function () {
    
    // get value datepicker
    var tgl_transaksi = $('#set_file_name_pdf').val();

	$('#daterangepicker').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });
    var lengtColumn = "{{str_column_count}}".split(",")
	$('.data-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: lengtColumn
                },
                title: 'Detail Rekap Penjualan' + tgl_transaksi
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: lengtColumn
                },
                title: 'Detail Rekap Penjualan' + tgl_transaksi,
                orientation:"landscape"
            }
        ],
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
    }) 

});

</script>