<input type="hidden" class="form-control" id="set_file_name_pdf" value="<?= date('Y_m_d').'-'.date('Y_m_d') ?>">
<!-- Header content -->
<section class="content-header">
    <h1>
        Laporan 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Laporan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    
                    <form name="filter" action="{{ url('report_transaction/filter') }}" method="POST">
                        <h3 class="box-title">
                            <p><i class="fa fa-calendar-minus-o"></i> Laporan tanggal <span id="tgl-filter"><?= date('d-m-Y') ?></span></p>
                        </h3>
                        <input type="hidden" name="id_unit" id="id_unit" value="{{ id_unit }}">
                        <div class="col-md-1 pull-right">
                            <button type="submit" class="btn btn-success btn-block">Filter</button>
                        </div>
                        <div class="form-group col-md-4 pull-right" style="margin-bottom: 0px;">
                            <input type="text" name="tgl_transaksi" class="form-control" id="daterangepicker">
                        </div>
                    </form>
                </div>
                <div class="box-body" id="list-view">
                    <table id="data_table" class="table table-striped data-table">
                        <thead>
                            <tr>
                                <th width="5%">Tanggal</th>
                                {% for x in data_header %}
                                <th><?= date_format(date_create($x->tgl), "d-M"); ?></th>
                                {% endfor %}
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set grandTotal = [] %}
                            {% for row_kelas in data_kelas %}
                            {% set total = 0 %}
                            <tr>
                                <td>{{ row_kelas.nama }}</td>
                                {% for x in data_header %}
                                    {% set nama = "" %}
                                    <td>Rp. 
                                        {% for data in data_row %}
                                        {% set dataCell = ((data.tgl == x.tgl and data.sesi == x.sesi and data.kelas == row_kelas.id) ? data.total : 0) %}
                                        {{ dataCell != 0 ? this.helper.formatRupiah(data.total) : ''}}
                                        {% set nama = (nama == "" and dataCell != 0) ? data.nama : nama %}
                                        {% set total = total + dataCell %}
                                        {% set grandTotal[x.tgl + x.sesi] = grandTotal[x.tgl + x.sesi] + ((data.tgl == x.tgl and data.sesi == x.sesi and data.kelas == row_kelas.id) ? data.total : 0) %}
                                        {% endfor %}
                                        {% if nama != "" %}
                                        <b>( {{ nama }} )</b>
                                        {% endif %}
                                    </td>
                                {% endfor %}
                                <td><b>Rp. {{ this.helper.formatRupiah(total) }}</b></td>
                                {% set grandTotal['grandTotal'] = grandTotal['grandTotal'] + total %}
                            </tr>
                            {% endfor %}
                            <tr>
                                <td><b>Setoran</b></td>
                                {% for x in data_header %}
                                    <td><b>Rp. {{ this.helper.formatRupiah(grandTotal[x.tgl + x.sesi]) }}</b></td>
                                {% endfor %}
                                <td><b>Rp. {{ this.helper.formatRupiah(grandTotal['grandTotal']) }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- include js file -->
<script>{% include "report_transaction/assets/main.js" %}</script>
    