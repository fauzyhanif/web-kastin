$(function () {
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

    $(".select2").select2();

    $("img").on("error", function() {
        $(this).attr('src', 'img/user.png');
    });

    $('#showPassword').on("click", function(e) {
        if ($('#pass').hasClass('active')) {
            $('#pass').attr('type', 'text');
            $('#pass').removeClass('active');
            $('#showPassword > i').attr('class', 'fa fa-eye');
        } else {
            $('#pass').attr('type', 'password');
            $('#pass').addClass('active');
            $('#showPassword > i').attr('class', 'fa fa-eye-slash');
        }

        e.preventDefault();        
    });

    $("#reset").on("click", function() {
        $("#form_title").text('Tambah User');
        $("#foto").attr('src', 'img/user.png');
        $("#jenis").removeAttr('disabled');
        $("#nama").removeAttr('disabled');
        $("#nama").empty().trigger("change");
        $("#uid").removeAttr('readonly');
        $("#area").val(null).trigger("change");
        $("#usergroup").val(null).trigger("change");
        $("#reset").attr('class', 'btn btn-default');
        $("#reset").html('<i class="fa fa-refresh"></i>&nbsp; Reset');
        $("#submit").attr('onclick', 'save_data(\'addUser\')');
    });

    $('#filter_jenis').on("change", function() {
        if (this.value == '2') {
            $('#filter_tingkat').removeAttr('disabled');
        } else {
            $('#filter_tingkat').attr('disabled', 'disabled');
        }
    });

    $('#proses').on("click", function() {
        var jenis = $('#filter_jenis').val();
            tingkat = $('#filter_tingkat').val();
            url = "{{ url('user/index') }}";

        if (jenis == '2') {
            url = "{{ url('user/index/') }}" + jenis + '/' + tingkat;
        }

        go_page(url);
    });
});

(function() {

  $('form[data-remote]').on('submit', function(e) {
    var form = $(this);
    var url = form.prop('action');

    $.ajax({
      type: 'POST',
      url: url,
      dataType:'json',
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function(response){
        reload_page2('user/index');
        new PNotify({
            title: data.title,
            text: data.text,
            type: data.type
        });
      }
    });

    e.preventDefault();
  });

})();

function changeJenis(element) {
    var id = element.value;    
    var img_folder = 'sdm';
    // reset uid dan nama
    $('#uid').removeAttr('readonly');
    $("#nama").empty().trigger("change");
    // jika jenisnya murid maka gk boleh ganti uid
    if (id == 2) {
        $('#uid').attr('readonly', 'readonly');
        img_folder = 'mhs';
    }    

    $.ajax({
        url       : "{{ url('user/searchNama/') }}"+id,
        type      : "POST",
        dataType  : "json",
        data      : {'name' : 'value'},
        cache     : false,
        success   : function(response){            
            $("#nama").select2({
                placeholder: 'Cari nama...',
                data: response
            }).on("select2:selecting", function(evt) {                
                var img_name = evt.params.args.data.foto;                
                var getValue = evt.params.args.data.id;                
                $('#uid').val(getValue);
                $('#nip').val(getValue);
                $('#foto').attr('src', 'img/'+img_folder+'/'+img_name);
            });
        }
    });
}

function filter(id) {
    var url = "{{ url('User/index/') }}" + id;
    go_page(url);
}



function edit_data(id_user) {
    $.ajax({
        type      : "GET",
        url       : "{{ url('user/get/') }}"+id_user,
        dataType  : "html",
        success   : function(file){
            $('#formCrud').html(file)
        }
    })   
}


function delete_data(id_user) {
    var url_target = '{{ url("user/deleteUser") }}/' + id_user;
    (new PNotify({
        title: 'Pesan Konfirmasi',
        text: 'Apakah Anda Yakin menghapus data ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: url_target,
            success: function (data) {
                reload_page2('user/index');
                new PNotify({
                    title: 'Sukses',
                    text: 'Data berhasil dihapus',
                    type: 'success'
                });
            }
        });
    }).on('pnotify.cancel', function () {
        console.log('batal');
    });
}

function PreviewImage(id) {
  $('input[name="remove_image"]').val('');
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadImage"+id).files[0]);

  oFReader.onload = function (oFREvent) {
    document.getElementById("uploadPreview"+id).src = oFREvent.target.result;
  };
};