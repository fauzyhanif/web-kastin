{# Define variables #}
{% set tanggalIndo = helper.dateBahasaIndo(date('Y-m-d')) %}
{% set urlPost = url('user/addUser') %}

{# Global css #}
<style>{% include "include/view.css" %}</style>

{# Custom css #}
<style>
#form_filter {
    margin-bottom: 1.5em
}

table a {
    font-weight: 600;
}
</style>
<!-- Header content -->
<section class="content-header">
    <h1>
        User Pengguna
        <small>
            <i class="fa fa-calendar-o"></i> {{ tanggalIndo }} 
        </small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Setup Admin</a></li>
        <li class="active">User</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-4">
            <div class="box box-default" id="formCrud">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah User</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="{{ urlPost }}" data-remote>
                            <div class="form-group col-md-12 text-center">
                                <img src="img/user.png" alt="Foto User" height="85px" class="img-circle text-center" id="foto">
                            </div>
                            
                            <input type="hidden" name="id_unit" value="{{ id_unit }}" id="id_unit">
                            <input type="hidden" name="jenis" value="1">

                            <div class="form-group col-md-12">
                                <label for="nama">
                                    <span class="text-red">*</span> Nama Lengkap
                                </label>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required="">
                            </div>

                            <div class="form-group col-md-12">
                                <label for="nama">
                                    <span class="text-red">*</span> NIP
                                </label>
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP (isi sembarangan jika tidak ada)" required="">
                            </div>  

                            <div class="form-group col-md-5">
                                <label for="uid">
                                    <span class="text-red">*</span> Username
                                </label>
                                <input type="text" class="form-control" name="uid" placeholder="Username" id="uid" required="">
                            </div>

                            <div class="form-group col-md-7">
                                <label for="password">
                                    <span class="text-red">*</span> Password
                                </label>
                                <div class="input-group">
                                    <input type="password" class="form-control active" name="password" placeholder="Password" id="pass" required="">
                                    <a class="input-group-addon" href="#" id="showPassword"><i class="fa fa-eye-slash"></i></a>
                                </div>                                
                            </div> 

                            <div class="form-group col-md-12">
                                <label for="usergroup">
                                    <span class="text-red">*</span> Usergroup
                                </label>
                                <select name="usergroup" class="form-control"  onchange="getArea(this.value)" required>
                                    <option value="">** Pilih Usergroup</option>
                                    {% for opt in usergroup %}
                                    <option value="{{ opt.id }}">{{ opt.nama }}</option>
                                    {% endfor %}
                                </select>
                            </div>                                   
                            
                            <div class="view-select-area"></div>

                            <div class="form-group col-md-12">
                                <label>Upload Foto</label>
                                <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
                            </div>
                              
                            <div class="form-group col-md-12">
                                <center>
                                {{ image("img/sdm/man-2.png", "width":"230", "id":"uploadPreview1", "class":"img-responsive") }}
                                </center>                                   
                            </div>

                            <div class="col-md-12">
                                <span class="text-red">
                                    * ) Wajib diisi.
                                </span>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default" id="reset">
                                        Batal
                                    </button>
                                    <button type="submit" class="btn btn-primary" id="submit">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data User</h3>
                </div>
                <div class="box-body">

                    <table id="data_table" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 10px">No</th>
                                <th>UID</th>
                                <th>Nama / No Identitas</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no=1 %} {% for v in data %}
                            
                            {% if (v.id_jenis == 1) %}
                                {% set folder = 'sdm' %}
                                {% set nama_jenis = 'Guru/Sdm' %}
                            {% else %}
                                {% set folder = 'mhs' %}
                                {% set nama_jenis = 'Murid' %}                                
                            {% endif %}

                            {% if (v.id_status == 'A') %}
                                {% set color = 'green' %}
                            {% else %}
                                {% set color = 'red' %}
                            {% endif %}

                            <tr id="data_{{v.login}}" class="middle-row">
                                <td>{{no}}</td>
                                <td><span class="badge bg-dark">{{v.login}}</span></td>
                                <td>
                                    <img src="img/user/{{ v.foto }}" alt="{{v.nama}}" style="height: 3em; float: left; margin-right: 10px; border-radius: 50px">
                                    <span style="font-weight: 600">{{v.nama}}</span> <br/> 
                                    {% if (v.nip != '') %}
                                        {% if (v.id_jenis == 1) %}
                                            {% set identitas = 'NIP' %}
                                        {% else %}
                                            {% set identitas = 'NIS' %}
                                        {% endif %}                                      
                                    <span class="label label-default">{{identitas}}</span> 
                                    <span class="label label-primary">{{v.nip}}</span>
                                    {% endif %}  
                                </td> 
                                <td>
                                    <a class="btn btn-primary btn-xs" onclick="edit_data('{{ v.id_user }}')"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                </td>
                            </tr>
                            {% set no=no+1 %} {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<script>{% include "akd_user/assets/user2.js" %}</script>