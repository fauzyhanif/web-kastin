<div class="form-group col-md-12">
    <label for="usergroup">
        <span class="text-red">*</span>Area
    </label>
    <select name="area" class="form-control">
        {% for x in dt_area %}
        <option value="{{ x.id }},{{ x.ps_id }}">{{ x.label_menu }}</option>
        {% endfor %}
    </select>
</div>