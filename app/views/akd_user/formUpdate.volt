<div class="box-header">
    <h3 class="box-title" id="form_title">Edit User</h3>
</div>
<!-- /.box-header -->
<div class="box-body pad">
    <div class="row">
        <form id="form_input" method="POST" data-remote action="{{ url('user/editUser') }}">
            <input type="hidden" name="id_user" value="{{ data[0]['id'] }}" id="id_user">
            <input type="hidden" name="id_unit" value="{{ data[0]['id_unit'] }}" id="id_unit">
            <div class="form-group col-md-12 text-center">
                <img src="img/user.png" alt="Foto User" height="85px" class="img-circle text-center" id="foto">
            </div>

            <div class="form-group col-md-12">
                <label for="nama">
                    <span class="text-red">*</span> Nama Lengkap
                </label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{ data[0]['nama'] }}" required="">
            </div>

            <div class="form-group col-md-12">
                <label for="nama">
                    <span class="text-red">*</span> NIP
                </label>
                <input type="text" class="form-control" name="nip" id="nip" value="{{ data[0]['nip'] }}" required="">
            </div>  

            <div class="form-group col-md-5">
                <label for="uid">
                    <span class="text-red">*</span> Username
                </label>
                <input type="text" class="form-control" name="uid" value="{{ data[0]['uid'] }}" id="uid" required="">
                <input type="hidden" name="nip" id="nip">
            </div>

            <div class="form-group col-md-7">
                <label for="password"> 
                    <span class="text-red">*</span> Password
                </label>
                <div class="input-group">
                    <input type="password" class="form-control active" name="password" placeholder="Password" id="pass" required="">
                    <a class="input-group-addon" href="#" id="showPassword"><i class="fa fa-eye-slash"></i></a>
                </div>                                
            </div> 

            <div class="form-group col-md-12">
                <label for="usergroup">
                    <span class="text-red">*</span> Usergroup
                </label>
                <select name="usergroup" class="form-control" required>
                    <?php $id_usergroup = str_replace(",", "", $data[0]['usergroup']); ?>
                    <option value="">** Pilih Usergroup</option>
                    {% for opt in usergroup %}
                    <option value="{{opt.id}}" {% if opt.id is id_usergroup %} selected {% endif %}>{{opt.nama}}</option>
                    {% endfor %}
                </select>
            </div>  
            
            <div class="form-group col-md-12">
                <label for="area"> 
                    <span class="text-red">*</span>Area
                </label>
                <select name="area" class="form-control" required>
                    <?php $id_area = str_replace(",", "", $data[0]['area']); ?>
                    <option value="">** Pilih Area</option>
                    {% for x in area %}
                    <option value="{{ x.id }},{{ x.ps_id }}" {% if x.id is id_area %} selected {% endif %}>{{ x.label_menu }}</option>
                    {% endfor %}
                </select>
            </div>  

            <div class="form-group col-md-12">
                <label>Upload Foto</label>
                <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
            </div>
    
            <div class="form-group col-md-12">
                <center>
                    <img src="img/user/{{ data[0]['foto'] }}" width="230" id="uploadPreview1" class="img-responsive">
                </center>                                   
            </div>
            
            <div class="col-md-12">
                <span class="text-red">
                    * ) Wajib diisi.
                </span>
            </div>

            <div class="col-md-12">
                <div class="pull-right">
                    <button type="reset" class="btn btn-default" id="reset">
                        Batal
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Simpan Perubahan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    (function() {

      $('form[data-remote]').on('submit', function(e) {
        var form = $(this);
        var url = form.prop('action');
        var id_unit     = $("#id_unit").val();
        var url_reload  = "{{ url('user/index/') }}"+id_unit;

        $.ajax({
          type: 'POST',
          url: url,
          dataType:'json',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData: false,
          success: function(response){
            go_page(url_reload);
            new PNotify({
                title: response.title,
                text: response.text,
                type: response.type
            });
          }
        });

        e.preventDefault();
      });

    })();

	$(function () {
		$(".select2").select2();
	})
</script>