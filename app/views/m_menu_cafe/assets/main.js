$(function () {

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'stokgudang_' + dd + mm + yyyy
            },
            {
                extend: 'pdfHtml5',
                title: 'stokgudang_' + dd + mm + yyyy
            }
        ],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "{{ url('m_menu_cafe/index/') }}"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, nama, jenis, harga, premi, id_unit, aktif, pengurangan_qty, foto, stok) {
  var form = $('form[name="menu_cafe"]').attr('action', '{{ url('m_menu_cafe/update/') }}' + id);
  form.find('[name="id_unit"]').val(id_unit);
  form.find('[name="nama"]').val(nama);
  form.find('[name="harga"]').val(harga);
  form.find('[name="premi"]').val(premi);
  form.find('[name="stok"]').val(stok);
  form.find('select[name="jenis"]').val(jenis);
  form.find('select[name="aktif"]').val(aktif);
  form.find('#uploadPreview2').attr("src", "img/produk/" + foto);

    if (pengurangan_qty == 0) {
        $('input:radio[name=pengurangan_qty]').filter('[value="0"]').prop("checked", true);
    } else {
        $('input:radio[name=pengurangan_qty]').filter('[value="1"]').prop("checked", true);
    }
}

function delete_data(id, nama) {
    $('input#id').val(id);
    $('#nama').text(nama);
}

function filter($jenis){
    var id_unit = $('#id_unit').val();
    var url     = "{{ url('m_menu_cafe/index/') }}"+id_unit+"/"+$jenis;
    reload_page2(url);
}

function PreviewImages(id) {
    $('input[name="remove_image"]').val('');
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage"+id).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview2").src = oFREvent.target.result;
    };
};