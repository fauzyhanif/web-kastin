<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Menu Cafe</h4>
      </div>

      <form name="menu_cafe" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <input type="hidden" name="id_unit" value="">
          <div class="form-group">
              <label>Nama Menu</label>
              <input type="text" name="nama" class="form-control" placeholder=" Nama Menu" id="nama_menu"> 
          </div>

          <div class="form-group">
              <label>Jenis</label>
              <select name="jenis" class="form-control">
                  <option value="">** Pilih Jenis</option>
                  <option>Makanan</option>
                  <option>Minuman</option>
                  <option>Snack</option>
                  <option>Others</option>
                  <option>Tiket</option>
              </select>
          </div>

          <div class="form-group">
              <label>Harga</label>
              <input type="text" name="harga" class="form-control tarif" placeholder=" Harga" id="harga"> 
          </div>
          
          <div class="form-group">
              <label>Premi</label>
              <input type="text" name="premi" class="form-control tarif" placeholder=" Premi"> 
          </div>
          
          <div class="form-group">
            <label>Stok</label>
            <input type="number" name="stok" class="form-control" placeholder=" Stok"> 
          </div>

          <div class="form-group col-md-12">
            <label>Upload Foto</label>
            <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
          </div>
          
          <div class="form-group col-md-12">
            <center>
            {{ image("img/produk/default.png", "width":"230", "id":"uploadPreview2", "class":"img-responsive") }}
            </center>                                   
          </div>

          <div class="form-group" style="display:none">
            <label>Pengurangan quantity?</label> <br>
            <label class="radio-inline"><input type="radio" name="pengurangan_qty" checked value="1">Ya</label>
            <label class="radio-inline"><input type="radio" name="pengurangan_qty" value="0">Tidak</label>
          </div>

          <div class="form-group">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal">
            Batal
          </button>
          <button type="submit" class="btn btn-primary">
            Simpan Perubahan
          </button>
        </div>
      </form>

    </div>
  </div>
</div>