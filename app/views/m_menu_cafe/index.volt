<!-- Header content -->
<section class="content-header">
    <h1>
        Manajemen Menu
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Manajemen Menu</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-4">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Menu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="{{ url('M_menu_cafe/create') }}" data-remote>
                            <input type="hidden" name="id_unit" value="{{ id_unit }}" id="id_unit">
                            <div class="form-group col-md-12">
                                <label>Nama Menu</label>
                                <input type="text" name="nama" class="form-control" placeholder=" Nama Menu" id="nama_menu"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Jenis Menu</label>
                                <select name="jenis" class="form-control">
                                    <option value="">** Pilih Jenis Menu</option>
                                    <option>Makanan</option>
                                    <option>Minuman</option>
                                    <option>Snack</option>
                                    <option>Others</option>
                                    <option>Tiket</option>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Harga</label>
                                <input type="text" name="harga" class="form-control tarif" placeholder=" Harga" id="harga"> 
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>Premi</label>
                                <input type="text" name="premi" class="form-control tarif" placeholder=" Premi"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Upload Foto</label>
                                <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
                            </div>
                            
                            <div class="form-group col-md-12">
                                <center>
                                {{ image("img/produk/default.png", "width":"230", "id":"uploadPreview1", "class":"img-responsive") }}
                                </center>                                   
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>Pengurangan quantity?</label> <br>
                                <label class="radio-inline"><input type="radio" name="pengurangan_qty" checked value="1">Ya</label>
                                <label class="radio-inline"><input type="radio" name="pengurangan_qty" value="0">Tidak</label>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default" onclick="return reload_page2('M_menu_cafe/index/{{ id_unit }}')">
                                        Batal
                                    </button>
                                    <button type="submit" class="btn btn-primary" id="submit">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <div class="col-sm-5">
                        <select class="form-control" onchange="filter(this.value)">
                            <option value="">** Filter Menu</option>
                            <option {% if jenis is 'Makanan' %} selected {% endif %}>Makanan</option>
                            <option {% if jenis is 'Minuman' %} selected {% endif %}>Minuman</option>
                            <option {% if jenis is 'Snack' %} selected {% endif %}>Snack</option>
                            <option {% if jenis is 'Others' %} selected {% endif %}>Others</option>
                            <option {% if jenis is 'Tiket' %} selected {% endif %}>Tiket</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-primary btn-block" onclick="return go_page('distribusi_barang/createDistribusi')">
                            <i class="fa fa-cart-arrow-down"></i> &nbsp; Distribusikan Barang
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th>Nama Menu</th>
                                <th width="10%">Stok</th>
                                <th class="text-right" width="15%">Harga</th>
                                <th class="text-right" width="15%">Premi</th>
                                <th width="10%">Aktif</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %} {% for x in data %}
                            <tr>
                                <td>{{ no }}.</td>
                                <td>{{ x.nama }}</td>
                                <td>{{ x.stok }}</td>
                                <td class="text-right">{{ this.helper.formatRupiah(x.harga) }}</td>
                                <td class="text-right">{{ this.helper.formatRupiah(x.premi) }}</td>
                                <td class="text-center">
                                    {% if x.aktif is 'Y' %}
                                    <span class="badge bg-green">{{ x.aktif }}</span>
                                    {% else %} 
                                    <span class="badge bg-red">{{ x.aktif }}</span>
                                    {% endif %}
                                </td>
                                <td> 
                                    <button class="btn btn-primary btn-xs" 
                                        onclick="edit_data('{{ x.id }}','{{ x.nama }}','{{ x.jenis }}','{{ x.harga }}','{{ x.premi }}','{{ x.id_unit }}','{{ x.aktif }}','{{ x.pengurangan_qty }}','{{ x.foto }}','{{ x.stok }}')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="fa fa-edit"></i> Edit
                                    </button>
                                </td>
                            </tr>
                            {% set no = no + 1 %} {% endfor %}
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
{% include "m_menu_cafe/formUpdate.volt" %}
{% include "m_menu_cafe/formDelete.volt" %}

<!-- include js file -->
<script>{% include "m_menu_cafe/assets/main.js" %}</script>