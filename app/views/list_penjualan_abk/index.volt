<input type="hidden" class="form-control" id="set_file_name_pdf" value="<?= date('Y_m_d').'-'.date('Y_m_d') ?>">
<!-- Header content -->
<section class="content-header">
    <h1>
        List Penjualan ABK
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">List Penjualan ABK</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    
                    <form name="filter" action="{{ url('list_penjualan_abk/filter') }}" method="POST">
                        <h3 class="box-title">
                            <p><i class="fa fa-calendar-minus-o"></i> Laporan tanggal <span id="tgl-filter"><?= date('d-m-Y') ?></span></p>
                        </h3>
                        <input type="hidden" name="id_unit" id="id_unit" value="{{ id_unit }}">
                        <div class="col-md-1 pull-right">
                            <button type="submit" class="btn btn-success btn-block">Filter</button>
                        </div>
                        <div class="form-group col-md-4 pull-right" style="margin-bottom: 0px;">
                            <input type="text" name="tgl_transaksi" class="form-control" id="daterangepicker">
                        </div>
                    </form>
                </div>
                <div class="box-body" id="list-view">
                    <table id="data_table" class="table table-striped data-table">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="20%">Waktu</th>
                                <th width="20%">Kelas</th>
                                <th width="15%">ID Transaksi</th>
                                <th class="text-right" width="20%">Total Belanja</th>
                                <th width="20%">Admin</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% set no = 1 %}
                            {% set total = 0 %}
                            {% for x in dt_record %}
                            <tr>
                                <td>{{ no }}.</td>
                                <td>{{ this.helper.konversi_tgl(x.tgl_transaksi) }} (<?= date_format(date_create($x->tgl_transaksi), "H:i:s"); ?>)</td>
                                <td>{{ x.nm_kelas }}</td>
                                <td>{{ x.id_transaksi }}</td>
                                <td class="text-right">Rp. {{ this.helper.formatRupiah(x.total) }}</td>
                                <td>{{ x.nama }}</td>
                                <td>
                                    <button 
                                        type="button"
                                        class="btn btn-primary btn-xs"
                                        data-toggle="modal" 
                                        data-target="#myModal"
                                        onclick="detail('{{ x.id_transaksi }}')"
                                    >
                                        <i class="fa fa-list-ul"></i> Detail
                                    </button>
                                </td>
                            </tr>
                            {% set no = no + 1 %}
                            {% set total = total + x.total %}
                            {% endfor %}

                            <tr>
                                <td colspan="4" class="text-right"><h3><b>Total:</b></h3></td> 
                                <td style="display : none;"></td>
                                <td style="display : none;"></td>
                                <td style="display : none;"></td>
                                <td class="text-right"><h3><b>Rp. {{ this.helper.formatRupiah(total) }}</b></h3></td> 
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->
{% include "list_penjualan_abk/modal_detail_penjualan.volt" %}

<!-- include js file -->
<script>{% include "list_penjualan_abk/assets/main.js" %}</script>
    