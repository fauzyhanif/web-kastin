<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
	<title>PUSKOPKA JATENG | Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Elite User Forms template Responsive, Login form web template,Flat Pricing tables,Flat Drop downs Sign up Web Templates, 
	Flat Web Templates, Login sign up Responsive web template, SmartPhone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- Custom Theme files -->
	<!--fonts--> 
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
	<!--//fonts--> 

	{{ stylesheet_link("login_elite_user/css/font-awesome.css") }}
	{{ stylesheet_link("login_elite_user/css/popup-box.css") }}
	{{ stylesheet_link("login_elite_user/css/style.css") }}
</head>
<body>
	<div class="agileinfo-dot">
		<!--background-->
		<!-- login -->
		<h1>PUSKOPKA JATENG</h1>
		<div class="login-section">
			<div class="login-w3l" style="margin-right: 0px; ">	
				<h2 class="sub-head-w3-agileits">Sign in</h2>	
				<div class="login-form">			
					<form action="{{ url('account/loginProses') }}" method="post">
						<input type="text" class="user" name="uid" placeholder="Username" required="" />
						<input type="password" class="lock" name="passwd" placeholder="Password" required="" />
						
						<input type="submit" value="Sign in">
					</form>	
					
				</div>
				<!-- //login -->
				<div class="clear"></div>
			</div> 
			<div class="profile-agileits bg-color-agile" style="height: 424px">
			{{ image('login_elite_user/images/g3350.png', 'style':'height:424px') }}
			</div>

			<div class="clear"></div>
		</div>	
		<p class="footer">© 2017 Elite User Forms. All Rights Reserved | Design by <a href="http://w3layouts.com/"> W3layouts</a></p>
	</div>
	{{ javascript_include("login_elite_user/js/jquery-2.1.4.min.js") }}
	{{ javascript_include("login_elite_user/js/jquery.magnific-popup.js") }}
	<script>
		$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>
	<!--//popup-js-->

</body>
</html>