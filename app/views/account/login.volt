<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Kastin | Sign in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {{ stylesheet_link("bootstrap/css/bootstrap.min.css") }}
  <!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  {{ stylesheet_link("css/AdminLTE.min.css") }} 
  <!-- iCheck -->
  {{ stylesheet_link("plugins/iCheck/square/blue.css") }}

  <style type="text/css">
    .logo {
      margin:-70px 0px -30px 0px; width:45%
    }
  
    .errorMessage{
      color: #a94442;
      background-color: #f2dede;
      border-color: #ebccd1;
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
      text-align: center;
    }
    
  </style>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    
    <div class="login-logo">
      <a href="#"><b>Kasir</b>Kantin</a>
    </div>

    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <?= $this->flashSession->output() ?>

      <form class="form" action="{{ url('account/loginProses') }}" method="post" >

        <div class="form-group has-feedback">
          <input type="text" name="uid" class="form-control" placeholder=" Username">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <input type="password" name="passwd" class="form-control" placeholder=" Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        
        <div class="row">
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
        </div>

      </form>

    </div>

  </div>

<!-- jQuery 2.2.3 -->
{{ javascript_include("jquery/dist/jquery.min.js") }} 
<!-- Bootstrap 3.3.6 -->
{{ javascript_include("bootstrap/dist/js/bootstrap.min.js") }}

</body>
</html>
