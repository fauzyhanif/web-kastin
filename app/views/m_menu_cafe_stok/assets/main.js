$(function () {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form        = $(this);
        var url         = form.prop('action');
        var id_unit     = $('#id_unit').val();
        var url_reload  = "{{ url('m_menu_cafe_stok/index/') }}" + id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id_stok, id_menu, harga_beli, qty) {
  var form = $('form[name="menu_cafe"]').attr('action', '{{ url('m_menu_cafe_stok/update/') }}' + id_stok);
  form.find('[name="id_stok"]').val(id_stok);
  form.find('select[name="id_menu"]').val(id_menu);
  form.find('[name="qty"]').val(qty);
  form.find('[name="harga_beli"]').val(harga_beli);
}

$("input[name='jumlah_dus']").keyup(function(){
    var jumlah_dus = this.value;
    var isi_dus = $("input[name='isi_dus']").val();
    var qty = 0;

    if (jumlah_dus <= 0) {
        $("input[name='qty']").val(isi_dus);
    } else {
        qty = isi_dus * jumlah_dus;
        $("input[name='qty']").val(qty);
    }
});

$("input[name='isi_dus']").keyup(function(){
    var isi_dus = this.value;
    var jumlah_dus = $("input[name='jumlah_dus']").val();
    var qty = 0;

    if (jumlah_dus <= 0) {
        $("input[name='qty']").val(isi_dus);
    } else {
        qty = isi_dus * jumlah_dus;
        $("input[name='qty']").val(qty);
    }
});

