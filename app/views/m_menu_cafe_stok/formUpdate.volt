<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Update Stok Menu</h4>
        </div>
    
        <form name="menu_cafe" method="POST" data-remote="data-remote">
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Menu</label>
                    <select name="id_menu" class="form-control" required disabled>
                        <option value="">** Pilih Menu</option>
                        {% for x in dt_menu %}
                        <option value="{{ x.id }}">{{ x.nama }}</option>
                        {% endfor %}
                    </select>
                </div>

                <div class="form-group">
                    <label>Qty</label>
                    <input type="text" name="qty" class="form-control" required> 
                </div>

                <div class="form-group">
                    <label>Harga Beli</label>
                    <input type="text" name="harga_beli" class="form-control tarif" required> 
                </div>
    
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">
                    Batal
                </a>

                <button type="submit" class="btn btn-primary btn-flat">
                    Simpan Perubahan
                </button>
            </div>
        </form>
    
        </div>
    </div>
    </div>