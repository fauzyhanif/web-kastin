<!-- Header content -->
<section class="content-header">
        <h1>
            Penambahan Stok Menu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">Menu Utama</a></li>
            <li class="active">Penambahan Stok Menu</li>
        </ol>   
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-3">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title" id="form_title">Tambah Stok Menu</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        <div class="row">
                            <form id="form_input" method="POST" action="{{ url('M_menu_cafe_stok/create') }}" data-remote>
                                <input type="hidden" name="id_unit" value="{{ id_unit }}" id="id_unit">
                                <div class="form-group col-md-12">
                                    <label>Nama Menu</label>
                                    <select name="id_menu" class="form-control" required>
                                        <option value="">** Pilih Menu</option>
                                        {% for x in dt_menu %}
                                        <option value="{{ x.id }}">{{ x.nama }}</option>
                                        {% endfor %}
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Jumlah Dus</label>
                                    <input type="number" name="jumlah_dus" class="form-control" value="0"> 
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label>Isi per Dus</label>
                                    <input type="number" name="isi_dus" class="form-control" value="0"> 
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label>Qty</label>
                                    <input type="text" name="qty" class="form-control" required> 
                                </div>
    
                                <div class="form-group col-md-12">
                                    <label>Harga Beli</label>
                                    <input type="text" name="harga_beli" class="form-control tarif" required> 
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="reset" class="btn btn-default" onclick="return reload_page2('M_menu_cafe_stok/index/{{ id_unit }}')">
                                            Batal
                                        </button>
                                        <button type="submit" class="btn btn-primary" id="submit">
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- right column -->
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Penambahan Stok Menu Bulan ini</h3>
                    </div>
                    <div class="box-body">
                        <table id="data_table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th>Nama Menu</th>
                                    <th width="12%">Jenis</th>
                                    <th width="15%">Tambahan stok</th>
                                    <th class="text-right" width="12%">Harga Beli</th>
                                    <th width="8%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% set no = 1 %} {% for x in dt_stok %}
                                <tr>
                                    <td>{{ no }}.</td>
                                    <td>{{ x.nama }}</td>
                                    <td>{{ x.jenis }}</td>
                                    <td>{{ x.qty }}</td>
                                    <td class="text-right">{{ this.helper.formatRupiah(x.harga_beli) }}</td>
                                    <td> 
                                        <button
                                            type="button"  
                                            class="btn btn-primary btn-xs" 
                                            onclick="edit_data('{{ x.id_stok }}','{{ x.id_menu }}','{{ x.harga_beli }}', '{{ x.qty }}')" 
                                            data-toggle="modal" 
                                            data-target="#update"
                                            {% if x.qty is 0 %} disabled {% endif %}
                                        >
                                            <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </td>
                                </tr>
                                {% set no = no + 1 %} {% endfor %}
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    
    </section>
    <!-- /.content -->
    
    <!-- include popup -->
    {% include "m_menu_cafe_stok/formUpdate.volt" %}
    
    <!-- include js file -->
    <script>{% include "m_menu_cafe_stok/assets/main.js" %}</script>