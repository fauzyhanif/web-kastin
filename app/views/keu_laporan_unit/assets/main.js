$(function () {

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

	$('#daterangepicker-1').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker-1').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });
    
    $('#daterangepicker-2').daterangepicker({
		startDate: moment(),
    	endDate: moment(),
		locale : {
			format : 'YYYY-MM-DD'
		}
	},
	function (start, end) {
        $('#daterangepicker-2').val(start.format('YYYY-MM-DD')+" - "+end.format('YYYY-MM-DD'))
    });

	$('.data-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'laporanpenjualan_' + dd + mm + yyyy
            },
            {
                extend: 'pdfHtml5',
                title: 'laporanpenjualan_' + dd + mm + yyyy
            }
        ],
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
	});

});

(function() {

    $('form[name="filter"]').on('submit', function(e) {
        var form      	= $(this);
        var url       	= form.prop('action');
        var data = {
	      "id_unit": $('input[name="id_unit"]').val(),
	      "tgl_transaksi": $('input[name="tgl_transaksi"]').val()
        };
        
        var dateAr          = $('input[name="tgl_transaksi"]').val().split(' - ');
        var dateAr1         = dateAr[0].split('-');
        var dateAr2         = dateAr[1].split('-');
        var date_start      = dateAr1[2] + '-' + dateAr1[1] + '-' + dateAr1[0];
        var date_end        = dateAr2[2] + '-' + dateAr2[1] + '-' + dateAr2[0];

        $('#tgl-filter').text(date_start + ' s/d ' + date_end);
        
        $.ajax({
            type: 'POST',
            url: url,
		    dataType: "html",
            data: data,
            success: function(response){
                $('#list-view').html(response);
            }	
        });

        e.preventDefault();
    });
})();

