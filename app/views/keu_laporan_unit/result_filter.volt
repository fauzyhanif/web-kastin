<table class="table table-striped data-table">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th>Item</th>
            <th width="15%">Terjual</th>
            <th class="text-right" width="15%">Modal</th>
            <th class="text-right" width="15%">Pendapatan</th>
            <th class="text-right" width="15%">Premi</th>
            <th class="text-right" width="15%">Keuntungan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $ttl_pcs = 0;
        $ttl_modal = 0;
        $ttl_pendapatan = 0;
        $ttl_premi = 0;
        $ttl_keuntungan = 0;
        for ($i=0; $i < count($dt_record); $i++) {
        ?>
        
        <tr>
            <td><?= $no; ?>.</td>
            <td><?= $dt_record[$i]['item']; ?></td>
            <td><?= $dt_record[$i]['qty']; ?> pcs</td>
            <td class="text-right"><?= $this->helper->formatRupiah($dt_record[$i]['jml_modal']); ?></td>
            <td class="text-right"><?= $this->helper->formatRupiah($dt_record[$i]['jml_pendapatan']); ?></td>
            <td class="text-right"><?= $dt_record[$i]['premi'] ?> @<?= $dt_record[$i]['qty'] ?> = <?= $this->helper->formatRupiah($dt_record[$i]['premi'] * $dt_record[$i]['qty']); ?></td>
            <td class="text-right"><?= $this->helper->formatRupiah($dt_record[$i]['jml_keuntungan']); ?></td>
        </tr>

        <?php
        $no = $no + 1;
        $ttl_pcs = $ttl_pcs + $dt_record[$i]['qty'];
        $ttl_modal = $ttl_modal + $dt_record[$i]['jml_modal'];
        $ttl_pendapatan = $ttl_pendapatan + $dt_record[$i]['jml_pendapatan'];
        $ttl_premi = $ttl_premi + $dt_record[$i]['premi'] * $dt_record[$i]['qty'];
        $ttl_keuntungan = $ttl_keuntungan + $dt_record[$i]['jml_keuntungan'];
        }
        ?>

        <tr>
            <td colspan="2" class="text-center"><h4><b>Total</b></h4></td>
            <td style="display: none;"></td>
            <td><h4><b>{{ ttl_pcs }} pcs</b></h3></td>
            <td class="text-right"><h4><b>Rp. {{ this.helper.formatRupiah(ttl_modal) }}</b></h4></td>
            <td class="text-right"><h4><b>Rp. {{ this.helper.formatRupiah(ttl_pendapatan) }}</b></h4></td>
            <td class="text-right"><h4><b>Rp. {{ this.helper.formatRupiah(ttl_premi) }}</b></h4></td>
            <td class="text-right"><h4><b>Rp. {{ this.helper.formatRupiah(ttl_keuntungan) }}</b></h4></td>
        </tr>
    </tbody>
</table>

<script>
$('.data-table').DataTable({
    dom: 'Bfrtip',
    buttons: [
            'copy', 'csv', 'excel', 'pdf'
    ],
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": false,
    "autoWidth": true,
    "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "Semua"]
    ],
    "iDisplayLength": 50,
    "language": {
        "url": "js/Indonesian.json"
    }
});
</script>