<table class="table table-striped data-table">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th>Item</th>
            <th width="15%">Terjual</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $ttl_pcs = 0;
        for ($i=0; $i < count($dt_record); $i++) {
        ?>
        
        <tr>
            <td><?= $no; ?>.</td>
            <td><?= $dt_record[$i]['item']; ?></td>
            <td><?= $dt_record[$i]['qty']; ?> pcs</td>
        </tr>

        <?php
        $no = $no + 1;
        $ttl_pcs = $ttl_pcs + $dt_record[$i]['qty'];
      
        }
        ?>

        <tr>
            <td colspan="2" class="text-center"><h4><b>Total</b></h4></td>
            <td style="display: none;"></td>
            <td><h4><b>{{ ttl_pcs }} pcs</b></h3></td>
            
        </tr>

    </tbody>
</table>