<!-- Header content -->
<section class="content-header">
    <h1>
        Laporan Penjualan 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#">Menu Utama</a></li>
        <li class="active">Laporan Penjualan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header with-border">
                    <form name="filter" action="{{ url('keu_laporan_unit/filterAbk') }}" method="POST">
                        <h3 class="box-title">
                            <p><i class="fa fa-calendar-minus-o"></i> Laporan tanggal <span id="tgl-filter"><?= date('d-m-Y') ?></span></p>
                        </h3>
                        <input type="hidden" name="id_unit" id="id_unit" value="{{ id_unit }}">

						<div class="col-md-1 pull-right">
							<button type="submit" class="btn btn-success btn-block">Filters</button>
						</div>
                        <div class="form-group col-md-4 pull-right" style="margin-bottom: 0px;">
                            <input type="text" name="tgl_transaksi" class="form-control" id="daterangepicker-2">
                        </div>
					</form>
				</div>
                <div class="box-body" id="list-view">
                    <table class="table table-striped data-table">
                        <thead>
                        	<tr>
                        		<th width="5%">No</th>
                        		<th>Item</th>
                        		<th width="15%">Terjual</th>
                        	</tr>
                        </thead>
						<tbody>
                            <?php
                            $no = 1;
                            $ttl_pcs = 0;
                            for ($i=0; $i < count($dt_record); $i++) {
                            ?>
                            
                            <tr>
                                <td><?= $no; ?>.</td>
                                <td><?= $dt_record[$i]['item']; ?></td>
                                <td><?= $dt_record[$i]['qty']; ?> pcs</td>
                            </tr>

                            <?php
                            $no = $no + 1;
                            $ttl_pcs = $ttl_pcs + $dt_record[$i]['qty'];
                          
                            }
                            ?>

                            <tr>
                                <td colspan="2" class="text-center"><h4><b>Total</b></h4></td>
                                <td style="display: none;"></td>
                                <td><h4><b>{{ ttl_pcs }} pcs</b></h3></td>
                                
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>{% include "keu_laporan_unit/assets/main.js" %}</script>
