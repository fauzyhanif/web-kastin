<?php
use Phalcon\Mvc\View;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Image\Adapter\Imagick;

class UserController extends \Phalcon\Mvc\Controller
{
    protected $check;
    protected $messages;
    protected $title;
    protected $type;
    protected $text;

  	public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction($id_unit)
    {      
        $akses          = MAksesApps::find(["conditions" => "aktif = 'Y'"]);
        $unit           = MUnit::find(["conditions" => "aktif = 'Y'"]);
        $dt_kelas       = MKelas::find(["conditions" => "aktif = '1'", "order" => "nama ASC"]);
        $usergroup      = RefUsergroup::find(["conditions" => "aktif = 'Y'"]);
        $area           = RefArea::find(["conditions" => "aktif = 'Y'"]);

        if ($id_unit == '') {
            $data = ViewUser::find();
        } else {
            $data = ViewUser::findByIdUnit($id_unit);
        }

        $this->view->setVars([
            "data"          => $data,
            "unit"          => $unit,
            "dt_kelas"      => $dt_kelas,
            "id_unit"       => $id_unit,
            "usergroup"     => $usergroup,
            "area"          => $area,
            "tingkat"       => $getTingkat,
            "set_tingkat"   => $tingkat,
            "set_jenis"     => $jenis,
            "filter"        => $filter
        ]);
        
        $this->view->pick('akd_user/index');        

    }   

    public function getAreaAction($id_usergroup)
    {
        $dt_area = RefArea::find(["conditions" => "usergroup_id like '%$id_usergroup%'"]);
        $this->view->dt_area = $dt_area;
        $this->view->pick("akd_user/view_select_area");
    }

    public function addUserAction()
    {        
        $this->checkValidation();
        
        if (count($this->messages) == 0) {
            $data = new RefUser();
            $this->save($data, 'tambah');        
        }
        
        $notif = ['title' => $this->title, 'text' => $this->text, 'type' => $this->type];
        echo json_encode($notif);        
    }

    public function editUserAction()
    {
        $check = new Validation();    
        
        $this->presenceOf($check, 'uid', 'UID');         

        $this->messages = $check->validate($_POST);
        $post           = $this->request->getPost();
        $id             = $_POST['id_user'];
        $arr_area       = explode(",", $_POST['area']);
        $id_area        = $arr_area[0];
        $id_kelas       = substr($_POST['area'], strpos($_POST['area'], "-") + 1);

        if (count($this->messages) == 0) {
            $data = RefUser::findFirst(["conditions" => "id = $id"]);

            $data->assign(array(
                'id_unit'       => $_POST['id_unit'],
                'usergroup'     => ','.$_POST['usergroup'].',',     
                'area'          => ','.$id_area.',',     
                'id_kelas'      => $id_kelas,     
                'nama'          => $_POST['nama'],
                'uid'           => $_POST['uid'],
            ));

            if ($_POST['password'] != '') {
                $data->assign(['passwd' => md5($_POST['password'])]);
            }
            
            if ($data->save()) {
                $urel       =  DOCUMENT_ROOT.'img/user/';
                $fileName   = 'man-2.png';
                $dt_sdm     = RefAkdSdm::find(["conditions" => "nip_riil = $id"]);

                if ($dt_sdm[0]->foto != 'man-2.png') {
                    unlink($urel.$dt_sdm[0]->foto);
                }

                if ($this->request->hasFiles() == true) {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        if ($file->getSize() > 0) {
                            $fileName = $data->id.'.'.$file->getExtension();
                            $file->moveTo($urel . $fileName);
                        } else {
                            $fileName = 'man-2.png';
                        }   
                    }
                }

                $dt_sdm[0]->assign(array(
                    'nip'       => $_POST['nip'],
                    'nama'      => $_POST['nama'],
                    'foto'      => $fileName,
                ));

                $dt_sdm[0]->save();

                $notif = array(
                    'title' => 'Success',
                    'text' => 'Data berhasil diubah',
                    'type' => 'success',
                );

            } else {
                $messages = '';
                $errors = $data->getMessages();
                foreach ($errors as $error) {
                    $messages .= "$error"."</br>";
                }

                $notif = array(
                    'title' => 'Ooppss',
                    'text' => $messages,
                    'type' => 'danger',
                );
            }            
        } else {
            $messages = '';
            $errors = $data->getMessages();
            foreach ($errors as $error) {
                $messages .= "$error"."</br>";
            }

            $notif = array(
                'title' => 'Ooppss',
                'text' => $messages,
                'type' => 'danger',
            );         
        }

        echo json_encode($notif);     
    }      

    public function deleteUserAction($id_user)
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $del1 = RefUser::findFirst(["conditions" => "id = '$id_user'"]);
        // $del2 = RefAkdSdm::findFirst(["conditions" => "nip = '$id_user'"]);
        $del1->delete();
        // $del2->delete();
        echo json_encode(array("status" => true));
    }

    public function getAction($id_user)
    {
        $unit       = MUnit::find(["conditions" => "aktif = 'Y'"]);
        $usergroup  = RefUsergroup::find(["conditions" => "aktif = 'Y'"]);
        $area       = RefArea::find(["conditions" => "aktif = 'Y'"]);
        $akses      = MAksesApps::find(["conditions" => "aktif = 'Y'"]);


        $data = $this->modelsManager->createBuilder()
            ->addFrom('RefUser', 'a')
            ->leftJoin('RefAkdSdm', 'a.id = b.nip_riil', 'b')
            ->columns(['a.id as id, a.id_jenis, a.uid, a.nip, a.area, a.usergroup, a.nama, a.id_unit,b.foto']) 
            ->where("a.id = '$id_user'")
            ->getQuery()
            ->execute();

        $this->view->setVars([
            "data"          => $data->toArray(),
            "unit"          => $unit,
            "usergroup"     => $usergroup,
            "area"          => $area,
            "akses"         => $akses,
        ]);

        $this->view->pick('akd_user/formUpdate'); 
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function save($data, $message) 
    {
        $arr_area       = explode(",", $_POST['area']);
        $id_area        = $arr_area[0];
        $id_kelas       = substr($_POST['area'], strpos($_POST['area'], "-") + 1);
        $data->assign(array(
            'id_unit'       => $_POST['id_unit'],
            'nama'          => $_POST['nama'],
            'uid'           => $_POST['uid'],
            'nip'           => $_POST['nip'],
            'id_jenis'      => $_POST['jenis'],
            'area'          => ",$id_area,",            
            'id_kelas'      => $id_kelas,            
            'usergroup'     => ','.$_POST['usergroup'].',',            
            'passwd'        => md5($_POST['password']),
            'aktif'         => 'Y'
        ));
        
        if ($data->save()) {

            $fileName = 'man-2.png';

            $urel =  DOCUMENT_ROOT.'img/user/';
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    if ($file->getSize() > 0) {
                        $fileName = $data->id.'.'.$file->getExtension();
                        $file->moveTo($urel . $fileName);
                    } else {
                        $fileName = 'man-2.png';
                    }   
                }
            }

            /* 
            insert
            jika kosong maka man-2.png

            update
            jika foto baru dan yg lama adalah man-2.png maka tidak hapus
            jika foto baru dan yg lama bukan man-2.png maka hapus
            */

            $data2 = new RefAkdSdm();
            $data2->assign(array(
                'nip'       => $_POST['nip'],
                'nip_riil'  => $data->id,
                'nama'      => $_POST['nama'],
                'foto'      => $fileName,
            ));
            $data2->save();

            $this->title = 'Sukses';
            $this->text = 'Data berhasil di' . $message;
            $this->type = 'success';
        } else {
            $errors = $data->getMessages();
            foreach ($errors as $error) {
                $this->text .= "$error"."</br>";
            }
            $this->title = 'Error!';
            $this->type = 'warning';
        }         
    }   
        
    public function presenceOf($validation, $column, $name) 
    {
        $validation->add($column, new PresenceOf([
            'message' => '<b>&raquo; '.$name.'</b> tidak boleh kosong'
        ]));
    }

    public function checkValidation() 
    {
        $check = new Validation();    
        
        $this->presenceOf($check, 'jenis', 'Jenis User');        
        $this->presenceOf($check, 'uid', 'UID');        
        $this->presenceOf($check, 'password', 'Password');        
        // $this->presenceOf($check, 'area', 'Area Akses Menu');        
        // $this->presenceOf($check, 'usergroup', 'Usergroup');        

        $this->messages = $check->validate($_POST);

        foreach ($this->messages as $message) {
            $this->text .= "$message"."</br>";
        }
        $this->title = 'Gagal';
        $this->type = 'warning';  
    }  
    
    public function profilAction()
    {
      if ($this->session->get('id_jenis') == 1) {
        $nip = $this->session->get('nip');
        $cmd  = "SELECT * from RefAkdSdm where nip = '$nip'";
        $query = $this->modelsManager->executeQuery($cmd);
        $query2 = $this->modelsManager->executeQuery($cmd)->toArray();
        $this->view->profil = $query;
        $this->view->profil2 = $query2;
        
        $this->view->pick('akd_user/sdm_profil');
      } elseif($this->session->get('id_jenis') == 2) {
        $this->view->pick('akd_user/mhs_profil');
      }      
    }

    public function resetAction($id)
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $user = RefUser::findFirst($id);
        $pas_db = $user->passwd;
        $pas_in = md5($_POST['pass']);
        $pas_bar = md5($_POST['pass_baru']);

        if ($pas_db != $pas_in) {
          $notif = array(
            'class'   => 'warning',
            'pesan1'  => 'Password yang anda masukkan salah',
            'pesan2'  => 'Warning',
          );

        } else {

          $validation = new Phalcon\Validation();
      		$validation->add('pass_baru', new PresenceOf(array(
      		    'message' => 'Password Baru tidak boleh kosong'
      		)));
      		$validation->add('pass', new PresenceOf(array(
      		    'message' => 'Password Lama tidak boleh kosong'
      		)));

      		$messages = $validation->validate($_POST);
      		$pesan    = '';
      		if (count($messages)) {
      		    foreach ($messages as $message) {
      		        $pesan .= "$message"."</br>";
      		    }
      			$notif = array(
      				'class'   => 'warning',
      				'pesan1'  => $pesan,
      				'pesan2'  => 'Warning',
      			);
      		}else{
            $user->assign(array(
              'passwd'  => $pas_bar
            ));

            $simpan = $user->save();
            $notif = array(
              'class'   => 'success',
              'pesan1'  => 'Password berhasil di update',
              'pesan2'  => 'Success',
            );
      		}

        }
        echo json_encode($notif);
    }

    private function imageCheck($extension)
    {
        $allowedTypes = [
            'image/gif',
            'image/jpg',
            'image/png',
            'image/jpeg'
        ];

        return in_array($extension, $allowedTypes);
    }

    public function uploadFotoAction($value='')
    {
        $nip = $this->session->get('nip');
        if ($this->session->get('id_jenis') == 1) {
          $urel =  DOCUMENT_ROOT.'img/sdm/';
        } elseif($this->session->get('id_jenis') == 2) {
          $urel =  DOCUMENT_ROOT.'img/mhs/';
        }
        // Check if the user has uploaded files
        if ($this->request->hasFiles()) {
            $files = $this->request->getUploadedFiles();

            // Print the real file names and sizes
            foreach ($files as $file) {

                $ex = explode('/', $file->getRealType()) ;
                $nama_file = $nip.'.'.$ex[1];

                //validasi men
                if ($this->imageCheck($file->getRealType())) {
                    if ($file->moveTo( $urel.$nama_file)) {
                        $this->db->execute("UPDATE ref_akd_sdm SET `foto`=? WHERE nip = ? ",array($nama_file,$nip));
                        $notif = array(
                            'title' => 'success',
                            'text' => 'Data berhasil di Upload',
                            'type' => 'success',
                        );
                    } else {
                        $notif = array(
                            'title' => 'warning',
                            'text' => "Gagal Upload",
                            'type' => 'warning',
                        );
                    }
                    echo json_encode($notif);
                } else {
                    $notif = array(
                        'title' => 'warning',
                        'text' => "Gagal Upload. File harus Image",
                        'type' => 'warning',
                    );
                    echo json_encode($notif);
                }                
                
            }
        }
    }

    public function gantiLoginAction($value='')
    {
      if ($this->session->get('id_jenis') == 1) {
        $this->view->pick('akd_user/ganti_login_sdm');
      } elseif($this->session->get('id_jenis') == 2) {
        $this->view->pick('akd_user/ganti_login_mhs');
      }
    }

    public function resetSdmAction()
    {
      // $nip          = $this->session->get('nip');
      $id_user      = $this->session->get('id_user');
      $pass_lama    = $_POST["pass_lama"];
      $username     = $_POST["username"];
      $pass_baru    = md5($_POST["pass_baru"]);

      $validation = new Phalcon\Validation(); 
      $validation->add('pass_lama', new PresenceOf(array(
          'message' => 'pass_lama tidak boleh kosong',
      )));
      $validation->add('username', new PresenceOf(array(
          'message' => 'username tidak boleh kosong',
      )));
      $validation->add('pass_baru', new PresenceOf(array(
          'message' => 'pass_baru tidak boleh kosong',
      )));

      $messages = $validation->validate($_POST);
        $pesan = '';
        if (count($messages)) {
            foreach ($messages as $message) {
                $pesan .= "$message"."</br>";
            }
            $notif = array(
                'status' => false,
                'title' => 'warning',
                'text' => $pesan,
                'type' => 'warning',
            );
        }else{
            $user = RefUser::findFirst(
                [
                    "id = :id: AND passwd = :passwd:",
                    "bind" => [
                        "id"        => $id_user,
                        "passwd"    => md5($pass_lama),
                    ]
                ]
            );  
            if ($user !== false) {

              $this->db->execute("UPDATE ref_user SET `uid`=? , `passwd`=? WHERE id = ? ",array($username,$pass_baru,$id_user));
              $notif = array(
                  'status' => true,
              );
            }else{
              $notif = array(
                  'status'  => false,
                  'title'   => 'warning',
                  'text'    => "Password Lama Salah.",
                  'type'    => 'warning',
              );
            }
        }
      echo json_encode($notif);
    }

///////////////////////////////////////////////////////
////////////////////// MAHASISWA ///////////////////////
///////////////////////////////////////////////////////

    public function resetMhsAction()
    {
      $nip = $this->session->get('nip');
      $pass_lama = $_POST["pass_lama"];
      $pass_baru = md5($_POST["pass_baru"]);

      $validation = new Phalcon\Validation(); 
      $validation->add('pass_lama', new PresenceOf(array(
          'message' => 'pass_lama tidak boleh kosong',
      )));

      $validation->add('pass_baru', new PresenceOf(array(
          'message' => 'pass_baru tidak boleh kosong',
      )));

      $messages = $validation->validate($_POST);
        $pesan = '';
        if (count($messages)) {
            foreach ($messages as $message) {
                $pesan .= "$message"."</br>";
            }
            $notif = array(
                'status' => false,
                'title' => 'warning',
                'text' => $pesan,
                'type' => 'warning',
            );
        }else{
            $user = RefUser::findFirst(
                [
                    "nip = :nip: AND passwd = :passwd:",
                    "bind" => [
                        "nip"    => $nip,
                        "passwd" => md5($pass_lama),
                    ]
                ]
            );  
            if ($user !== false) {

              $this->db->execute("UPDATE ref_user SET  `passwd`=? WHERE nip = ? ",array($pass_baru,$nip));
              $notif = array(
                  'status' => true,
              );
            }else{
              $notif = array(
                  'status' => false,
                  'title' => 'warning',
                  'text' => "Salah Memasukkan Password Lama .",
                  'type' => 'warning',
              );
            }
        }
      echo json_encode($notif);
    }

    public function searchNamaAction($id_jenis)
    {
        if ($id_jenis != '' || $id_jenis != 0) {
            // ambil list yg sdh terdaftar
            if ($id_jenis == 1) {
                $user = ViewUser::find([
                    "columns" => "nip",
                    "conditions" => "id_jenis = 1"
                ])->toArray();
            } else {
                $user = ViewUser::find([
                    "columns" => "nip",
                    "conditions" => "id_jenis = 2"
                ])->toArray();
            }

            $list = '';
            for ($i = 0; $i < count($user); $i++) {
                $list .= "'".$user[$i]['nip'] . "',";
            }
            $list = substr($list, 0, -1);                      

            if ($id_jenis == 1) {
                $data = RefAkdSdm::find([
                    "columns" => "nip AS id, nama AS text, foto",
                    "conditions" => "nip NOT IN ($list)",
                    "order" => "nama"
                ]);
            } else {
                $data = RefAkdMhs::find([
                    "columns" => "nis AS id, nama AS text, foto",
                    "conditions" => "nis NOT IN ($list)",
                    "order" => "nama"
                ]);
            }

            echo json_encode($data->toArray());
            
            $this->view->disable();
        }
    } 
    
}
