<?php

use Phalcon\Mvc\View;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Mvc\Url;

class DashboardController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function homeAction()
    {
        $date 	    = date('Y-m-d');
        $id_kelas   = $this->session->get('ps_id');
        if ($id_kelas == 0) {
            $param_kelas = "";
        } else {
            $param_kelas = "AND kelas = $id_kelas";
        }
        // data rekap per hari ini
        $query          = " SELECT
                                SUM(total) as jml_pendapatan,
                                COUNT(id_transaksi) AS jml_transaksi,
                                SUM(IF(is_proses = 0, 1, 0)) AS waiting_list
                            FROM KeuTransaksiCafe 
                                WHERE DATE_FORMAT(tgl_transaksi, '%Y-%m-%d') = '$date'
                                $param_kelas
                                AND stts_transaksi != 3";
        $dt_hari_ini    = $this->modelsManager->executeQuery($query);

        // list orderan
        $dt_record  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('RefUser','a.id_user = b.id','b')
            ->columns(['
                        a.id_transaksi, a.tgl_transaksi, a.total, a.is_proses,
                        b.nama
                        '])
            ->where("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date' $param_kelas")
            ->andWhere("a.stts_transaksi != 3")
            ->andWhere("is_proses = 0 or is_proses = 1")
            ->orderBy("a.id_transaksi DESC")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "dt_record" 	=> $dt_record,
            "dt_hari_ini" 	=> $dt_hari_ini
        ]);

        $this->view->pick('index/dashboard');
    }

    public function detailAction($id_transaksi)
    {
        $dt_menu  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuCafe','a.id_item = b.id','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->andWhere("a.is_paket = 0")
            ->orderBy("b.nama ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $dt_paket  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuPaket','a.id_item = b.id_paket','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nm_paket as nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->andWhere("a.is_paket = 1")
            ->orderBy("b.nm_paket ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $this->view->setVars([
            "dt_record" 	=> array_merge($dt_menu, $dt_paket),
        ]);

        $this->view->pick("index/detail_transaksi");
    }

    public function listViewAction()
    {
        $date 	    = date('Y-m-d');
        // list orderan
        $dt_record  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('RefUser','a.id_user = b.id','b')
            ->columns(['
                        a.id_transaksi, a.tgl_transaksi, a.total, a.is_proses,
                        b.nama
                        '])
            ->where("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
            ->andWhere("a.stts_transaksi != 3")
            ->andWhere("is_proses = 0 or is_proses = 1")
            ->orderBy("a.id_transaksi DESC")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "dt_record" 	=> $dt_record
        ]);

        $this->view->pick('index/view_index');
    }

    public function prosesAction()
    {
        $post = $this->request->getPost();

        $dt_transaksi = KeuTransaksiCafe::findFirst($post['id_transaksi']);
        $dt_transaksi->assign($post);
        if ($dt_transaksi->save()) {
            $notif = [
                'title' => 'Success', 
                'text'  => 'Data berhasil diproses', 
                'type'  => 'success'
            ];
        } else {
	        
	        $notif = [
                'title' => 'Danger', 
                'text'  => 'Data gagal diproses', 
                'type'  => 'danger'
            ];
        
        }
	    
        return json_encode($notif); 
    }


}


