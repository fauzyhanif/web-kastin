<?php

use Phalcon\Mvc\View;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Mvc\Url;

class MMenuCafeController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function indexAction($id, $jenis = '')
    {
    	if ($jenis != '') {
    		$data = MMenuCafe::find([
	        	"conditions" => "id_unit = $id and jenis = '$jenis'",
	            "order" => "jenis, nama ASC"
	        ]);
    	} else {
	        $data = MMenuCafe::find([
	        	"conditions" => "id_unit = $id",
	            "order" => "jenis, nama ASC"
	        ]);
    	}

        $this->view->jenis 		= $jenis;
        $this->view->id_unit 	= $id;
        $this->view->data 		= $data;
        $this->view->pick('m_menu_cafe/index');
    }

    public function createAction()
    { 
	    $post 			= $this->request->getPost();
	    $post['harga'] 	= str_replace(".", "", $post['harga']);
	    $post['premi'] 	= str_replace(".", "", $post['premi']);

        $validation = new Phalcon\Validation(); 
		$validation->add('nama', new PresenceOf(array(
		    'message' => 'Nama Menu tidak boleh kosong'
		)));
		$validation->add('jenis', new PresenceOf(array(
		    'message' => 'Jenis Menu tidak boleh kosong'
		)));
		$validation->add('harga', new PresenceOf(array(
		    'message' => 'Harga tidak boleh kosong'
		)));

		$messages = $validation->validate($_POST);
		$pesan = '';

		//jika gagal falidasi
		if (count($messages)) {
		    foreach ($messages as $message) {
		        $pesan .= "$message"."</br>";
		    }
			$notif = array(
				'title' => 'warning',
				'text' => $pesan,
				'type' => 'warning',
			);

		}else{    
	        
	        $data = new MMenuCafe();
	        $data->assign($post);        
	        if ($data->save()) {

				$fileName = 'default.png';
				$urel =  DOCUMENT_ROOT.'img/produk/';
				if ($this->request->hasFiles() == true) {
					foreach ($this->request->getUploadedFiles() as $file) {
						if ($file->getSize() > 0) {
							$fileName = $data->id.'.'.$file->getExtension();
							$file->moveTo($urel . $fileName);
						}   
					}
				}

				$update_data = MMenuCafe::findFirst($data->id);
				$update_data->assign(["foto" => "$fileName"]);
				$update_data->save();
			}
	        
	        $notif = [
				'title' => 'Success', 
				'text'  => 'Data berhasil disimpan', 
				'type'  => 'success'
			];
	    }
        return json_encode($notif);        
    }

    public function updateAction($id)
    {
    	$post 			= $this->request->getPost();
	    $post['harga'] 	= str_replace(".", "", $post['harga']);
	    $post['premi'] 	= str_replace(".", "", $post['premi']);
        $validation = new Phalcon\Validation(); 
		$validation->add('nama', new PresenceOf(array(
		    'message' => 'Nama Menu tidak boleh kosong'
		)));
		$validation->add('jenis', new PresenceOf(array(
		    'message' => 'Jenis Menu tidak boleh kosong'
		)));
		$validation->add('harga', new PresenceOf(array(
		    'message' => 'Harga tidak boleh kosong'
		)));

		$messages = $validation->validate($_POST);
		$pesan = '';

		//jika gagal falidasi
		if (count($messages)) {
		    foreach ($messages as $message) {
		        $pesan .= "$message"."</br>";
		    }
			$notif = array(
				'title' => 'warning',
				'text' => $pesan,
				'type' => 'warning',
			);

		}else{   
	        
	        $data 			= MMenuCafe::findFirst("id = $id");	                
			$urel       	=  DOCUMENT_ROOT.'img/produk/';
			
			
			if ($this->request->hasFiles() == true) {
				foreach ($this->request->getUploadedFiles() as $file) {
					if ($file->getSize() > 0) {
						if ($data->foto != 'default.png') {
							unlink($urel.$data->foto);
						}
						$post['foto'] = $data->id.'.'.$file->getExtension();
						$file->moveTo($urel . $post['foto']);
					} else {
						$post['foto'] = $data->foto;
					}  
				}
			}

			$data->assign($post);
			$data->save();
	        
	        $notif = [
				'title' => 'Success', 
				'text'  => 'Data berhasil diubah', 
				'type'  => 'success'
			];
	    }

        return json_encode($notif);     
    }    

    public function deleteAction()
    {
        $id     = $this->request->getPost('id');
        $del    = MMenuCafe::findFirst("id_wahana = $id");
        $del->delete();

        $notif = array ('title' => 'Success', 
                        'text'  => 'Data berhasil dihapus', 
                        'type'  => 'success'
                        );

        return json_encode($notif);
    }

   
}

