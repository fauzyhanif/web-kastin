<?php

use Phalcon\Mvc\View;

class ReportTransactionController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction()
    {
        $date 	    = date('Y-m-d');
            
        $data_header = $this->modelsManager->createBuilder()
        ->addFrom('KeuTransaksiCafe', 'a')
        ->columns(["
                    DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') as tgl,
                    sesi
                    "])
        ->where("a.stts_transaksi != 3")
        ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
        ->orderBy("tgl,sesi ASC")
        ->groupBy("tgl,sesi")
        ->getQuery()
        ->execute();

        $data_row = $this->modelsManager->createBuilder()
        ->addFrom('KeuTransaksiCafe', 'a')
        ->innerJoin('RefUser','a.id_user = b.id','b')
        ->columns(["
                    DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') as tgl,
                    sesi,
                    kelas,
                    sum(total-premi) as total,
                    b.nama
                    "])
        ->where("a.stts_transaksi != 3")
        ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
        ->orderBy("tgl,sesi ASC")
        ->groupBy("tgl,sesi,kelas")
        ->getQuery()
        ->execute();

        $data_kelas = $this->modelsManager->createBuilder()
        ->addFrom('MKelas', 'a')
        ->columns([" a.id,a.nama "])
        ->where("a.aktif = 1")
        ->getQuery()
        ->execute();

        $str_column_count = join(",",range(0,count($data_header) + 1));
        $data_user = RefUser::find();
        $this->view->setVars([
            "data_header" 	=> $data_header,
            "data_kelas" 	=> $data_kelas,
            "data_row" 	=> $data_row,
            "str_column_count" 	=> $str_column_count,
        ]);
    }

    public function filterAction()
    {
		$post           = $this->request->getPost();
        $date           = explode(' - ', $post['tgl_transaksi']);
        

            
        $data_header = $this->modelsManager->createBuilder()
        ->addFrom('KeuTransaksiCafe', 'a')
        ->columns(["
                    DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') as tgl,
                    sesi
                    "])
        ->where("a.stts_transaksi != 3")
        ->andWhere(
            "DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
            [
                "min" => $date[0],
                "max" => $date[1],
            ],
            [
                "min" => PDO::PARAM_STR,
                "max" => PDO::PARAM_STR,
            ]
        )
        ->orderBy("tgl,sesi ASC")
        ->groupBy("tgl,sesi")
        ->getQuery()
        ->execute();

        $data_row = $this->modelsManager->createBuilder()
        ->addFrom('KeuTransaksiCafe', 'a')
        ->innerJoin('RefUser','a.id_user = b.id','b')
        ->columns(["
                    DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') as tgl,
                    sesi,
                    kelas,
                    sum(total-premi) as total,
                    b.nama
                    "])
        ->where("a.stts_transaksi != 3")
        ->andWhere(
            "DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
            [
                "min" => $date[0],
                "max" => $date[1],
            ],
            [
                "min" => PDO::PARAM_STR,
                "max" => PDO::PARAM_STR,
            ]
        )
        ->orderBy("tgl,sesi ASC")
        ->groupBy("tgl,sesi,kelas")
        ->getQuery()
        ->execute();

        $data_kelas = $this->modelsManager->createBuilder()
        ->addFrom('MKelas', 'a')
        ->columns([" a.id,a.nama "])
        ->where("a.aktif = 1")
        ->getQuery()
        ->execute();

        $str_column_count = join(",",range(0,count($data_header) + 1));
        
        $this->view->setVars([
            "data_header" 	=> $data_header,
            "data_kelas" 	=> $data_kelas,
            "data_row" 	=> $data_row,
            "str_column_count" 	=> $str_column_count,
        ]);


		$this->view->pick('report_transaction/result_filter');

    }
}

