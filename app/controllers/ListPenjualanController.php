<?php

use Phalcon\Mvc\View;

class ListPenjualanController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction($id_unit)
    {
        $id_kelas       = $this->session->get('ps_id');
        $param_kelas    = '';

        if ($id_kelas != 0) {
            $param_kelas = "and a.kelas = $id_kelas";
        }

        $date 	    = date('Y-m-d');
        $dt_record  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('RefUser','a.id_user = b.id','b')
            ->innerJoin('MKelas','a.kelas = c.id','c')
            ->columns(['
                        a.id_transaksi, a.tgl_transaksi, a.total,
                        b.nama,
                        c.nama as nm_kelas
                        '])
            ->where("a.id_unit = $id_unit $param_kelas")
            ->andWhere("a.stts_transaksi != 3")
            ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
            ->orderBy("a.tgl_transaksi DESC")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "id_unit" 		=> $id_unit,
            "dt_record" 	=> $dt_record,
        ]);
    }

    public function filterAction()
    {
		$post           = $this->request->getPost();
        $date           = explode(' - ', $post['tgl_transaksi']);
        $id_kelas       = $this->session->get('ps_id');
        $param_kelas    = '';

        if ($id_kelas != 0) {
            $param_kelas = "and a.kelas = $id_kelas";
        }

		$dt_record = $this->modelsManager->createBuilder()
                ->addFrom('KeuTransaksiCafe', 'a')
                ->innerJoin('RefUser','a.id_user = b.id','b')
                ->innerJoin('MKelas','a.kelas = c.id','c')
                ->columns(['
                            a.id_transaksi, a.tgl_transaksi, a.total,
                            b.nama,
                            c.nama as nm_kelas
                        '])
                ->where("a.id_unit = '$post[id_unit]' $param_kelas")
                ->andWhere("a.stts_transaksi != 3")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->orderBy("a.tgl_transaksi DESC")
				->getQuery()
				->execute();

		$this->view->setVars([
            "dt_record"     => $dt_record,
            "tgl_transaksi" => $post['tgl_transaksi'],
        ]);

		$this->view->pick('list_penjualan/result_filter');

    }

    public function detailAction($id_transaksi)
    {
        $dt_menu  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuCafe','a.id_item = b.id','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->andWhere("a.is_paket = 0")
            ->orderBy("b.nama ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $dt_paket  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuPaket','a.id_item = b.id_paket','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nm_paket as nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->andWhere("a.is_paket = 1")
            ->orderBy("b.nm_paket ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $this->view->setVars([
            "dt_record" 	=> array_merge($dt_menu, $dt_paket),
        ]);

        $this->view->pick("list_penjualan/view_detail_penjualan");
    }

    /* rekap di gudang */
    public function indexGudangAction()
    {
        $tgl_transaksi      = date('Y-m-d');
        $this->session->set("ses_tgl_transaksi", date('Y-m-d - Y-m-d'));

        $dt_record      = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('MKelas','a.kelas = b.id','b')
            ->columns(['
                        SUM(a.total) as total, a.kelas,
                        b.nama as nm_kelas
                        '])
            ->where("a.stts_transaksi != 3")
            ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$tgl_transaksi'")
            ->orderBy("b.id ASC")
            ->groupBy("a.kelas")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "dt_record" 	=> $dt_record,
        ]);

        $this->view->pick("list_penjualan/index_gudang");
    }

    public function filterGudangAction()
    {
		$post           = $this->request->getPost();
        $tgl_transaksi  = explode(' - ', $post['tgl_transaksi']);
        $this->session->set("ses_tgl_transaksi", $post['tgl_transaksi']);

		$dt_record = $this->modelsManager->createBuilder()
                ->addFrom('KeuTransaksiCafe', 'a')
                ->innerJoin('MKelas','a.kelas = b.id','b')
                ->columns(['
                            SUM(a.total) as total, a.kelas,
                            b.nama as nm_kelas
                            '])
                ->where("a.stts_transaksi != 3")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $tgl_transaksi[0],
						"max" => $tgl_transaksi[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->orderBy("b.id ASC")
                ->groupBy("a.kelas")
				->getQuery()
				->execute();

		$this->view->setVars([
            "dt_record" 		=> $dt_record,
            "tgl_transaksi" 	=> $tgl_transaksi,
        ]);

		$this->view->pick('list_penjualan/result_filter_gudang');

    }

    public function listPenjualanPerKantinAction($id_kelas)
    {
        $tgl_transaksi  = explode(' - ', $this->session->get('ses_tgl_transaksi'));

        $dt_record = $this->modelsManager->createBuilder()
                ->addFrom('KeuTransaksiCafe', 'a')
                ->innerJoin('RefUser','a.id_user = b.id','b')
                ->innerJoin('MKelas','a.kelas = c.id','c')
                ->columns(['
                            a.id_transaksi, a.tgl_transaksi, a.total,
                            b.nama,
                            c.nama as nm_kelas
                        '])
                ->where("a.kelas = $id_kelas")
                ->andWhere("a.stts_transaksi != 3")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $tgl_transaksi[0],
						"max" => $tgl_transaksi[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->orderBy("a.tgl_transaksi DESC")
				->getQuery()
				->execute();

		$this->view->setVars([
            "dt_record" 		=> $dt_record,
            "tgl_transaksi" 	=> date("d-m-Y", strtotime($tgl_transaksi[0])) . " s/d " . date("d-m-Y", strtotime($tgl_transaksi[1])),
        ]);

		$this->view->pick('list_penjualan/list_penjualan_per_kantin');
    }
}

