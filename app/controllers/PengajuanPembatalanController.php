<?php

use Phalcon\Mvc\View;

class PengajuanPembatalanController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction($id_unit)
    {
        $date 	    = date('Y-m-d');
        $dt_record  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('RefUser','a.id_user = b.id','b')
            ->columns(['
                        a.id_transaksi, a.tgl_transaksi, a.total, a.stts_transaksi,
                        b.nama
                        '])
            ->where("a.id_unit = $id_unit")
            ->andWhere("a.stts_transaksi != 1")
            ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
            ->orderBy("a.tgl_transaksi DESC")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "id_unit" 		=> $id_unit,
            "dt_record" 	=> $dt_record,
        ]);
    }

    public function listViewAction()
    {
        $date 	    = date('Y-m-d');
        $dt_record  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiCafe', 'a')
            ->innerJoin('RefUser','a.id_user = b.id','b')
            ->columns(['
                        a.id_transaksi, a.tgl_transaksi, a.total, a.stts_transaksi,
                        b.nama
                        '])
            ->andWhere("a.stts_transaksi != 1")
            ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
            ->orderBy("a.tgl_transaksi DESC")
            ->getQuery()
            ->execute();
        
        $this->view->setVars([
            "id_unit" 		=> $id_unit,
            "dt_record" 	=> $dt_record,
        ]);

        $this->view->pick('pengajuan_pembatalan/result_filter');
    }

    public function filterAction()
    {
		$post = $this->request->getPost();
		$date = explode(' - ', $post['tgl_transaksi']);

		$dt_record = $this->modelsManager->createBuilder()
                ->addFrom('KeuTransaksiCafe', 'a')
                ->innerJoin('RefUser','a.id_user = b.id','b')
                ->columns(['
                            a.id_transaksi, a.tgl_transaksi, a.total, a.stts_transaksi,
                            b.nama
                        '])
                ->where("a.id_unit = '$post[id_unit]'")
                ->andWhere("a.stts_transaksi != 1")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->orderBy("a.tgl_transaksi DESC")
				->getQuery()
				->execute();

		$this->view->setVars([
            "dt_record" 		=> $dt_record,
        ]);

		$this->view->pick('pengajuan_pembatalan/result_filter');

    }

    public function detailAction($id_transaksi)
    {
        $dt_cafe  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuCafe','a.id_item = b.id','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->orderBy("b.nama ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $dt_paket  = $this->modelsManager->createBuilder()
            ->addFrom('KeuTransaksiDtlCafe', 'a')
            ->innerJoin('MMenuPaket','a.id_item = b.id_paket','b')
            ->columns(['
                        a.qty, a.harga_satuan, a.harga_total,
                        b.nm_paket as nama
                        '])
            ->where("a.id_transaksi = '$id_transaksi'")
            ->andWhere("a.is_paket = 1")
            ->orderBy("b.nm_paket ASC")
            ->getQuery()
            ->execute()
            ->toArray();

        $this->view->setVars([
            "dt_record" 	=> array_merge($dt_menu, $dt_paket)
        ]);

        $this->view->pick("pengajuan_pembatalan/view_detail_pengajuan");
    }

    public function prosesAction()
    {
        $post = $this->request->getPost();

        $dt_transaksi = KeuTransaksiCafe::findFirst($post['id_transaksi']);
        $dt_transaksi->assign($post);
        if ($dt_transaksi->save()) {
            $notif = [
                'title' => 'Success', 
                'text'  => 'Data berhasil diproses', 
                'type'  => 'success'
            ];
        } else {
	        
	        $notif = [
                'title' => 'Danger', 
                'text'  => 'Data gagal diproses', 
                'type'  => 'danger'
            ];
        
        }
	    
        return json_encode($notif); 
    }

}

