<?php

use Phalcon\Mvc\View;

class StokBarangController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction($jenis = '')
    {
        $id_kelas = $this->session->get('ps_id');

        if ($jenis != '') {
            $param_jenis = "and a.jenis = '$jenis'"; 
        }

        $dt_stok  = $this->modelsManager->createBuilder()
            ->addFrom('MMenuCafe', 'a')
            ->innerJoin('MStokKantin','a.id = b.id_menu','b')
            ->columns(['
                        a.nama, a.jenis, a.harga,
                        b.stok, b.id
                        '])
            ->where("b.id_kelas = $id_kelas $param_jenis")
            ->orderBy("a.nama ASC")
            ->getQuery()
            ->execute();

        $this->view->jenis      = $jenis;
        $this->view->dt_stok    = $dt_stok;
        
    }

    public function updateStokAction($id)
    {
        $stok = $this->request->getPost('stok');
        $dt_stok = MStokKantin::findFirst($id);
        $dt_stok->assign([
            "stok" => $stok
        ]);
        if ($dt_stok->save()) {
            $notif = array (
                'title' => 'Success', 
                'text'  => 'Data berhasil disimpan', 
                'type'  => 'success'
            );
        } else {
            $notif = array(
                'title' => 'warning',
                'text' 	=> "Gagal",
                'type' 	=> 'warning',
            );
        }

        return json_encode($notif);
    }

}

