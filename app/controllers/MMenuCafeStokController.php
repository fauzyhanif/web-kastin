<?php

use Phalcon\Mvc\View;
use Phalcon\Validation;
class MMenuCafeStokController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function indexAction($id, $jenis = '')
    {
        if ($jenis != '') {
            $dt_stok = $this->modelsManager->createBuilder()
                ->addFrom('MMenuCafeStok', 'a')
                ->leftJoin('MMenuCafe', 'a.id_menu = b.id', 'b')
                ->columns('a.id_stok, a.id_menu, a.qty, a.harga_beli,  a.total, b.nama, b.jenis') 
                ->where("a.id_unit = $id")
                ->andWhere("a.jenis = '$jenis'")
                ->andWhere("MONTH(a.created_at) = MONTH(CURRENT_DATE())")
                ->orderBy("a.id_stok DESC")
                ->getQuery()
                ->execute();
        } else {
            $dt_stok = $this->modelsManager->createBuilder()
                ->addFrom('MMenuCafeStok', 'a')
                ->leftJoin('MMenuCafe', 'a.id_menu = b.id', 'b')
                ->columns('a.id_stok, a.id_menu, a.qty, a.harga_beli,  a.total, b.nama, b.jenis') 
                ->where("a.id_unit = $id")
                ->andWhere("MONTH(a.created_at) = MONTH(CURRENT_DATE())")
                ->orderBy("a.id_stok DESC")
                ->getQuery()
                ->execute();
        }

        $dt_menu = MMenuCafe::find([
            "conditions" => "id_unit = $id",
            "order" => "jenis, nama ASC"
        ]);

        $this->view->id_unit 	= $id;
        $this->view->dt_stok    = $dt_stok;
        $this->view->dt_menu    = $dt_menu;
        $this->view->pick('m_menu_cafe_stok/index');
    }

    public function createAction()
    { 
	    $post 			        = $this->request->getPost();
	    $post['harga_beli'] 	= str_replace(".", "", $post['harga_beli']);
        $post['total'] 	        = $post['qty'] * $post['harga_beli'];
        $post['id_user']        = $this->session->get('id_user');

        $dt_stok = new MMenuCafeStok();
        $dt_stok->assign($post);        

        if ($dt_stok->save()) {
            $notif = [
                'title' => 'Success', 
                'text'  => 'Data berhasil disimpan', 
                'type'  => 'success'
            ];
        } else {
	        
	        $notif = [
                'title' => 'Danger', 
                'text'  => 'Data gagal disimpan', 
                'type'  => 'danger'
            ];
        
        }
	    
        return json_encode($notif);        
    }

    public function updateAction($id_stok)
    { 
	    $post 			        = $this->request->getPost();
	    $post['harga_beli'] 	= str_replace(".", "", $post['harga_beli']);
        $post['total'] 	        = $post['qty'] * $post['harga_beli'];
        $post['id_user']        = $this->session->get('id_user');

        $dt_stok = MMenuCafeStok::findFirst($id_stok);
        $dt_stok->assign($post);        

        if ($dt_stok->save()) {
            $notif = [
                'title' => 'Success', 
                'text'  => 'Data berhasil diubah', 
                'type'  => 'success'
            ];
        } else {
	        
	        $notif = [
                'title' => 'Danger', 
                'text'  => 'Data gagal diubah', 
                'type'  => 'danger'
            ];
        
        }
	    
        return json_encode($notif);        
    }

}

