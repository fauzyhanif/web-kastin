<?php
use \Phalcon\Mvc\Controller;
use \Phalcon\Mvc\View;
use Phalcon\Http\Request;
use Phalcon\Http\Response;


class ApiController extends Controller {

	private $session_login;
	private $MhsController;
	protected $headers    = array();

	public function initialize() {
		$this->authAction();
		$this->view->setRenderLevel(View::LEVEL_NO_RENDER);
	}

	// auth apps
	public function signinAction() {

		$request 	= new Request();

		$userLogin 	= $request->getPost("uid");
		$passwd    	= $request->getPost("password");
		$login 		= '';
		if ($userLogin == '' or $passwd == '') {
			$login = "failed";
		}

		$akun_db = $this->modelsManager->createBuilder()
            ->addFrom('RefUser', 'u')
            ->leftJoin('MUnit', 'u.id_unit = b.id_unit', 'b')
            ->leftJoin('RefAkdSdm', 'd.nip_riil = u.id', 'd')
			->columns([	'u.id','u.passwd','u.uid','u.nama','u.id_unit','u.usergroup', 'u.id_kelas',
						'b.nama as nama_unit', 'b.telp as telp', 'b.alamat as alamat', 'b.hotline as hotline', 
						'd.foto as foto']) 
            ->where("u.uid = '" . $userLogin."'")
            ->getQuery()
            ->execute()
            ->toArray();
		
		if (count($akun_db) > 0) {
			$akun1 = $akun_db[0];
			
			if (count($akun1) > 0) {
				if ($akun1['passwd']!= md5($passwd)) {
					$login="failed";
				}
			}else{
				$login="failed";
			}
		}else{
			$login="failed";
		}

		if ($login=='failed') {
			$result = [
				'success' 		=> false
				, 'message' 	=> 'Username atau password salah.'
				, 'data' 		=> []
			];
			$this->session->remove('user');
			$this->session->remove('userDetail');
		} else {

				$this->session->set('user', $akun1['uid']);
				$user 				= $this->session->get('user');
				$akun1['usergroup']	= str_replace(",", "", $akun1['usergroup']);
				$akun1['usergroup']	= (int) $akun1['usergroup'];
				$akun1['id_unit'] 	= (int) $akun1['id_unit'];
				$akun1['id'] 		= (int) $akun1['id'];
				$result = [
					'success' 	=> true
					, 'message' => 'Login berhasil.'
					, 'data' 	=> [[
									'user' 			=> $user
									, 'id_user' 	=> $akun1['id']
									, 'usergroup' 	=> $akun1['usergroup']
									, 'nama' 		=> $akun1['nama']
									, 'id_unit' 	=> $akun1['id_unit']
									, 'id_kelas' 	=> $akun1['id_kelas']
									, 'nama_unit' 	=> $akun1['nama_unit']
									, 'telp' 		=> $akun1['telp']
									, 'alamat' 		=> $akun1['alamat']
									, 'hotline' 	=> $akun1['hotline']
									, 'foto' 		=> $akun1['foto']
									]]
				];

		}

		echo json_encode($result);
	}
	public function logoutAction() {
		try {
			$request 	= new Request();
	
			$idUser 	= $request->getPost("id_user");
			$kelas 		= $request->getPost("kelas");
			$tgl    	= $request->getPost("tgl");
			$datas = KeuTransaksiCafe::find(["conditions" => "id_user = '$idUser' and kelas = '$kelas' and stts_transaksi=0"]);
			foreach ($datas as $data) {
				$data->assign([
								"tgl_transaksi" => $tgl,
								"stts_transaksi" => 1
								]);
				$data->save();
			}
			$result = [
				'success'	=> true,
				'message' 	=> 'Logout Success',
				'data' 		=> []
			];
			echo json_encode($result);
		} catch (\Exception $e) {
			$result = [
				'success'	=> false,
				'message' 	=> 'Failed',
				'data' 		=> ''
			];
			echo json_encode($result);
		}
	}

	// api key
	public function authAction() {
		$headers 			= getallheaders();
		$headers['Miftah'] 	= '_40jaqHLzBxMQFgdeIuBLIsJBYKsSkNICo4u9Tq9-bTf2NqQzUJ1LRsD0Tvdy4B4A';
		$header 			= $this->request->getHeaders();

		if (empty($header['Miftah'])) {
			$this->accessdeniedAction();
			exit;
		} else {

			if ($headers['Miftah'] == $header['Miftah']) {
				return true;
	    	} else {
				$this->accessdeniedAction();
				exit;
	    	}
		}
	}

	// access control
	public function accessdeniedAction() {
		$result = [
			'response' 	=> true
			, 'code' 	=> '100'
			, 'status' 	=> 'failed'
			, 'detail' 	=> 'you dont have access this data.'
		];
		
		echo json_encode($result);
	}

	// sign out
	public function signoutAction() {
		$this->authAction();
		$user 		= $this->session->get('user');
		$this->session->remove('user');
		$this->session->remove('userDetail');
		$result = [
			'response' 	=> true
			, 'code' 	=> '200'
			, 'status' 	=> 'success'
			, 'detail' 	=> 'signed out. I hope you miss me.'
			, 'user' 	=> $user,
		];

		echo json_encode($result);
	}//end api auth


	// tarif cafe
	public function tarifCafeAction() 
	{
		$this->authAction();
		$id_unit 	= $this->request->get('id_unit');
		$id_kelas 	= $this->request->get('id_kelas');

		$dt_menu = $this->modelsManager->createBuilder()
            ->addFrom('MMenuCafe', 'a')
            ->leftJoin('MMenuCafeStok', 'a.id = b.id_menu and b.qty != 0', 'b')
            ->leftJoin('MStokKantin', 'a.id = c.id_menu', 'c')
			->columns(' a.id, 
						a.jenis,
						a.nama, 
						a.foto,
						MIN(b.id_menu) as x, 
						c.stok, 
						IFNULL(b.harga_beli, 0) AS harga_beli,
						a.harga,
						a.pengurangan_qty,
						false as is_paket') 
            ->where("a.id_unit = $id_unit")
            ->andWhere("c.id_kelas = $id_kelas")
            ->orderBy("a.jenis, a.nama ASC")
            ->groupBy("a.id")
            ->getQuery()
			->execute()
			->toArray();


		$dt_paket = $this->modelsManager->createBuilder()
			->addFrom('MMenuPaket', 'a')
			->leftJoin('MMenuPaketItem', 'a.id_paket = b.id_paket', 'b')
			->leftJoin('MStokKantin', "b.id_menu = c.id_menu and id_kelas = $id_kelas", 'c')
			->columns(
						'a.id_paket as id, 
						"Paket" as jenis, 
						a.nm_paket as nama, 
						"default.png" as foto,
						NULL as x, 
						MIN(IF(c.stok IS NULL, 0, c.stok)) as stok,
						a.harga_modal AS harga_beli, 
						a.harga, 
						"1" as pengurangan_qty,
						true as is_paket
						') 						 
			->orderBy('a.id_paket ASC')
			->groupBy('a.id_paket')
			->where("a.id_unit = $id_unit")
            ->getQuery()
			->execute()
			->toArray();

		$data 	= array();
		$data 	= array_merge($dt_menu,$dt_paket);;

        for ($i=0; $i < count($data) ; $i++) { 
        	$data[$i]['id'] 					= (int) $data[$i]['id']; 
        	$data[$i]['harga'] 					= (int) $data[$i]['harga']; 
        	$data[$i]['harga_beli'] 			= (int) $data[$i]['harga_beli']; 
        	$data[$i]['stok'] 					= (int) $data[$i]['stok']; 
        }

		$result = [ 
			'success' 		=> true
			, 'message' 	=> 'Berhasil.'
			, 'data' 		=> $data,
		];
		
		echo json_encode($result);
	}

	// validasi stok tersedia
	public function validasiStokAction()
	{
		$id_unit 			= $this->request->get('id_unit');
		$id_kelas 			= $this->request->get('id_kelas');
		$id_item 			= $this->request->get('id_item');
		$jenis 				= $this->request->get('jenis'); // penambahan atau pengurangan jumlah order
		$pengurangan_qty 	= $this->request->get('pengurangan_qty');
		$is_paket 			= $this->request->get('is_paket');

		// cari stok
		$dt_item = 	MStokKantin::find(["conditions" => "id_menu = $id_item and id_kelas = $id_kelas"]);
		
			
		if ($pengurangan_qty == 1) {
			
			if ($jenis == 1) {
				if ($is_paket == 1) { //validasi jika item = paket 
					$dt_paket = MMenuPaketItem::find(["conditions" => "id_paket = $id_item"]);
					foreach ($dt_paket as $key) {
						$dt_menu = $this->getStokAction($key->id_menu, $id_kelas);

						// penambahan qty dri android == pengurangan jumlah stok
						$dt_menu->assign(["stok" => $dt_menu->stok - 1]);
						$dt_menu->save();
						$dt_menu->toArray();

						// response
						$result = [ 
							'success' 		=> true
							, 'message' 	=> 'Stok masih tersedia.'
							, 'data' 		=> $dt_menu
						];

					}
				} else { //validasi jika item = menu 
				
					if ($dt_item[0]->stok > 0) { //validasi jika stok masih ada
						// penambahan qty dri android == pengurangan jumlah stok
						$dt_item[0]->assign(["stok" => $dt_item[0]->stok - 1]);
						$dt_item[0]->save();
						$dt_item[0]->toArray();
						// respon
						$result = [ 
							'success' 		=> true
							, 'message' 	=> 'Stok masih tersedia.'
							, 'data' 		=> $dt_item[0]
						];
					} else { //validasi jika stok tidak tersedia

						// respon
						$result = [ 
							'success' 		=> false
							, 'message' 	=> 'Stok tidak mencukupi.'
							, 'data' 		=> (object) []
						];
					}
				}

			} else {
				if ($is_paket == 1) { //validasi jika item = paket 
					$dt_paket = MMenuPaketItem::find(["conditions" => "id_paket = $id_item"]);
					foreach ($dt_paket as $key) {
						$dt_menu = $this->getStokAction($key->id_menu, $id_kelas);

						// pengurangan qty dri android == penambahan jumlah stok
						$dt_menu->assign(["stok" => $dt_menu->stok + 1]);
						$dt_menu->save();
						$dt_menu->toArray();

						// response
						$result = [ 
							'success' 		=> true
							, 'message' 	=> 'Stok masih tersedia.'
							, 'data' 		=> $dt_menu
						];

					}
				} else { //validasi jika item = menu 

					// pengurangan qty dri android == penambahan jumlah stok
					$dt_item[0]->assign(["stok" => $dt_item[0]->stok + 1]);
					$dt_item[0]->save();
					$dt_item[0]->toArray();

					// respon
					$result = [ 
						'success' 		=> true
						, 'message' 	=> 'Stok masih tersedia.'
						, 'data' 		=> $dt_item[0]
					];

				}
			}
		} else {
			// respon
			$result = [ 
				'success' 		=> true
				, 'message' 	=> 'Stok tidak dikurangi.'
				, 'data' 		=> (object) []
			];
		}

		echo json_encode($result);
		
	}

	public function transaksiCafeAction() 
	{
		$this->authAction();
		$post 		= $this->request->getPost();
		
		$content 	= trim(file_get_contents("php://input"));
		$data 		= json_decode($content,true);
		$dataArray 	= $data['data'];
		$postTrnsct = [
						"total" 				=> $data['total'],
						"meja" 					=> $data['meja'],
						"id_user" 				=> $data['id_user'],
						"id_unit" 				=> $data['id_unit'],
						"nominal_bayar" 		=> $data['nominal_bayar'],
						"kelas" 				=> $data['kelas'],
						"sesi"	 				=> $data['sesi'],
						"stts_transaksi"		=> $data['stts_transaksi'],
						"jenis_pelanggan" 		=> $data['jenis_pelanggan'], # 1 = umum, 2 = kru / abk
					];		
		$format="%d-%m-%Y %H:%M:%S";
		$strf=strftime($format);

		if ($postTrnsct['jenis_pelanggan'] == 1) {
			
			// transaksi umum
			$trx_umum = new KeuTransaksiCafe();
			$trx_umum->assign($postTrnsct);

			if ($trx_umum->save()) {

				$last_trx_umum = KeuTransaksiCafe::findFirst($trnsct->id_transaksi);
				$totalPremi = 0;
				for ($i=0; $i < count($dataArray); $i++) {
					if ($dataArray[$i]["pengurangan_qty"] == 1) {	
						# pengurangan stok
						$this->penguranganStokAction($postTrnsct["kelas"], $dataArray[$i]["id_item"], $dataArray[$i]["qty"], $dataArray[$i]["is_paket"]);
					}
					$dataMenu = MMenuCafe::findFirst($dataArray[$i]["id_item"]);
					$totalPremi += $dataMenu->premi * $dataArray[$i]["qty"];
					$dataArray[$i]['id_transaksi'] = $trx_umum->id_transaksi;
					$dataArray[$i]['premi'] = $dataMenu->premi * $dataArray[$i]["qty"];
					$dtl = new KeuTransaksiDtlCafe();
					$dtl->assign($dataArray[$i]);
					$dtl->save();

				}
				$updateData = KeuTransaksiCafe::findFirst($trx_umum->id_transaksi);
				$updateData->premi = $totalPremi;
				$updateData->save();
				$result = [ 
					'success' 		=> true
					, 'message' 	=> 'Berhasil.'
					, 'data' 		=> [["id_transaksi" => (int) $trx_umum->id_transaksi, "tgl_transaksi" => $last_trx_umum->tgl_transaksi]]
				];

			} else {
				$result = [ 
					'success' 		=> false
					, 'message' 	=> 'Gagal.'
					, 'data' 		=> []
				];
			}
		} else {

			// transaksi abk
			$trx_abk = new KeuTransaksiAbk();
			$trx_abk->assign($postTrnsct);

			if ($trx_abk->save()) {
				$last_trx_abk = KeuTransaksiAbk::findFirst($trx_abk->id_transaksi);
				
				for ($i=0; $i < count($dataArray); $i++) {
					if ($dataArray[$i]["pengurangan_qty"] == 1) {	
						# pengurangan stok
						$this->penguranganStokAction($postTrnsct["kelas"], $dataArray[$i]["id_item"], $dataArray[$i]["qty"], $dataArray[$i]["is_paket"]);
					}

					$dataArray[$i]['id_transaksi'] = $trx_abk->id_transaksi;
					$trx_dtl_abk = new KeuTransaksiDtlAbk();
					$trx_dtl_abk->assign($dataArray[$i]);
					$trx_dtl_abk->save();

				}

				$result = [ 
					'success' 		=> true
					, 'message' 	=> 'Berhasil.'
					, 'data' 		=> [["id_transaksi" => (int) $trx_abk->id_transaksi, "tgl_transaksi" => $last_trx_abk->tgl_transaksi]]
				];

			} else {
				$result = [ 
					'success' 		=> false
					, 'message' 	=> 'Gagal.'
					, 'data' 		=> []
				];
			}

			$result = [ 
				'success' 		=> true
				, 'message' 	=> 'Berhasil.'
				, 'data' 		=> [["id_transaksi" => (int) 0, "tgl_transaksi" => $strf]]
			];
		}
		
		echo json_encode($result);
	}

	public function penguranganStokAction($id_kelas, $id_menu, $qty, $is_paket)
	{
		if ($is_paket == 1) {
			$dt_paket = MMenuPaketItem::find(["conditions" => "id_paket = $id_menu"]);
			foreach ($dt_paket as $key) {
				$dt_menu = $this->getStokAction($key->id_menu, $id_kelas);

				$dt_menu->assign(["stok" => $dt_menu->stok - $qty]);
				$dt_menu->save();
				$dt_menu->toArray();
			}
		} else {
			$stok_barang = MStokKantin::find(["conditions" => "id_kelas = $id_kelas and id_menu = $id_menu"]);
			$stok_barang[0]->assign(["stok" => $stok_barang[0]->stok - $qty]);
			$stok_barang[0]->save();
		}
	}

	public function getStokAction($id_menu, $id_kelas)
	{
		$dt_stok = MStokKantin::find(["conditions" => "id_menu = $id_menu and id_kelas = $id_kelas"]);

		return $dt_stok[0];
	}

	public function pengajuanPembatalanAction()
	{
		$id_unit 		= $this->request->get('id_unit');
		$id_user 		= $this->request->get('id_user');
		$id_transaksi 	= $this->request->get('id_transaksi');

		// pengajuan batal table transksi
		$dt_transaksi = KeuTransaksiCafe::findFirst($id_transaksi);
		$dt_transaksi->assign([
			"stts_transaksi" => 3,
			"user_pengajuan_pembatalan" => $id_user
		]);

		if ($dt_transaksi->save()) {
	        $result = [ 
				'success' 		=> true
				, 'message' 	=> 'Berhasil.'
				, 'data' 		=> []
			];
        } else {
        	$result = [ 
				'success' 		=> false
				, 'message' 	=> 'Gagal.'
				, 'data' 		=> []
			];
        }
		
		echo json_encode($result);


	}

	public function rekapHarianAction() 
	{
		$id_unit 	= $this->request->get('id_unit');
		$id_kelas 	= $this->request->get('id_kelas');
		$id_user 	= $this->request->get('id_user');
		$tgl 		= $this->request->get('tgl');

		$dt_cafe = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi=b.id_transaksi','b')
				->innerJoin('MMenuCafe','b.id_item = c.id','c')
				->columns(['c.nama as item, SUM(b.harga_total) as jumlah, SUM(b.qty) as qty, c.premi * SUM(b.qty) as premi, b.id_item'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.id_user = '$id_user'")
				->andWhere("a.stts_transaksi = 0")
				// ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$tgl'")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

		$dt_paket = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi=b.id_transaksi','b')
				->innerJoin('MMenuPaket','b.id_item = c.id_paket','c')
				->columns(['c.nm_paket as item, SUM(b.harga_total) as jumlah, SUM(b.qty) as qty, "0" as premi, b.id_item'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.id_user = '$id_user'")
				->andWhere("a.stts_transaksi = 0")
				// ->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$tgl'")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();
				
		$data = array_merge($dt_cafe, $dt_paket);
		$total_penjualan = 0;
		$total_premi = 0;
		for ($i=0; $i < count($data) ; $i++) { 
			$data[$i]['jumlah'] 	= (int) $data[$i]['jumlah']; 
			$data[$i]['premi'] 		= (int) $data[$i]['premi']; 
			$total_premi 			= $total_premi + (int) $data[$i]['premi'];
			$total_penjualan 		= $total_penjualan + (int) $data[$i]['jumlah'];
		}

		$result = [ 
			'success' 			=> true,
			'message' 			=> 'Berhasil.',
			'total_penjualan' 	=> $total_penjualan,
			'total_premi' 		=> $total_premi,
			'data' 				=> $data
		];
		
		echo json_encode($result);

	}

	public function historiPenjualanAction()
	{
		$id_unit 	= $this->request->get('id_unit');
		$id_kelas 	= $this->request->get('id_kelas');
		$id_user 	= $this->request->get('id_user');
		$tgl 		= $this->request->get('tgl');

		$dt_transaksi = KeuTransaksiCafe::find([
			"conditions" 	=> "DATE_FORMAT(tgl_transaksi, '%Y-%m-%d') = '$tgl' and id_user = '$id_user' and id_unit = '$id_unit' and kelas = '$id_kelas' and stts_transaksi = 1",
			"order" 		=> "tgl_transaksi DESC"
		]);

		$dt_transaksi = $dt_transaksi->toArray();

		for ($i=0; $i < count($dt_transaksi) ; $i++) { 
			$dt_transaksi[$i]['total'] 			= (int) $dt_transaksi[$i]['total']; 
			$dt_transaksi[$i]['nominal_bayar'] 	= (int) $dt_transaksi[$i]['nominal_bayar']; 

			$dt_dtl_transaksi 					= KeuTransaksiDtlCafe::find(["conditions" => "id_transaksi = ".$dt_transaksi[$i]['id_transaksi']])->toArray();
			
			for ($x=0; $x < count($dt_dtl_transaksi) ; $x++) {
				
				$dt_dtl_transaksi[$x]['harga_beli'] 	= (int) $dt_dtl_transaksi[$x]['harga_beli'];
				$dt_dtl_transaksi[$x]['harga_satuan'] 	= (int) $dt_dtl_transaksi[$x]['harga_satuan'];
				$dt_dtl_transaksi[$x]['harga_total'] 	= (int) $dt_dtl_transaksi[$x]['harga_total'];

				$dt_transaksi[$i]['detail'][$x] 		= $dt_dtl_transaksi[$x];
			}

		}

		$result = [ 
			'success' 		=> true,
			'message' 		=> 'Berhasil.',
			'data' 			=> $dt_transaksi
		];
		
		echo json_encode($result);


	}



}
