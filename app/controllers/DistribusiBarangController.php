<?php

use Phalcon\Mvc\View;

class DistribusiBarangController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction()
    {

    }

    public function createDistribusiAction()
    {
        # get data
        $dt_kelas   = MKelas::find(["order" => "nama ASC"]);
        $dt_barang  = MMenuCafe::find(["order" => "nama ASC"]);

        # if post is exist
        if ($this->request->isPost()) {
            $request = $this->request->getPost();
            for ($i=0; $i < count($request['id_menu']); $i++) { 
                
                # cek request qty
                if ($request['stok'][$i] != 0 or $request['stok'][$i] != '') {

                    $notif = '';

                    # cek exist or not
                    $cek_barang_ready = $this->checkBarangReadyAction($request['id_menu'][$i], $request['stok'][$i]);
                    
                    if ($cek_barang_ready <= 0) {
                        $notif = array(
                            'title' => 'Ooopss!',
                            'text'  => 'Jumlah barang kurang untuk didistribusikan',
                            'type'  => 'danger',
                        );
                    } else {
                        # cek exist or not
                        $cek_barang = $this->checkBarangExistAction($request['id_menu'][$i], $request['id_kelas']);
                        
                        if (count($cek_barang) >= 1) { # if exist do Update
                            $cek_barang[0]->assign([
                                "stok" => $cek_barang[0]->stok + $request['stok'][$i]
                            ]);
                            
                            if ($cek_barang[0]->save()) {
                                $notif = array(
                                    'title' => 'Success',
                                    'text' => 'Barang berhasil didistribusikan',
                                    'type' => 'success',
                                );
                            } else {
                                $notif = array(
                                    'title' => 'Ooopss!',
                                    'text' => 'Barang gagal didistribusikan',
                                    'type' => 'danger',
                                );
                            }
                        } else { # if not exist do Insert
                            $stok_baru = new MStokKantin();
                            $stok_baru->assign([
                                "id_kelas"  => $request['id_kelas'],
                                "id_menu"   => $request['id_menu'][$i],
                                "stok"      => $request['stok'][$i]
                            ]);

                            if ($stok_baru->save()) {
                                $notif = array(
                                    'title' => 'Success',
                                    'text' => 'Barang berhasil didistribusikan',
                                    'type' => 'success',
                                );
                            } else {
                                $notif = array(
                                    'title' => 'Ooopss!',
                                    'text' => 'Barang gagal didistribusikan',
                                    'type' => 'danger',
                                );
                            }
                        }

                        # insert into history distribution
                        $this->createHistoryDistributionAction($request['id_kelas'], $request['id_menu'][$i], $request['stok'][$i], $this->session->get('uid'));
                    }
                } 
            }

            return json_encode($notif);
        }

        $this->view->dt_kelas   = $dt_kelas;
        $this->view->dt_barang  = $dt_barang;
        $this->view->pick("distribusi_barang/create_distribusi");
    }

    public function checkBarangReadyAction($id_menu, $stok)
    {
        # query check barang ready or not
        $cek_barang = MMenuCafe::findFirst($id_menu);

        $hasil = $cek_barang->stok - $stok;

        return $hasil;
    }
    
    public function checkBarangExistAction($id_menu, $id_kelas)
    {
        # query check barang exist or not
        $cek_barang = MStokKantin::find([
            "conditions" => "id_kelas = $id_kelas and id_menu = $id_menu"
        ]);

        return $cek_barang;
    }

    public function createHistoryDistributionAction($id_kelas, $id_menu, $stok, $admin)
    {
        $dt_history = new MHistoryDistribusi();
        $dt_history->assign([
            "id_kelas"  => $id_kelas,
            "id_menu"   => $id_menu,
            "stok"      => $stok,
            "admin"     => $admin
        ]);
        
        $dt_history->save();
    }

    public function historyAction()
    {
        $this_day 	    = date('Y-m-d');
        $dt_kelas       = MKelas::find(["order" => "nama ASC"]);
        $dt_history     = $this->modelsManager->createBuilder()
            ->addFrom('MHistoryDistribusi', 'a')
            ->innerJoin('MMenuCafe','a.id_menu = b.id','b')
            ->innerJoin('MKelas','a.id_kelas = c.id','c')
            ->columns(['
                        a.stok, a.admin, a.created_at,
                        b.nama as nm_menu, b.jenis, b.harga,
                        c.nama as nm_kelas
                        '])
            ->where("DATE_FORMAT(a.created_at, '%Y-%m-%d') = '$this_day'")
            ->orderBy("b.nama ASC")
            ->getQuery()
            ->execute();

        $this->view->dt_kelas   = $dt_kelas;
        $this->view->dt_history = $dt_history;
        $this->view->pick("distribusi_barang/history");
    }

    public function filterHistoryAction()
    {
        
        $request    = $this->request->getPost();
        $date       = explode(' - ', $request['created_at']);
        if ($request['id_kelas'] != '') {
            $param_kelas = "and a.id_kelas = '$request[id_kelas]'"; 
        }
        
        $dt_history     = $this->modelsManager->createBuilder()
            ->addFrom('MHistoryDistribusi', 'a')
            ->innerJoin('MMenuCafe','a.id_menu = b.id','b')
            ->innerJoin('MKelas','a.id_kelas = c.id','c')
            ->columns(['
                        a.stok, a.admin, a.created_at,
                        b.nama as nm_menu, b.jenis, b.harga,
                        c.nama as nm_kelas
                        '])
            ->where(
                "DATE_FORMAT(a.created_at, '%Y-%m-%d') between  :min: and :max: $param_kelas",
                [
                    "min" => $date[0],
                    "max" => $date[1],
                ],
                [
                    "min" => PDO::PARAM_STR,
                    "max" => PDO::PARAM_STR,
                ]
            )
            ->orderBy("b.nama ASC")
            ->getQuery()
            ->execute();

        $this->view->dt_history = $dt_history;
        $this->view->pick("distribusi_barang/filter_history");
    }

}

