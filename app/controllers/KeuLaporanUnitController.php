<?php

use Phalcon\Mvc\View;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Mvc\Url;
class KeuLaporanUnitController extends \Phalcon\Mvc\Controller
{
	public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        date_default_timezone_set('Asia/Jakarta');
    }


    public function indexAction($id_unit)
    {
		$id_kelas   = $this->session->get('ps_id');
		$date 	= date('Y-m-d');

		$dt_cafe = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi = b.id_transaksi','b')
				->innerJoin('MMenuCafe','b.id_item = c.id','c')
				->columns(['
							c.nama as item, 
							SUM(b.qty) as qty, 
							c.premi,
							SUM(b.harga_beli * b.qty) as jml_modal, 
							SUM(b.harga_total) as jml_pendapatan,
							SUM(b.harga_total) - SUM(b.harga_beli * b.qty) as jml_keuntungan
							'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.stts_transaksi != 3")
				->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
				->andWhere("b.is_paket = 0")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

			$dt_paket = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi = b.id_transaksi','b')
				->innerJoin('MMenuPaket','b.id_item = c.id_paket','c')
				->columns(['
							c.nm_paket as item, 
							SUM(b.qty) as qty, 
							"0" as premi,
							SUM(b.harga_beli * b.qty) as jml_modal, 
							SUM(b.harga_total) as jml_pendapatan,
							SUM(b.harga_total) - SUM(b.harga_beli * b.qty) as jml_keuntungan
							'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.stts_transaksi != 3")
				->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
				->andWhere("b.is_paket = 1")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

		$dt_user = ViewUser::find(["conditions" => "id_unit = '$id_unit'"]);

		$this->view->setVars([
			"date" 			=> $date,
			"id_unit" 		=> $id_unit,
			"dt_user" 		=> $dt_user,
			"dt_record" 	=> array_merge($dt_cafe, $dt_paket),
		]);

    }

    public function filterAction()
    {
		$post = $this->request->getPost();
		$date = explode(' - ', $post['tgl_transaksi']);
		$id_kelas   = $this->session->get('ps_id');

		$dt_cafe = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi = b.id_transaksi','b')
				->innerJoin('MMenuCafe','b.id_item = c.id','c')
				->columns(['
							c.nama as item, 
							SUM(b.qty) as qty, 
							c.premi,
							SUM(b.harga_beli * b.qty) as jml_modal, 
							SUM(b.harga_total) as jml_pendapatan,
							SUM(b.harga_total) - SUM(b.harga_beli * b.qty) as jml_keuntungan
							'])
				->where("a.id_unit = '$post[id_unit]'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.stts_transaksi != 3")
				->andWhere("b.is_paket = 0")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

			$dt_paket = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiCafe', 'a')
				->innerJoin('KeuTransaksiDtlCafe','a.id_transaksi = b.id_transaksi','b')
				->innerJoin('MMenuPaket','b.id_item = c.id_paket','c')
				->columns(['
							c.nm_paket as item, 
							SUM(b.qty) as qty, 
							"0" as premi,
							SUM(b.harga_beli * b.qty) as jml_modal, 
							SUM(b.harga_total) as jml_pendapatan,
							SUM(b.harga_total) - SUM(b.harga_beli * b.qty) as jml_keuntungan
							'])
				->where("a.id_unit = '$post[id_unit]'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("a.stts_transaksi != 3")
				->andWhere("b.is_paket = 1")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

		$this->view->setVars([
            "dt_record" 	=> array_merge($dt_cafe, $dt_paket),
        ]);

		$this->view->pick('keu_laporan_unit/result_filter');

	}
	
	/* function ABK */
	public function indexAbkAction($id_unit)
    {
		$id_kelas   = $this->session->get('ps_id');
		$date 		= date('Y-m-d');
		// echo $date;
		$dt_cafe = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiAbk', 'a')
				->leftJoin('KeuTransaksiDtlAbk','a.id_transaksi = b.id_transaksi','b')
				->leftJoin('MMenuCafe','b.id_item = c.id','c')
				->columns(['
							a.id_transaksi,
							c.nama as item, 
							SUM(b.qty) as qty
							'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
				->andWhere("b.is_paket = 0")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();
		
			$dt_paket = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiAbk', 'a')
				->leftJoin('KeuTransaksiDtlAbk','a.id_transaksi = b.id_transaksi','b')
				->leftJoin('MMenuPaket','b.id_item = c.id_paket','c')
				->columns(['
							a.id_transaksi,
							c.nm_paket as item, 
							SUM(b.qty) as qty
							'])
				->where("a.id_unit = '$id_unit'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') = '$date'")
				->andWhere("b.is_paket = 1")
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

		$dt_user = ViewUser::find(["conditions" => "id_unit = '$id_unit'"]);

		$this->view->setVars([
			"date" 			=> $date,
			"id_unit" 		=> $id_unit,
			"dt_user" 		=> $dt_user,
			"dt_record" 	=> array_merge($dt_cafe, $dt_paket),
		]);

		$this->view->pick('keu_laporan_unit/index_abk');
	}
	
	public function filterAbkAction()
    {
		$post = $this->request->getPost();
		$date = explode(' - ', $post['tgl_transaksi']);
		$id_kelas   = $this->session->get('ps_id');

		$dt_cafe = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiAbk', 'a')
				->innerJoin('KeuTransaksiDtlAbk','a.id_transaksi = b.id_transaksi','b')
				->innerJoin('MMenuCafe','b.id_item = c.id','c')
				->columns(['
							a.id_transaksi,
							c.nama as item, 
							SUM(b.qty) as qty
							'])
				->where("a.id_unit = '$post[id_unit]'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("b.is_paket = 0")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

			$dt_paket = $this->modelsManager->createBuilder()
				->addFrom('KeuTransaksiAbk', 'a')
				->leftJoin('KeuTransaksiDtlAbk','a.id_transaksi = b.id_transaksi','b')
				->leftJoin('MMenuPaket','b.id_item = c.id_paket','c')
				->columns(['
							a.id_transaksi,
							c.nm_paket as item, 
							SUM(b.qty) as qty
							'])
				->where("a.id_unit = '$post[id_unit]'")
				->andWhere("a.kelas = '$id_kelas'")
				->andWhere("b.is_paket = 1")
				->andWhere(
					"DATE_FORMAT(a.tgl_transaksi, '%Y-%m-%d') between  :min: and :max:",
					[
						"min" => $date[0],
						"max" => $date[1],
					],
					[
						"min" => PDO::PARAM_STR,
						"max" => PDO::PARAM_STR,
					]
				)
				->groupBy("b.id_item")
				->getQuery()
				->execute()
				->toArray();

		$this->view->setVars([
            "dt_record" 	=> array_merge($dt_cafe, $dt_paket),
        ]);

		$this->view->pick('keu_laporan_unit/result_filter_abk');

	}

}

