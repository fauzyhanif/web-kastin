<?php

use Phalcon\Mvc\View;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Mvc\Url;

class MMenuPaketController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        if (empty($this->session->get('uid'))) {
            $this->response->redirect('account/loginEnd');
        }
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function indexAction($id_unit)
    {
        $dt_paket = MMenuPaket::find([
            "conditions" 	=> "id_unit = $id_unit and aktif = 'Y'",
            "order" 		=> "nm_paket ASC"
        ]);

		$dt_menu = MMenuCafe::find([
			"conditions" 	=> "id_unit = $id_unit and aktif = 'Y'",
			"order" 		=> "nama ASC"
		]);
        $this->view->id_unit 	= $id_unit;
        $this->view->dt_paket 	= $dt_paket;
        $this->view->dt_menu 	= $dt_menu;
        $this->view->pick('m_menu_paket/index');
	}
	
	public function createAction()
	{
		$post 					= $this->request->getPost();
		$post['harga'] 			= str_replace(".", "", $post['harga']);
		$post['harga_modal'] 	= str_replace(".", "", $post['harga_modal']);

		$dt_paket = new MMenuPaket();
		$dt_paket->assign($post);
		if ($dt_paket->save()) {
			for ($i=0; $i < count($post['id_menu']); $i++) { 
				$dt_paket_item = new MMenuPaketItem();
				$dt_paket_item->assign([
					"id_paket" => $dt_paket->id_paket,
					"id_menu"  => $post['id_menu'][$i]
				]);

				if ($dt_paket_item->save()) {
					$notif = array (
						'title' => 'Success', 
						'text'  => 'Data berhasil disimpan', 
						'type'  => 'success'
					);
				} else {
					$notif = array(
						'title' => 'warning',
						'text' 	=> "Gagal 2",
						'type' 	=> 'warning',
					);
				}
			}
		} else {
			$notif = array(
				'title' => 'warning',
				'text' 	=> "Gagal 1",
				'type' 	=> 'warning',
			);
		}

		return json_encode($notif);
	}

	public function getItemAction($id_paket)
	{
		$dt_paket_item 	= MMenuPaketItem::find(["conditions" => "id_paket = '$id_paket'"]);
		$dt_menu 		= MMenuCafe::find([
			"conditions" 	=> "aktif = 'Y'",
			"order" 		=> "nama ASC"
		]);

		$this->view->dt_paket_item 	= $dt_paket_item;
		$this->view->dt_menu 		= $dt_menu;
		$this->view->pick("m_menu_paket/listItem");
	}

	public function updateAction($id_paket)
	{
		$post 					= $this->request->getPost();
		$post['harga'] 			= str_replace(".", "", $post['harga']);
		$post['harga_modal'] 	= str_replace(".", "", $post['harga_modal']);

		$dt_paket = MMenuPaket::findFirst($id_paket);
		$dt_paket->assign($post);
		if ($dt_paket->save()) {
			if (isset($post['id_menu'])) {
			
				foreach ($post['id_menu'] as $key => $value) {
					$dt_paket_item = MMenuPaketItem::findFirst($key);
					$dt_paket_item->assign([
						"id_paket" => $id_paket,
						"id_menu"  => $post['id_menu'][$key]
					]);

					if ($dt_paket_item->save()) {
						$notif = array (
							'title' => 'Success', 
							'text'  => 'Data berhasil disimpan', 
							'type'  => 'success'
						);
					} else {
						$notif = array(
							'title' => 'warning',
							'text' 	=> "Gagal",
							'type' 	=> 'warning',
						);
					}
				}
			}

				
			for ($i=0; $i < count($post['id_menu_tambah']); $i++) { 
				if ($post['id_menu_tambah'][$i] != '') {
				
					$dt_paket_item = new MMenuPaketItem();
					$dt_paket_item->assign([
						"id_paket" => $id_paket,
						"id_menu"  => $post['id_menu_tambah'][$i]
					]);

					if ($dt_paket_item->save()) {
						$notif = array (
							'title' => 'Success', 
							'text'  => 'Data berhasil disimpan', 
							'type'  => 'success'
						);
					} else {
						$notif = array(
							'title' => 'warning',
							'text' 	=> "Gagal",
							'type' 	=> 'warning',
						);
					}
				}
			}
			
		} else {
			$notif = array(
				'title' => 'warning',
				'text' 	=> "Gagal",
				'type' 	=> 'warning',
			);
		}

		return json_encode($notif);
	}

	public function deleteAction()
	{
		$id_paket = $this->request->getPost('id_paket');
		$dt_paket = MMenuPaket::findFirst($id_paket);
		if ($dt_paket->delete()) {

			$dt_paket_item = MMenuPaketItem::find("id_paket = $id_paket");
			if ($dt_paket_item->delete()) {
				$notif = array (
					'title' => 'Success', 
					'text'  => 'Data berhasil disimpan', 
					'type'  => 'success'
				);
			} else {
				$notif = array(
					'title' => 'warning',
					'text' 	=> "Gagal",
					'type' 	=> 'warning',
				);	
			}
		} else {
			$notif = array(
				'title' => 'warning',
				'text' 	=> "Gagal",
				'type' 	=> 'warning',
			);
		}

		return json_encode($notif);
	}

	public function deleteItemAction($id_paket_item)
	{
		$dt_paket_item = MMenuPaketItem::findFirst($id_paket_item);
		if ($dt_paket_item->delete()) {
			$notif = array (
				'title' => 'Success', 
				'text'  => 'Data berhasil disimpan', 
				'type'  => 'success'
			);
		} else {
			$notif = array(
				'title' => 'warning',
				'text' 	=> "Gagal",
				'type' 	=> 'warning',
			);	
		}

		return json_encode($notif);
	}
 
   
}

