<?php

class KeuBtlTransaksiCafe extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id_transaksi;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $tgl_transaksi;

    /**
     *
     * @var integer
     * @Column(type="integer", length=20, nullable=false)
     */
    public $total;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $meja;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=false)
     */
    public $id_user;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id_unit;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=true)
     */
    public $user_cancel;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $cancel_at;

    /**
     * Initialize method for model.
     */


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'keu_btl_transaksi_cafe';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuBtlTransaksiCafe[]|KeuBtlTransaksiCafe
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuBtlTransaksiCafe
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
