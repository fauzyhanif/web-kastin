<?php

class MMenuPaketItem extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_paket_item;

    /**
     *
     * @var integer
     */
    public $id_paket;

    /**
     *
     * @var integer
     */
    public $id_menu;
    
    /**
     *
     * @var string
     */
    public $aktif;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("kastin");
        $this->setSource("m_menu_paket_item");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_menu_paket_item';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MMenuPaketItem[]|MMenuPaketItem|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MMenuPaketItem|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
