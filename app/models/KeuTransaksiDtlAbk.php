<?php

class KeuTransaksiDtlAbk extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $id_transaksi;

    /**
     *
     * @var integer
     */
    public $id_item;

    /**
     *
     * @var integer
     */
    public $harga_beli;

    /**
     *
     * @var integer
     */
    public $harga_satuan;

    /**
     *
     * @var integer
     */
    public $qty;

    /**
     *
     * @var integer
     */
    public $harga_total;

    /**
     *
     * @var integer
     */
    public $is_paket;

    /**
     *
     * @var integer
     */
    public $stts_transaksi;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("keu_transaksi_dtl_abk");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'keu_transaksi_dtl_abk';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiDtlAbk[]|KeuTransaksiDtlAbk|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiDtlAbk|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
