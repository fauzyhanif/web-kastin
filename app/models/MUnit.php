<?php

class MUnit extends \Phalcon\Mvc\Model
{


    public $id_unit;
    public $nama;
    public $telp;
    public $alamat;
    public $hotline;
    public $aktif;

    public function getSource()
    {
        return 'm_unit';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefAkdRuang[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefAkdRuang
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
