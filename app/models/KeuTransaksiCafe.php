<?php

class KeuTransaksiCafe extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id_transaksi;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $tgl_transaksi;

    /**
     *
     * @var integer
     * @Column(type="integer", length=20, nullable=false)
     */
    public $total;

    /**
     *
     * @var integer
     * @Column(type="integer", length=6, nullable=false)
     */
    public $id_user;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id_unit;
    public $is_proses;

    /**
     * Initialize method for model.
     */


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'keu_transaksi_cafe';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiCafe[]|KeuTransaksiCafe
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiCafe
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
