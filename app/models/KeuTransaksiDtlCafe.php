<?php

class KeuTransaksiDtlCafe extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id_transaksi;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $id_item;

    /**
     *
     * @var integer
     * @Column(type="integer", length=20, nullable=false)
     */
    public $harga_satuan;

    /**
     *
     * @var integer
     * @Column(type="integer", length=5, nullable=false)
     */
    public $qty;

    /**
     *
     * @var integer
     * @Column(type="integer", length=20, nullable=false)
     */
    public $harga_total;
    
    /**
     *
     * @var integer
     * @Column(type="integer", length=20, nullable=false)
     */
    public $is_paket;

    /**
     * Initialize method for model.
     */

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'keu_transaksi_dtl_cafe';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiDtlCafe[]|KeuTransaksiDtlCafe
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiDtlCafe
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
