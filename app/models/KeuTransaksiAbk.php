<?php

class KeuTransaksiAbk extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_transaksi;

    /**
     *
     * @var string
     */
    public $tgl_transaksi;

    /**
     *
     * @var integer
     */
    public $total;

    /**
     *
     * @var integer
     */
    public $nominal_bayar;

    /**
     *
     * @var string
     */
    public $meja;

    /**
     *
     * @var string
     */
    public $kelas;

    /**
     *
     * @var integer
     */
    public $id_user;

    /**
     *
     * @var integer
     */
    public $id_unit;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var integer
     */
    public $stts_transaksi;

    /**
     *
     * @var integer
     */
    public $user_pengajuan_pembatalan;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("keu_transaksi_abk");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'keu_transaksi_abk';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiAbk[]|KeuTransaksiAbk|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return KeuTransaksiAbk|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
