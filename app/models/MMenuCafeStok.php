<?php

class MMenuCafeStok extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_stok;

    /**
     *
     * @var integer
     */
    public $id_unit;

    /**
     *
     * @var integer
     */
    public $id_menu;

    /**
     *
     * @var integer
     */
    public $qty;

    /**
     *
     * @var integer
     */
    public $harga_beli;

    /**
     *
     * @var integer
     */
    public $total;

    /**
     *
     * @var string
     */
    public $id_user;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("kastin");
        $this->setSource("m_menu_cafe_stok");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'm_menu_cafe_stok';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MMenuCafeStok[]|MMenuCafeStok|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MMenuCafeStok|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
