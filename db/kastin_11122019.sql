-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 11 Des 2019 pada 11.27
-- Versi Server: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kastin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `acl`
--

CREATE TABLE `acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(32) DEFAULT NULL,
  `action` varchar(32) DEFAULT NULL,
  `area` varchar(128) DEFAULT NULL,
  `parent` varchar(128) NOT NULL DEFAULT '0',
  `label_menu` varchar(128) DEFAULT NULL,
  `child` varchar(128) DEFAULT NULL,
  `usergroup_id` varchar(128) DEFAULT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `except_user` varchar(128) DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `urut` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `acl`
--

INSERT INTO `acl` (`id`, `controller`, `action`, `area`, `parent`, `label_menu`, `child`, `usergroup_id`, `icon`, `except_user`, `aktif`, `urut`) VALUES
(22, 'acl', '', ',1,', '0', 'Access Control List', '', ',1,', 'fa-cogs', '', 'Y', 99),
(81, 'adminsetup', 'webGroup', '', '79', 'Usergroup', '', ',1,2,', 'fa-users', '', 'Y', 2),
(80, 'adminsetup', 'webArea', '', '79', 'Menu Area', '', ',1,2,', 'fa-list-ul', '', 'Y', 3),
(121, NULL, NULL, ',1,', '0', 'Setup', 'Y', ',1,', 'fa-folder', '', 'N', 7),
(79, '', NULL, ',1,', '0', 'Setup Admin', 'Y', ',1,2,', 'fa-folder', '', 'Y', 0),
(99, 'sysinformasi', 'berita/teks_berjalan', '', '98', 'Teks Berjalan', '', ',1,2,', 'fa-text-width', '', 'Y', 1),
(98, '', '', ',1,2,', '0', 'Informasi', 'Y', ',1,4,', 'fa-folder', '', 'Y', 3),
(124, 'akdresetpass', 'sdmPass', '', '123', ' Reset Passowrd SDM', NULL, ',0,', 'fa-folder-o', '', 'Y', NULL),
(125, NULL, NULL, ',1,', '0', 'MHS', 'Y', ',1,', 'fa-folder', '', 'N', 9),
(126, 'akdresetpass', 'mhsPass', '', '125', 'Reset Password MHS', NULL, ',0,', 'fa-folder-o', '', 'Y', NULL),
(262, 'user', 'index', ',23,', '79', 'User', NULL, ',1,', 'fa-user', '', 'Y', 1),
(346, 'user', 'index/2', ',25,', '339', 'Data User', NULL, ',1,', 'fa-circle-o', '', 'Y', 1),
(345, 'user', 'index/1', ',24,', '332', 'Data User', NULL, ',1,', 'fa-circle-o', '', 'Y', 1),
(334, 'user', 'index', ',23,', '326', 'Data User', NULL, ',1,', 'fa-user', '', 'Y', 1),
(378, NULL, NULL, ',25,', '0', 'Sejarah Transaksi', 'Y', ',1,', 'fa-history', '', 'Y', 6),
(420, 'list_penjualan', 'index/1', ',27,28,29,30,', '0', 'List Penjualan', NULL, ',1,', 'fa-list-ul', '', 'Y', 8),
(419, 'user', 'index/1', ',27,', '0', 'User', NULL, ',1,', 'fa-users', '', 'Y', 1),
(416, 'm_menu_cafe', 'index/1', ',27,', '0', 'Master Menu', NULL, ',1,', 'fa-folder', '', 'Y', 2),
(417, 'm_menu_cafe_stok', 'index/1', ',27,', '0', 'Barang Masuk', NULL, ',1,2,', 'fa-cart-plus', '', 'Y', 4),
(418, 'keu_laporan_unit', 'index/1', ',27,28,29,30,', '0', 'Laporan Penjualan', NULL, ',1,', 'fa-file-text-o', '', 'Y', 9),
(421, 'pengajuan_pembatalan', 'index/1', ',27,', '0', 'Pengajuan Pembatalan', NULL, ',1,', ' fa-close', '', 'Y', 10),
(422, 'm_menu_paket', 'index/1', ',27,', '0', 'Master Paket', NULL, ',1,2,', 'fa-folder', '', 'Y', 3),
(423, 'distribusi_barang', 'createDistribusi', ',27,', '0', 'Distribus Barang', NULL, ',1,2,', 'fa-cart-arrow-down', '', 'Y', 5),
(424, 'distribusi_barang', 'history', ',27,', '0', 'History Distribusi', NULL, ',1,2,', 'fa-history', '', 'Y', 6),
(425, 'stok_barang', 'index', ',28,29,30,', '0', 'Stok Barang', NULL, ',1,2,', 'fa-list', '', 'Y', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `keu_btl_transaksi_cafe`
--

CREATE TABLE `keu_btl_transaksi_cafe` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` int(20) NOT NULL,
  `meja` varchar(50) DEFAULT NULL,
  `id_user` int(6) NOT NULL,
  `id_unit` int(5) NOT NULL,
  `user_cancel` int(5) DEFAULT NULL,
  `cancel_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keu_btl_transaksi_dtl_cafe`
--

CREATE TABLE `keu_btl_transaksi_dtl_cafe` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(5) NOT NULL,
  `id_item` int(5) NOT NULL,
  `harga_satuan` int(20) NOT NULL,
  `qty` int(5) NOT NULL,
  `harga_total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keu_transaksi_cafe`
--

CREATE TABLE `keu_transaksi_cafe` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` int(20) NOT NULL,
  `nominal_bayar` int(20) NOT NULL,
  `meja` varchar(50) DEFAULT NULL,
  `kelas` varchar(50) DEFAULT NULL,
  `id_user` int(6) NOT NULL,
  `id_usergroup` int(5) DEFAULT NULL,
  `id_unit` int(5) NOT NULL,
  `user_update` int(10) DEFAULT NULL,
  `is_proses` int(1) NOT NULL DEFAULT '0',
  `updated_at` varchar(50) DEFAULT NULL,
  `stts_transaksi` int(5) NOT NULL DEFAULT '1',
  `user_pengajuan_pembatalan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Trigger `keu_transaksi_cafe`
--
DELIMITER $$
CREATE TRIGGER `tr_after_update_pembatalan` AFTER UPDATE ON `keu_transaksi_cafe` FOR EACH ROW IF NEW.stts_transaksi = 3 THEN
BEGIN
update keu_transaksi_dtl_cafe set 
stts_transaksi = NEW.stts_transaksi
where id_transaksi = NEW.id_transaksi;
END;
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keu_transaksi_dtl_cafe`
--

CREATE TABLE `keu_transaksi_dtl_cafe` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(5) NOT NULL,
  `id_item` int(5) NOT NULL,
  `harga_beli` int(20) NOT NULL,
  `harga_satuan` int(20) NOT NULL,
  `qty` int(5) NOT NULL,
  `harga_total` int(20) NOT NULL,
  `is_paket` int(11) NOT NULL DEFAULT '0',
  `stts_transaksi` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_akses_apps`
--

CREATE TABLE `m_akses_apps` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m_akses_apps`
--

INSERT INTO `m_akses_apps` (`id`, `nama`, `aktif`) VALUES
(1, 'Web', 'Y'),
(7, 'Kasir Kantin', 'Y'),
(12, 'Web & Kasir Kantin', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_history_distribusi`
--

CREATE TABLE `m_history_distribusi` (
  `id` int(11) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_menu` int(10) NOT NULL,
  `stok` int(10) NOT NULL,
  `admin` varchar(35) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_history_distribusi`
--

INSERT INTO `m_history_distribusi` (`id`, `id_kelas`, `id_menu`, `stok`, `admin`, `created_at`) VALUES
(1, 1, 108, 40, 'admin', '2019-12-06 02:24:50'),
(2, 1, 103, 7, 'admin', '2019-12-06 02:24:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kelas`
--

CREATE TABLE `m_kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `aktif` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `m_kelas`
--

INSERT INTO `m_kelas` (`id`, `nama`, `aktif`) VALUES
(1, 'Ekonomi', 1),
(2, 'VIP', 1),
(3, 'Eksekutif', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_menu_cafe`
--

CREATE TABLE `m_menu_cafe` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL DEFAULT '0',
  `premi` int(20) DEFAULT '0',
  `id_unit` int(5) NOT NULL,
  `pengurangan_qty` int(11) NOT NULL DEFAULT '1',
  `aktif` varchar(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `m_menu_cafe`
--

INSERT INTO `m_menu_cafe` (`id`, `nama`, `jenis`, `harga`, `stok`, `premi`, `id_unit`, `pengurangan_qty`, `aktif`) VALUES
(1, 'Adem Sari Chinku ', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(2, 'Antimo', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(3, 'Aqua', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(4, 'Buavita', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(5, 'C1000', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(6, 'Ciki ', 'Minuman', 3000, 0, 1000, 1, 1, 'Y'),
(7, 'Contry Choice', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(8, 'Crispy Nissin', 'Minuman', 15000, 0, 2000, 1, 1, 'Y'),
(9, 'Floridinia', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(10, 'Fruitea', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(11, 'Hydro Coco', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(12, 'Indomilk', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(13, 'K Energen', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(14, 'K Susu Jahe', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(15, 'K Teh Tarik', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(16, 'Kacang Garuda', 'Minuman', 10000, 0, 2000, 1, 1, 'Y'),
(17, 'Kopi ABC Susu', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(18, 'Kopi Good Day', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(19, 'Kopi Kapal Api', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(20, 'Kopi Luak', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(21, 'Kopi Mix', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(22, 'Kratindaeng', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(23, 'Larutan Kaleng', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(24, 'Mie Goreng', 'Minuman', 15000, 0, 2000, 1, 1, 'Y'),
(25, 'Milo Kotak', 'Minuman', 6000, 0, 1000, 1, 1, 'Y'),
(26, 'Mizone', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(27, 'Mony Cincau ', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(28, 'Oreo Bolu', 'Minuman', 3000, 0, 1000, 1, 1, 'Y'),
(29, 'Oreo Mini', 'Minuman', 3000, 0, 1000, 1, 1, 'Y'),
(30, 'Pembalut', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(31, 'Pocari Botol', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(32, 'Popmie', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(33, 'Pulpy Orange', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(34, 'Roma Kelapa', 'Minuman', 15000, 0, 2000, 1, 1, 'Y'),
(35, 'Roma Malkis', 'Minuman', 10000, 0, 2000, 1, 1, 'Y'),
(36, 'Roma Wafer Coklat', 'Minuman', 3000, 0, 1000, 1, 1, 'Y'),
(37, 'S Tee', 'Minuman', 10000, 0, 2000, 1, 1, 'Y'),
(38, 'Sari Kacang Hijau', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(39, 'Slai Olai', 'Minuman', 3000, 0, 1000, 1, 1, 'Y'),
(40, 'Sosro Botol', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(41, 'Sprite/CocaCola/Fanta', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(42, 'Susu Beruang', 'Minuman', 15000, 0, 2000, 1, 1, 'Y'),
(43, 'Susu Ultra', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(44, 'Tebs', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(45, 'Teh Celup', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(46, 'Teh Pucuk', 'Minuman', 10000, 0, 2000, 1, 1, 'Y'),
(47, 'Tisu Basah', 'Minuman', 8000, 0, 2000, 1, 1, 'Y'),
(48, 'Tisu Kering', 'Minuman', 6000, 0, 1000, 1, 1, 'Y'),
(49, 'Tolak Angin', 'Minuman', 6000, 0, 1000, 1, 1, 'Y'),
(50, 'Kripik Tempe', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(51, 'Kacang Bawang', 'Minuman', 13000, 0, 2000, 1, 1, 'Y'),
(52, 'Popcorn', 'Minuman', 13000, 0, 2000, 1, 1, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_menu_cafe_stok`
--

CREATE TABLE `m_menu_cafe_stok` (
  `id_stok` int(11) NOT NULL,
  `id_unit` int(10) NOT NULL,
  `id_menu` int(10) NOT NULL,
  `qty` int(25) NOT NULL,
  `harga_beli` int(25) NOT NULL,
  `total` int(25) NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Trigger `m_menu_cafe_stok`
--
DELIMITER $$
CREATE TRIGGER `tr_after_insert_stok_cafe` AFTER INSERT ON `m_menu_cafe_stok` FOR EACH ROW BEGIN
update m_menu_cafe set 
stok = stok + NEW.qty
where id = NEW.id_menu;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tr_after_update_stok_cafe` AFTER UPDATE ON `m_menu_cafe_stok` FOR EACH ROW BEGIN
update m_menu_cafe set 
stok = stok - OLD.qty + NEW.qty
where id = old.id_menu;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_menu_paket`
--

CREATE TABLE `m_menu_paket` (
  `id_paket` int(11) NOT NULL,
  `nm_paket` varchar(50) NOT NULL,
  `harga_modal` int(15) DEFAULT NULL,
  `harga` int(15) NOT NULL,
  `id_unit` int(10) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_menu_paket_item`
--

CREATE TABLE `m_menu_paket_item` (
  `id_paket_item` int(11) NOT NULL,
  `id_paket` int(10) NOT NULL,
  `id_menu` int(15) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_status_transaksi`
--

CREATE TABLE `m_status_transaksi` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_status_transaksi`
--

INSERT INTO `m_status_transaksi` (`id`, `keterangan`) VALUES
(1, 'Berhasil'),
(2, 'Pengajuan pembatalan'),
(3, 'Dibatalkan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_stok_kantin`
--

CREATE TABLE `m_stok_kantin` (
  `id` int(11) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_menu` int(10) NOT NULL,
  `stok` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `m_stok_kantin`
--
DELIMITER $$
CREATE TRIGGER `tr_after_ins_kurangi_stok_gudang` BEFORE INSERT ON `m_stok_kantin` FOR EACH ROW begin
update m_menu_cafe
set m_menu_cafe.stok = m_menu_cafe.stok - NEW.stok
where m_menu_cafe.id = NEW.id_menu;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tr_after_upt_kurangi_stok_gudang` AFTER UPDATE ON `m_stok_kantin` FOR EACH ROW begin
update m_menu_cafe
set m_menu_cafe.stok = m_menu_cafe.stok - (NEW.stok - OLD.stok)
where m_menu_cafe.id = NEW.id_menu;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_unit`
--

CREATE TABLE `m_unit` (
  `id_unit` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT 'Y',
  `telp` varchar(40) DEFAULT NULL,
  `alamat` text,
  `hotline` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `m_unit`
--

INSERT INTO `m_unit` (`id_unit`, `nama`, `aktif`, `telp`, `alamat`, `hotline`) VALUES
(1, 'Kapal 1', 'Y', '(0298) 711078', 'Jl Veteran No 3 Jetis, Bandungan, Semarang Jawa Tengah 50231', '08989829832');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_akd_ps`
--

CREATE TABLE `ref_akd_ps` (
  `id_ps` int(8) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `nama_en` varchar(64) NOT NULL,
  `id_jenjang` int(8) NOT NULL,
  `id_jurusan` int(8) NOT NULL,
  `id_fakultas` int(8) NOT NULL,
  `id_aktif` varchar(1) NOT NULL,
  `ext` varchar(5) NOT NULL,
  `status` varchar(16) NOT NULL,
  `grade_status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_akd_ps`
--

INSERT INTO `ref_akd_ps` (`id_ps`, `nama`, `nama_en`, `id_jenjang`, `id_jurusan`, `id_fakultas`, `id_aktif`, `ext`, `status`, `grade_status`) VALUES
(1, 'S1 Ilmu Ekonomi', '', 1, 1, 1, 'Y', 'EK', '', ''),
(2, 'S1 Manajemen', '', 1, 1, 1, 'Y', 'MN', '', ''),
(3, 'S1 Akuntansi', '', 1, 1, 1, 'Y', '', '', ''),
(10, 'PENGAJARAN STIEWW', '', 1, 0, 1, '1', 'STIE', '', ''),
(11, 'MANAJEMEN', 'MANAGEMENT', 1, 0, 1, '1', 'RM', '', ''),
(12, 'AKUNTANSI', 'ACCOUNTING', 1, 0, 1, '1', 'RA', '', ''),
(21, 'MANAJEMEN', 'MANAGEMENT', 1, 0, 1, '1', 'EM', '', ''),
(22, 'AKUNTANSI', 'ACCOUNTING', 1, 0, 1, '1', 'EA', '', ''),
(31, 'MANAJEMEN', 'MANAGEMENT', 2, 0, 1, '1', 'MM', '', ''),
(32, 'AKUNTANSI', 'ACCOUNTING', 1, 0, 1, '1', 'MA', '', ''),
(41, 'MANAJEMEN', 'MANAGEMENT', 1, 0, 1, '1', 'WM', '', ''),
(42, 'AKUNTANSI', 'ACCOUNTING', 1, 0, 1, '1', 'WA', '', ''),
(51, 'MANAJEMEN', 'MANAGEMENT', 1, 0, 1, '1', 'IM', '', ''),
(52, 'AKUNTANSI', 'ACCOUNTING', 1, 0, 1, '1', 'IA', '', ''),
(61, 'MANAGEMENT MAGISTER', 'MANAGEMENT MAGISTER', 2, 0, 1, '1', 'M.M', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_akd_sdm`
--

CREATE TABLE `ref_akd_sdm` (
  `id_sdm` int(11) NOT NULL,
  `nip` varchar(19) DEFAULT NULL,
  `nip_riil` varchar(19) DEFAULT '',
  `id_ps` int(11) DEFAULT NULL,
  `jenjang` varchar(20) DEFAULT NULL,
  `nama` text,
  `gelar` text,
  `alias0` varchar(10) DEFAULT '',
  `e_mail` text,
  `kelamin` enum('l','p') DEFAULT 'l',
  `alamat` text,
  `rt` varchar(3) DEFAULT NULL,
  `rw` varchar(3) DEFAULT NULL,
  `nama_dusun` varchar(50) DEFAULT NULL,
  `desa_kelurahan` varchar(50) DEFAULT NULL,
  `kode_wilayah` varchar(50) DEFAULT NULL,
  `kodepos` varchar(5) DEFAULT '',
  `telpon` text,
  `fax` text,
  `tmp_lahir` text,
  `tgl_lahir` date DEFAULT NULL,
  `gol_darah` enum('A','B','O','AB') DEFAULT NULL,
  `kode_agama` char(1) DEFAULT '',
  `kelp` varchar(30) DEFAULT '',
  `nickname` varchar(16) DEFAULT '',
  `gelar_dpn` text,
  `gelar_blk` text,
  `foto` varchar(500) DEFAULT NULL,
  `id_status` varchar(5) DEFAULT 'A',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nik` char(16) DEFAULT NULL,
  `niy_nigk` varchar(30) DEFAULT NULL,
  `nuptk` char(16) DEFAULT NULL,
  `status_kepegawaian_id` smallint(6) DEFAULT NULL,
  `jenis_ptk_id` decimal(2,0) DEFAULT NULL,
  `pengawas_bidang_studi_id` int(11) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `entry_sekolah_id` int(11) DEFAULT NULL,
  `status_keaktifan_id` decimal(2,0) DEFAULT NULL,
  `sk_cpns` varchar(50) DEFAULT NULL,
  `tgl_cpns` date DEFAULT NULL,
  `sk_pengangkatan` varchar(50) DEFAULT NULL,
  `tmt_pengangkatan` date DEFAULT NULL,
  `lembaga_pengangkat_id` decimal(2,0) DEFAULT NULL,
  `pangkat_golongan_id` decimal(2,0) DEFAULT NULL,
  `keahlian_laboratorium_id` smallint(6) DEFAULT NULL,
  `sumber_gaji_id` decimal(2,0) DEFAULT NULL,
  `nama_ibu_kandung` varchar(60) DEFAULT NULL,
  `status_perkawinan` decimal(1,0) DEFAULT NULL,
  `nama_suami_istri` varchar(60) DEFAULT NULL,
  `nip_suami_istri` char(18) DEFAULT NULL,
  `pekerjaan_suami_istri` int(11) DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `sudah_lisensi_kepala_sekolah` decimal(1,0) DEFAULT NULL,
  `jumlah_sekolah_binaan` smallint(6) DEFAULT NULL,
  `pernah_diklat_kepengawasan` decimal(1,0) DEFAULT NULL,
  `status_data` int(11) DEFAULT NULL,
  `mampu_handle_kk` int(11) DEFAULT NULL,
  `keahlian_braille` decimal(1,0) DEFAULT '0',
  `keahlian_bhs_isyarat` decimal(1,0) DEFAULT '0',
  `npwp` char(15) DEFAULT NULL,
  `kewarganegaraan` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_akd_sdm`
--

INSERT INTO `ref_akd_sdm` (`id_sdm`, `nip`, `nip_riil`, `id_ps`, `jenjang`, `nama`, `gelar`, `alias0`, `e_mail`, `kelamin`, `alamat`, `rt`, `rw`, `nama_dusun`, `desa_kelurahan`, `kode_wilayah`, `kodepos`, `telpon`, `fax`, `tmp_lahir`, `tgl_lahir`, `gol_darah`, `kode_agama`, `kelp`, `nickname`, `gelar_dpn`, `gelar_blk`, `foto`, `id_status`, `created`, `nik`, `niy_nigk`, `nuptk`, `status_kepegawaian_id`, `jenis_ptk_id`, `pengawas_bidang_studi_id`, `no_hp`, `entry_sekolah_id`, `status_keaktifan_id`, `sk_cpns`, `tgl_cpns`, `sk_pengangkatan`, `tmt_pengangkatan`, `lembaga_pengangkat_id`, `pangkat_golongan_id`, `keahlian_laboratorium_id`, `sumber_gaji_id`, `nama_ibu_kandung`, `status_perkawinan`, `nama_suami_istri`, `nip_suami_istri`, `pekerjaan_suami_istri`, `tmt_pns`, `sudah_lisensi_kepala_sekolah`, `jumlah_sekolah_binaan`, `pernah_diklat_kepengawasan`, `status_data`, `mampu_handle_kk`, `keahlian_braille`, `keahlian_bhs_isyarat`, `npwp`, `kewarganegaraan`) VALUES
(4, '030206003', '1676', 0, NULL, 'Admin', '', '', '', 'l', '', '0', '0', '', '', NULL, '', '', NULL, '', NULL, 'A', '', 'DOSEN', '', '', '', 'man-2.png', 'A', '2019-01-09 19:59:47', '', NULL, NULL, 0, '0', NULL, NULL, 0, '1', NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', '', '0', NULL, NULL, 0, NULL, '0', NULL, '0', NULL, 0, '0', '0', NULL, ''),
(87, '123', '1761', NULL, NULL, 'udin', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-01-09 20:06:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(88, '123', '1762', NULL, NULL, 'fauzy hanif', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-01-23 19:36:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(89, '123', '1763', NULL, NULL, 'acong', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-01 08:29:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(90, '1234', '1764', NULL, NULL, 'test', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-08 14:21:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(91, '1234', '1765', NULL, NULL, 'fu yung hay', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-08 14:29:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(92, '1234', '1766', NULL, NULL, 'FAUZY HANIF NOER', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-08 14:31:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(93, '1234', '1767', NULL, NULL, 'zzz', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-08 14:39:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(94, '123', '1768', NULL, NULL, 'asdsdas', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, '', 'A', '2019-02-08 14:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(95, '1234', '1769', NULL, NULL, 'maman', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-02-09 07:02:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(96, '123', '1770', NULL, NULL, 'eja', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'A', '2019-12-07 01:44:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(97, '123', '1771', NULL, NULL, 'ejun', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'A', '2019-12-07 01:45:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(98, '123', '1772', NULL, NULL, 'wey', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-12-07 01:46:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(99, '123', '1773', NULL, NULL, 'aa', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-12-07 02:00:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(100, '123', '1774', NULL, NULL, 'xxx', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-12-07 02:24:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL),
(101, '123', '1775', NULL, NULL, 'p_ekonomi', NULL, '', NULL, 'l', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 'man-2.png', 'A', '2019-12-07 02:33:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_area`
--

CREATE TABLE `ref_area` (
  `id` int(11) NOT NULL,
  `label_menu` varchar(200) NOT NULL,
  `ps_id` varchar(128) NOT NULL,
  `usergroup_id` varchar(200) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_area`
--

INSERT INTO `ref_area` (`id`, `label_menu`, `ps_id`, `usergroup_id`, `aktif`, `created`) VALUES
(1, 'ADMIN DEVELOPER', 'ps-0', ',0,', 'Y', '2019-12-07 01:17:05'),
(27, 'ADMIN GUDANG', 'ps-0', ',1,', 'Y', '2019-12-07 01:20:58'),
(28, 'EKONOMI', 'ps-1', ',1,2,3,', 'Y', '2019-12-07 01:31:10'),
(29, 'VIP', 'ps-2', ',1,2,3,', 'Y', '2019-12-07 01:31:10'),
(30, 'APER', 'ps-3', ',1,2,3,', 'Y', '2019-12-07 01:31:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_sys_usergroup`
--

CREATE TABLE `ref_sys_usergroup` (
  `id` int(2) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `deskripsi` text,
  `aktif` enum('Y','N') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_sys_usergroup`
--

INSERT INTO `ref_sys_usergroup` (`id`, `nama`, `deskripsi`, `aktif`, `created`) VALUES
(1, 'Admin Gudang', '', 'Y', '2019-12-07 01:22:58'),
(2, 'Kasir', '', 'Y', '2019-02-08 13:58:34'),
(3, 'Pramugari', '', 'Y', '2019-02-08 13:58:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_user`
--

CREATE TABLE `ref_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `nip` varchar(500) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `area` varchar(500) DEFAULT NULL,
  `usergroup` varchar(16) NOT NULL,
  `passwd` char(128) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `id_unit` int(5) NOT NULL,
  `id_kelas` int(10) DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_user`
--

INSERT INTO `ref_user` (`id`, `uid`, `nip`, `id_jenis`, `area`, `usergroup`, `passwd`, `nama`, `email`, `id_unit`, `id_kelas`, `aktif`) VALUES
(1676, 'admin', '123', 1, ',1,27,28,29,30,', ',1,', '81c5c45172f5df2ac50e71e561bdfc6d', 'Admin', NULL, 1, NULL, 'Y'),
(1775, 'p_ekonomi', '123', 1, ',28,', ',3,', '094766f4eb615f1065a2fcf4f4a62aa8', 'p_ekonomi', NULL, 1, 1, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ref_user_jenis`
--

CREATE TABLE `ref_user_jenis` (
  `id_jenis` int(8) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `id_aktif` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `ref_user_jenis`
--

INSERT INTO `ref_user_jenis` (`id_jenis`, `nama`, `id_aktif`) VALUES
(1, 'SDM', 'Y'),
(2, 'MHS', 'Y');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_user`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_user` (
`id_user` int(11)
,`id_jenis` int(11)
,`area` varchar(500)
,`usergroup` varchar(16)
,`login` varchar(128)
,`id_unit` int(5)
,`nama` varchar(50)
,`nip` varchar(19)
,`foto` varchar(500)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_user`
--
DROP TABLE IF EXISTS `view_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_user`  AS  select `c`.`id` AS `id_user`,`c`.`id_jenis` AS `id_jenis`,`c`.`area` AS `area`,`c`.`usergroup` AS `usergroup`,`c`.`uid` AS `login`,`c`.`id_unit` AS `id_unit`,`c`.`nama` AS `nama`,`b`.`nip` AS `nip`,`b`.`foto` AS `foto` from (`ref_user` `c` left join `ref_akd_sdm` `b` on(((`c`.`uid` = `b`.`nip`) or (`c`.`id` = `b`.`nip_riil`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl`
--
ALTER TABLE `acl`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `controller` (`controller`) USING BTREE,
  ADD KEY `controller_2` (`controller`) USING BTREE;

--
-- Indexes for table `keu_btl_transaksi_cafe`
--
ALTER TABLE `keu_btl_transaksi_cafe`
  ADD PRIMARY KEY (`id_transaksi`) USING BTREE;

--
-- Indexes for table `keu_btl_transaksi_dtl_cafe`
--
ALTER TABLE `keu_btl_transaksi_dtl_cafe`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `keu_transaksi_cafe`
--
ALTER TABLE `keu_transaksi_cafe`
  ADD PRIMARY KEY (`id_transaksi`) USING BTREE;

--
-- Indexes for table `keu_transaksi_dtl_cafe`
--
ALTER TABLE `keu_transaksi_dtl_cafe`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m_akses_apps`
--
ALTER TABLE `m_akses_apps`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m_history_distribusi`
--
ALTER TABLE `m_history_distribusi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kelas`
--
ALTER TABLE `m_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menu_cafe`
--
ALTER TABLE `m_menu_cafe`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m_menu_cafe_stok`
--
ALTER TABLE `m_menu_cafe_stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `m_menu_paket`
--
ALTER TABLE `m_menu_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `m_menu_paket_item`
--
ALTER TABLE `m_menu_paket_item`
  ADD PRIMARY KEY (`id_paket_item`);

--
-- Indexes for table `m_status_transaksi`
--
ALTER TABLE `m_status_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_stok_kantin`
--
ALTER TABLE `m_stok_kantin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_unit`
--
ALTER TABLE `m_unit`
  ADD PRIMARY KEY (`id_unit`) USING BTREE;

--
-- Indexes for table `ref_akd_ps`
--
ALTER TABLE `ref_akd_ps`
  ADD PRIMARY KEY (`id_ps`) USING BTREE,
  ADD KEY `akd_ps_id01` (`id_jenjang`) USING BTREE,
  ADD KEY `akd_ps_id02` (`id_fakultas`) USING BTREE,
  ADD KEY `akd_ps_id03` (`id_aktif`) USING BTREE,
  ADD KEY `akd_ps_id05` (`id_jurusan`) USING BTREE;

--
-- Indexes for table `ref_akd_sdm`
--
ALTER TABLE `ref_akd_sdm`
  ADD PRIMARY KEY (`id_sdm`) USING BTREE;

--
-- Indexes for table `ref_sys_usergroup`
--
ALTER TABLE `ref_sys_usergroup`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ref_user`
--
ALTER TABLE `ref_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_unit` (`id_unit`) USING BTREE;

--
-- Indexes for table `ref_user_jenis`
--
ALTER TABLE `ref_user_jenis`
  ADD PRIMARY KEY (`id_jenis`) USING BTREE,
  ADD KEY `web_user_jenis_id01` (`id_aktif`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl`
--
ALTER TABLE `acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=426;
--
-- AUTO_INCREMENT for table `keu_transaksi_cafe`
--
ALTER TABLE `keu_transaksi_cafe`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keu_transaksi_dtl_cafe`
--
ALTER TABLE `keu_transaksi_dtl_cafe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_akses_apps`
--
ALTER TABLE `m_akses_apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `m_history_distribusi`
--
ALTER TABLE `m_history_distribusi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_kelas`
--
ALTER TABLE `m_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_menu_cafe`
--
ALTER TABLE `m_menu_cafe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `m_menu_cafe_stok`
--
ALTER TABLE `m_menu_cafe_stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_menu_paket`
--
ALTER TABLE `m_menu_paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_menu_paket_item`
--
ALTER TABLE `m_menu_paket_item`
  MODIFY `id_paket_item` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_status_transaksi`
--
ALTER TABLE `m_status_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_stok_kantin`
--
ALTER TABLE `m_stok_kantin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_unit`
--
ALTER TABLE `m_unit`
  MODIFY `id_unit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ref_akd_sdm`
--
ALTER TABLE `ref_akd_sdm`
  MODIFY `id_sdm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `ref_sys_usergroup`
--
ALTER TABLE `ref_sys_usergroup`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30001;
--
-- AUTO_INCREMENT for table `ref_user`
--
ALTER TABLE `ref_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1776;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
