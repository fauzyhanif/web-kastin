<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" name="" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
    <h1>
        Transaksi Lain-lain 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-text-o"></i> Admin</a></li>
        <li class="active">Transaksi Lain-lain</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Tanggal : <?= $this->helper->konversi_tgl($date1) ?> <?php if ($date2 != '') { ?> - <?= $this->helper->konversi_tgl($date2) ?><?php } ?></h4> 
                    <a href="#" onclick="return go_page('keu_transaksi_dll/formAdd/<?= $id_unit ?>')" class="btn btn-flat btn-primary box-tools pull-right" style="margin-right: 78px;">
                      <i class="fa fa-plus-square"></i>
                      Input Transaksi
                    </a>
                    <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
                      <i class="fa fa-filter"></i> Filter
                    </button>
                    <?php if (!empty($username)) { ?>
                    <h4>Nama : <?= $username ?></h4>
                    <?php } ?>

                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-sm" role="document"">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Filter Laporan Transaksi</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                              </div> 
                            </div>

                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                              </div> 
                            </div>

                          </div>
                          <div class="modal-footer">
                            <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="box-body" style="overflow: auto;" id="listView">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                        	<tr>
                        		<th class="text-center" width="5%">No</th>
                        		<th class="text-center" width="5%">Edit</th>
                            <th class="text-center" width="15%">Tgl Transaksi</th>
                            <th class="text-center">Item</th>
                            <th class="text-center" width="15%">Qty / Satuan</th>
                            <th class="text-center" width="10%">Jumlah</th>
                        	</tr>
                        </thead>
                        <tbody>
                          <?php $no = 1; ?>
                          <?php $jumlah = 0; ?>
                          <?php foreach ($data as $x) { ?>
                            <tr>
                              <td class="text-center"><?= $no ?>.</td>
                              <td>
                                <a  class="btn btn-primary btn-xs btn-flat" onclick="return go_page('keu_transaksi_dll/formUpdate/<?= $x->id_transaksi ?>')">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                              </td>
                              <td><?= $this->helper->konversi_tgl($x->tgl_transaksi) ?></td>
                              <td><?= $x->item ?></td>
                              <td><?= $x->qty ?> / <?= $x->satuan ?></td>
                              <td class="text-right"><?= $this->helper->formatRupiah($x->harga_total) ?></td>
                            </tr>
                          <?php $jumlah = $jumlah + $x->harga_total; ?>
                          <?php $no = $no + 1; ?>
                          <?php } ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="5" class="text-center"><b>Total</b></td>
                            <td style="display: none"></td>
                            <td style="display: none"></td>
                            <td style="display: none"></td>
                            <td style="display: none"></td>
                            <td class="text-right"><b><?= $this->helper->formatRupiah($jumlah) ?></b></td>
                          </tr>
                        </tfoot>
                        <div style="position: relative; overflow: auto; max-height: 100px">
                            
                        </div>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>(function() {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('.datepicker').datepicker({
        language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
    });

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('keu_transaksi_dll/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'danger') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter()
{
    var id_unit = $('#id_unit').val();
    var date1   = $('#date1').val();
    var date2   = $('#date2').val();

    var url     = "<?= $this->url->get('keu_transaksi_dll/index/') ?>" + id_unit + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
    go_page(url);
}
</script>
