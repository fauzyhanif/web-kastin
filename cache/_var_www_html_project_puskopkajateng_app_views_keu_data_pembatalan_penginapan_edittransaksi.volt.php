<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<!-- Header content -->
<section class="content-header">
  <h1>
    Detail Transaksi penginapan
  </h1>
  <ol class="breadcrumb">
    <li>
      <button class="btn bg-navy btn-flat" onclick="return go_page('keu_data_pembatalan/index/<?= $id_unit ?>/8')" style="margin-top: -11px;">
        <i class="fa  fa-chevron-circle-left"></i>
        Back
      </button>
    </li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header  with-border">
          <div class="col-md-12">
            <div class="col-md-3" style="font-size: 22px">Nomor Struk</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->a->id_transaksi ?>.</div>
            <div class="col-md-3" style="font-size: 22px">Dibatalkan Oleh:</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->user_cancel ?>.</div>

            <div class="col-md-3" style="font-size: 22px">Kasir</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->nm_user ?>.</div>
            <div class="col-md-3" style="font-size: 22px">Tgl Pembatalan</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->a->cancel_at ?>.</div>
            
            <div class="col-md-3" style="font-size: 22px">Tgl Transaksi</div>
            <div class="col-md-9" style="font-size: 22px">: <?= $data[0]->a->tgl_transaksi ?>.</div>
          </div>
        </div>

        <div class="box-body" style="overflow: auto;" id="listView">
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" width="5%">No</th>
                  <th class="text-center" width="22%">Item</th>
                  <th class="text-center" width="8%">Extra Bed</th>
                  <th class="text-center" width="5%">Qty</th>
                  <th class="text-center" width="10%">Harga Satuan</th>
                  <th class="text-center" width="10%">Jumlah</th>
                </tr>
              </thead>
              <tbody>
                <?php $total = 0; ?>
                <?php $no = 1; ?>
                <?php foreach ($dataRinci as $x) { ?>

                <tr class="content"> 
                  <td class="text-center"><?= $no ?>.</td>
                  <td> <?= $x->item ?> </td>
                  <td> <?= $x->a->extrabed ?> </td>
                  <td class="text-center"> <?= $x->a->qty ?> </td>
                  <td class="text-right"> <?= $this->helper->formatRupiah($x->a->harga_satuan) ?> </td>
                  <td class="text-right"> <?= $this->helper->formatRupiah($x->a->harga_total) ?> </td>
                </tr>

                <?php $total = $total + $x->a->harga_total; ?>
                <?php $no = $no + 1; ?>
                <?php } ?>

              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5" class="text-center"><h4><b>Total</b></h4></td>
                  <td class="text-right"><h4><b id="total"><?= $this->helper->formatRupiah($total) ?></b></h4></td>
                </tr>
              </tfoot>
            </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$(".select2").select2();
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_data_pembatalan/index/') ?>" + id_unit + "/6/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
</script>

<!-- include popup -->
