
$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable( {
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
        dom: 'Bfrtip',
        buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        searching: false
    });

});

$(function() {
    $('#harga_satuan').keyup(function() {
        harga = $(this).val().replace(/\./g, '');
        qty   = $('#qty').val();
        hasil = harga * qty;
        $('#harga_total').val(hasil);
    });
    
    $('#qty').keyup(function() {
        qty     = $(this).val();
        harga   = $('#harga_satuan').val().replace(/\./g, '');
        hasil   = qty * harga;
        $('#harga_total').val(hasil);
    });
    
});
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit  	= $('input[name="id_unit"]').val();
        var urlReload 	= '<?= $this->url->get('keu_sejarah_transaksi/index/') ?>'+idUnit+'/10';

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_sejarah_transaksi/index/') ?>" + id_unit + "/5/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,hargaSatuan,qty,hargaTotal) {
	$('#idTransaksi').html(": "+idTransaksi);	
	$('#nmUser').html(": "+nmUser);
	$('#tglTransaksi').html(": "+tglTransaksi);
	$('#item').html(": "+item);
	$('#hargaSatuan').html(": Rp."+hargaSatuan);
	$('#qty').html(": "+qty);
	$('#hargaTotal').html(": Rp."+hargaTotal);
}


function editTransaksix(idUnit,idTransaksi,nmUser,tglTransaksi,item,noPolisi,hargaSatuan,qty,hargaTotal) {
	$('form[id="parkir"]').attr('action', '<?= $this->url->get('keu_sejarah_transaksi/editTransaksi/') ?>' + idTransaksi);
	$('input[name="id_unit"]').val(idUnit);
	$('input[id="idTransaksi"]').val(idTransaksi);	
	$('input[id="nmUser"]').val(nmUser);
	$('input[id="tglTransaksi"]').val(tglTransaksi);
	$('input[name="id_item"]').val(item);
	$('input[name="no_polisi"]').val(noPolisi);
	$('input[name="harga_satuan"]').val(hargaSatuan);
	$('input[name="qty"]').val(qty);
	$('input[name="harga_total"]').val(hargaTotal);
}

