
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PUSKOPKA JATENG</title>
  <!-- favicon  -->
  <link rel="icon" href="img/favicon.png" sizes="32x32" />
  <link rel="icon" href="img/favicon.png" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="img/favicon.png" />
  <meta name="msapplication-TileImage" content="img/favicon.png" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css">
   <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="css/skins/_all-skins.min.css">
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"> -->
  <link rel="stylesheet" href="css/asilah.css">
  <link rel="stylesheet" href="css/AdminLTE.css">
  <style type="text/css">
    /*.modal-backdrop {
      z-index: -1 !important;
      }*/
  </style>
  <link rel="stylesheet" href="plugins/notify/pnotify.custom.min.css">
  <link rel="stylesheet" href="plugins/notify/animate.css">

  <?= $this->tag->javascriptInclude('plugins/jQuery/jquery-2.2.4.min.js') ?>
  <?= $this->tag->javascriptInclude('js/asilah.js') ?>

  </head>
  <!-- <body onload="StartTimers();" onmousemove="ResetTimers();" class=" skin-purple hold-transition skin-blue sidebar-mini"> -->
  <body class=" skin-red hold-transition fixed sidebar-mini">

    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
          <a href="../../index2.html" class="logo">
        <!-- <a href="<?= $baseUri ?>" class="logo"> -->
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>PUSKOPKA JATENG</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>PUSKOPKA JATENG</b> </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->


              <!-- Tasks: style can be found in dropdown.less -->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php if ($this->session->get('id_jenis') == 1): ?>
                  <img class="user-image" src="<?= PUBLIC_URL ?>img/sdm/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
                  <?php else: ?>
                  <img class="user-image" src="<?= PUBLIC_URL ?>img/mhs/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
                  <?php endif ?>
                  <span class="hidden-xs"><?= $this->session->get('nama'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if ($this->session->get('id_jenis') == 1): ?>
                    <img class="img-circle" src="<?= PUBLIC_URL ?>img/sdm/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
                    <?php else: ?>
                    <img class="img-circle" src="<?= PUBLIC_URL ?>img/mhs/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
                    <?php endif ?>
                    <p>
                      <?= $this->session->get('nama'); ?> - <?php if ($this->session->get('id_jenis') == 1): ?>
                      SDM
                      <?php else: ?>
                      Mahasiswa
                      <?php endif ?>

                      <small><?=$this->session->get('nip') ?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">

                    <div class="col-xs-12 text-center">
                      <!-- <a href="#" onclick="return load_page2('user/gantiLogin')">Ubah Akun Login</a> -->
                      Selamat Datang di Web Admin <br/> KOPERASI PJKA
                    </div>

                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <!-- <a href="#" onclick="return load_page2('user/profil')" class="btn 
                      btn-default btn-flat">Ubah Profil</a> -->
                      <a href="#" onclick="return load_page2('user/gantiLogin')" class="btn btn-default btn-flat"><i class="fa fa-refresh"></i> Ubah Akun</a>
                    </div>
                    <div class="pull-right">
                      <a href="account/logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="account/logout"><i class="fa fa-sign-out"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php if ($this->session->get('id_jenis') == 1): ?>
              <img class="img-circle" src="<?= PUBLIC_URL ?>img/sdm/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
              <?php else: ?>
              <img class="img-circle" src="<?= PUBLIC_URL ?>img/mhs/<?= $this->session->get('foto'); ?>" alt="User Avatar" onerror="imageError(this)">
              <?php endif ?>
            </div>
            <div class="pull-left info">
              <p class="user-overflow" title="<?= $this->session->get('nama'); ?>"><?= $this->session->get('nama'); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <div id="manu">
          
          </div>
        </section>
        <!-- /.sidebar -->
      </aside>


<div class="content-wrapper">
	<img class="load" src="<?= BASE_URL ?>public/img/loader.gif">
	<div class="content-wrapper2">
		<?= $this->getContent() ?>
	</div>
</div>
	<!-- floating refresh button  -->
	<a href="#" onclick="return reload_page2('dashboard/home')" class="float-button">
		<i class="fa fa-refresh my-float-button"></i>
	</a>
	<div class="float-label-container">
		<div class="float-label-text">Refresh Halaman</div>
		<i class="fa fa-play float-label-arrow"></i>
	</div>	
	<!-- end floating refresh button  -->

	<footer class="main-footer">
	    <div class="container">
	      <strong>Copyright &copy; 2017 <a href="#"> Koperasi PJKA</a>.</strong> All rights
	      reserved.
	    </div>
	    <!-- /.container -->
	</footer>
	</div>
	<!-- ./wrapper -->

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    
    <!-- InputMask -->
    <script src="js/jquery.mask.js"></script>
    <!-- <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script> -->
    <!-- <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script> -->
	
	<!-- Bootstrap 3.3.6 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery.maskMoney.min.js"></script>
	<!-- jquery InputMask -->
	<script src="plugins/input-mask/jquery.inputmask.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

	<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js "></script>

	<script src="plugins/select2/select2.full.min.js"></script>

	<!-- bootstrap datepicker -->
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="plugins/datepicker/locales/bootstrap-datepicker.id.js"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="js/app.js"></script>
	<!-- AdminLTE for demo purposes -->
    <script src="js/demo.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <!-- PNotify -->
    
    <?= $this->tag->javascriptInclude('plugins/notify/pnotify.custom.min.js') ?>
    <?= $this->tag->javascriptInclude('https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js') ?>

    
    <!-- chart js -->
</body>
</html>

<script>
$(document).ready(function () {
    var urel = "<?= $this->url->get('Index/sessionLoginJenis') ?>";
    $.ajax({
        method: "GET",
        dataType: "json",
        url: urel,
        success: function (res) {
            // cek login sebagai apa
            // console.log(res);
            // 1 = DOSEN
            // 2 = MHS
            if (res.id_jenis == "1") {
                var link = "<?= $this->url->get('Index/menuArea') ?>";
                $.ajax({
                    method: "GET",
                    dataType: "html",
                    url: link,
                    success: function (res) {
                        $('#manu').html(res);
                    }
                });
            } else if (res.id_jenis == "2") {
                var link = "<?= $this->url->get('Index/menuMhs') ?>";
                $.ajax({
                    method: "GET",
                    dataType: "html",
                    url: link,
                    success: function (res) {
                        $('#manu').html(res);
                    }
                });
            }
        }
    });
});

function menu(id, ps_id, nama_area) {
    var cek_link = "<?= $this->url->get('Index/cekSession') ?>";
    var login = "<?= $this->url->get('account/login') ?>";
    $.ajax({
        method: "GET",
        dataType: "json",
        url: cek_link,
        success: function (res) {
            // cek session
            if (res.session == "true") {
                var link = "<?= $this->url->get('Index/menuUser?id=') ?>" + id + "&ps_id=" + ps_id + "&nama_area=" + nama_area;
                $.ajax({
                    type: "GET",
                    dataType: "html",
                    url: link,
                    success: function (res) {
                        // console.log(ps_id);
                        $('#manu').html(res);
                        // localStorage.setItem('ps_id_menu', base_url);
                    }
                });
            } else {
                window.location.replace(login);
            }
        }
    });

    $("#activeMenu li a").click(function () {
        $(this).parent().addClass('active').siblings().removeClass('active');
        alert("Sudah dipanggil!");
    });
}

function back_menu() {
    // cek session
    var cek_link = "<?= $this->url->get('Index/cekSession') ?>";
    var login = "<?= $this->url->get('account/login') ?>";
    $.ajax({
        method: "GET",
        dataType: "json",
        url: cek_link,
        success: function (res) {
            // cek session
            if (res.session == "true") {
                var link = "<?= $this->url->get('Index/menuArea') ?>";
                $.ajax({
                    method: "GET",
                    dataType: "html",
                    url: link,
                    success: function (res) {
                        $('#manu').html(res);
                    }
                });
            } else {
                window.location.replace(login);
            }
        }
    });
}

function notif(tipe, judul, isi) {
    $('#notif').addClass('alert-' + tipe);
    $('#notif .notif-title').html(judul);
    $('#notif .notif-body').html(isi);
    $('#notif').fadeIn();
    setTimeout(function () {
        $('#notif').fadeOut(function () {
            $('#notif').removeClass('alert-' + tipe);
        });
    }, 4000);
}
</script>