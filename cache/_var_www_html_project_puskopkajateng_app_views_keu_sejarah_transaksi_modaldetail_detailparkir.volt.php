<!-- Modal -->
<div class="modal fade" id="detailParkir" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Transaksi Parkir</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-sort-numeric-asc"></i>
              Id Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: 321123123</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calendar-check-o"></i>
              Tgl Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Selasa, 20 April 2018</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-bus"></i>
              Jenis Kendaraan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Mobil</div>

          <div class="col-md-4">
            <b>
              <i class="fa fa-tags"></i>
              No. Polisi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: T4551WJ</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Harga Satuan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calculator"></i>
              Qty
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Total
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
      </div>
    </div>
    
  </div>
</div>