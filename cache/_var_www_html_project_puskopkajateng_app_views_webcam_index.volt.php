<style type="text/css">
	#results { float:right; margin:20px; padding:20px; border:1px solid; background:#ccc; }
</style>
<!-- Header content -->
<section class="content-header">
    <h1>
        Webcam
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-coffee"></i> Admin</a></li>
        <li class="active">Webcam</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-md-6">
	    	<div class="box">
	    		<div class="box-body">
					<div id="my_camera"></div>
					<br>	
					<button class="btn btn-default btn-flat" onclick="take_snapshot()">Tak Snapshoot</button>    			
	    		</div>
	    	</div>
    	</div>

    	<div class="col-md-6">
	    	<div class="box">
	    		<div class="box-body">
	    			<div id="results"></div>
					<form action="<?= $this->url->get('webcam/create') ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="foto" id="foto">
						<button type="submit">submit</button>
					</form>    			
	    		</div>
	    	</div>
    	</div>
    </div>
</section>


<script language="JavaScript">
	Webcam.set({
		width: 320,
		height: 240,
		image_format: 'jpeg',
		jpeg_quality: 90
	});

	Webcam.attach( '#my_camera' );
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
                $('#foto').val(raw_image_data);
                $('#results').html('<img src="'+data_uri+'"/>');
        } );
    }
</script>