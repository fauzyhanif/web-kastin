<div class="modal fade" id="batalTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Pembatalan Transaksi</h4>
      </div>

      <form name="batalTransaksi" method="POST" action="<?= $this->url->get('keu_sejarah_transaksi/batal') ?>" data-remote="data-remote">
        <input type="hidden" name="id_unit">
        <input type="hidden" name="id_transaksi">
        <div class="modal-body">
          <div class="form-group">
            <label>Anda yakin ingin membatalkan transaksi dgn id transaksi <span id="id-transaksi"></span>?</label>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-trash"></i> Batalkan
          </button>
        </div>
      </form>

    </div>
  </div>
</div>