<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Wahana</h4>
      </div>

      <form name="tiket_masuk" method="POST" data-remote="data-remote">
        <div class="modal-body">

          <div class="form-group col-md-12">
              <label>Tarif Weekday</label>
              <input type="text" name="harga_weekday" class="form-control tarif" placeholder=" Tarif Weekday" id="harga_weekday"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Weekend</label>
              <input type="text" name="harga_weekend" class="form-control tarif" placeholder=" Tarif Weekend" id="harga_weekend"> 
          </div>

          <div class="form-group col-md-12">
              <label>Jenis Pengunjung</label>
              <select class="form-control" name="id_jns_pengunjung">
                  <option value="">** Pilih Jenis Pengunjung</option>
                  <?php foreach ($jnsPengunjung as $x) { ?>
                      <option value="<?= $x->id_jenis_pengunjung ?>"><?= $x->nama ?></option>
                  <?php } ?>
              </select>
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>