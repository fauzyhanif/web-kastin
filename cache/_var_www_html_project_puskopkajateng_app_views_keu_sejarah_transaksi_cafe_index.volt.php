<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
  <h1>
    Sejarah Transaksi Cafe
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-edit"></i> Admin</a></li>
    <li class="active">Sejarah Transaksi</li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Data Transaksi Cafe</h3>
          <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-filter"></i> Filter
          </button>

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" role="document">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Filter Transaksi</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                    </div> 
                  </div>

                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="box-body" style="overflow: auto;" id="listView">
          <table id="data_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" width="9%">Id Transaksi</th>
                <th class="text-center" width="10%">User</th>
                <th class="text-center" width="15%">Tgl Transaksi</th>
                <th class="text-center" width="30%">Meja</th>
                <th class="text-center" width="8%">Jumlah</th>
                <th class="text-center" width="9%">Action</th>
              </tr>
            </thead>
            <tbody>

              <?php foreach ($cafe as $x) { ?>
              <tr>
                <td><?= $x->a->id_transaksi ?></td>
                <td><?= $x->nm_user ?></td>
                <td><?= $x->a->tgl_transaksi ?></td>
                <td><?= $x->a->meja ?></td>
                <td class="text-right"><?= $this->helper->formatRupiah($x->a->total) ?></td>
                <td class="text-center">

                  <button class="btn btn-primary btn-flat btn-xs" title="Edit"  onclick="return go_page('keu_sejarah_transaksi/formEdit/<?= $id_unit ?>/7/<?= $x->a->id_transaksi ?>')">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger btn-flat btn-xs" title="Batalkan Transaksi" data-toggle="modal" data-target="#batalTransaksi<?= $x->a->id_transaksi ?>" >
                    <i class="fa fa-trash"></i>
                  </button>

                  <div class="modal fade batalTransaksi" id="batalTransaksi<?= $x->a->id_transaksi ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">Pembatalan Transaksi</h4>
                        </div>

                        <form name="batalTransaksi" method="POST" action="<?= $this->url->get('keu_sejarah_transaksi/batalTransaksi') ?>">
                          <input type="hidden" name="id_unit" value="<?= $id_unit ?>">
                          <input type="hidden" name="id_transaksi" value="<?= $x->a->id_transaksi ?>">
                          <input type="hidden" name="item" value="7">
                          <input type="hidden" name="id_user" value="<?= $this->session->get('id_user') ?>">
                          <div class="modal-body">
                            <div class="form-group">
                              <label>Anda yakin ingin membatalkan transaksi dengan id transaksi <span id="id-transaksi"><?= $x->a->id_transaksi ?></span>?</label>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-danger btn-flat" data-dismiss="modal">
                              <i class="fa fa-remove"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary btn-flat">
                            <i class="fa fa-trash"></i> Batalkan
                            </button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <?php } ?>

            </tbody>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$(".select2").select2();
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable( {
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
        dom: 'Bfrtip',
        buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        searching: false
    });

});

// get harga dri db
$('select').on('change', function() {
 	var content = $(this).closest(".content");
	$.ajax({
        type: 'POST',
        url: '<?= $this->url->get('keu_sejarah_transaksi/hargaMenuCafe/') ?>'+this.value,
        dataType:'json',
        data: "id="+this.value,
        success: function(data){
        	content.find(".harga_satuan").val(data);
        	qty 	= content.find(".qty").val();
        	hasil 	= qty * data;
        	content.find(".harga_total").val(hasil);
        	total();
        }
    });
});

$(function() {
    $('.harga_satuan').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.qty').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.harga_total').keyup(function() {
        total();
    });
});

function total(){
    var sum = 0;
    $(".harga_total").each(function(){
        sum += +$(this).val();
    });
    $("#total").text(sum);
    $("#hargaTotal").val(sum);
}

(function() {

    $('form[name="editTransaksi"]').on('submit', function(e) {
        var form        = $(this);
        var url         = form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var idTransaksi = $('input[name="id_transaksi"]').val();
        var urlReload   = '<?= $this->url->get('keu_sejarah_transaksi/formEdit/') ?>'+idUnit+"/7/"+idTransaksi;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });

    $('form[name="batalTransaksi"]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var urlReload 	= '<?= $this->url->get('keu_sejarah_transaksi/index/') ?>'+idUnit+"/7";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_sejarah_transaksi/index/') ?>" + id_unit + "/7/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

// function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,hargaSatuan,qty,hargaDiskon,hargaTotal) {
// 	$('#idTransaksi').html(": "+idTransaksi);	
// 	$('#nmUser').html(": "+nmUser);
// 	$('#tglTransaksi').html(": "+tglTransaksi);
// 	$('#meja').html(": "+meja);
// 	$('#hargaDiskon').html(": Rp."+hargaDiskon);
// 	$('#hargaSatuan').html(": Rp."+hargaSatuan);
// 	$('#qty').html(": "+qty);
// 	$('#hargaTotal').html(": Rp."+hargaTotal);
// }
</script>

<!-- include popup -->
