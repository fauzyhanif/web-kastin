<section class="content-header">
  <h1>
    Dashboard
    <small>it all starts here</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
  </ol>
</section>
<section class="content" style="min-height: 0px">
  <div class="row">
    <section class="col-lg-6 col-lg-offset-3">
      <select class="form-control" onchange="filter(this.value)">
        <option value="">** Pilih Tahun</option>
        <?php foreach ($tahun as $x) { ?>
        <option <?php if ($x->thn == $thn) { ?> selected <?php } ?>><?= $x->thn ?></option>
        <?php } ?>
      </select>
    </section>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <section class="col-lg-6">
      <div class="box box-info">
        <div class="box-header text-center">
          <h3 class="box-title">Bandungan</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <div class="col-md-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h4><?= $pjgBdg[0]->org ?></h4>
                <p>Jumlah Pengunjung</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-stalker"></i>
              </div>
              <a href="javascript:void(0)" class="small-box-footer" onclick="return go_page('laporan_umum_bandungan')">
                More info 
              <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div><!-- ./col -->

          <div class="col-md-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h4>Rp. <?= $this->helper->formatRupiah($pendapatanBandungan) ?> ,-</h4>
                <p>Jumlah Pendapatan</p>
              </div>
              <div class="icon">
                <i class="ion ion-social-usd"></i>
              </div>
              <a href="javascript:void(0)" class="small-box-footer" onclick="return go_page('laporan_umum_bandungan')">
                More info 
              <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div><!-- ./col -->
        </div>
      </div>
    </section>

    <section class="col-lg-6">
      <div class="box box-info"> 
        <div class="box-header text-center">
          <h3 class="box-title">Kopeng</h3>
        </div>
        <div class="box-body chat" id="chat-box">
          <div class="col-md-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h4><?= $pjgKpg[0]->org ?></h4>
                <p>Jumlah Pengunjunga</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-stalker"></i>
              </div>
              <a href="javascript:void(0)" class="small-box-footer" onclick="return go_page('laporan_umum_kopeng')">
                More info 
              <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div><!-- ./col -->

          <div class="col-md-6 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h4>Rp. <?= $this->helper->formatRupiah($pendapatanKopeng) ?> ,-</h4>
                <p>Jumlah Pendapatan</p>
              </div>
              <div class="icon">
                <i class="ion ion-social-usd"></i>
              </div>
              <a href="javascript:void(0)" class="small-box-footer" onclick="return go_page('laporan_umum_kopeng')">
                More info 
              <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div><!-- ./col -->
        </div>
      </div>
    </section>

  </div>
  <div class="row">
    <section class="col-md-6">
      <div class="box box-info">
        <div class="box-header text-center">
          <h3 class="box-title ">Bandungan</h3> <br> <p>3 bulan terakhir</p>
        </div>
        <div class="box-body chat" id="chat-box">
          <div class="col-md-12 col-xs-12">
            <div class="chart">
              <canvas id="salesChart1" style="height: 180px;"></canvas>
            </div><!-- ./col -->
          </div><!-- ./col -->

        </div>
      </div>
    </section>

    <section class="col-md-6">
      <div class="box box-info">
        <div class="box-header text-center">
          <h3 class="box-title ">Kopeng</h3> <br> <p>3 bulan terakhir</p>
        </div>
        <div class="box-body chat" id="chat-box">
          <div class="col-md-12 col-xs-12">
            <div class="chart">
              <canvas id="salesChart2" style="height: 180px;"></canvas>
            </div><!-- ./col -->
          </div><!-- ./col -->

        </div>
      </div>
    </section>
  </div>      
</section><!-- /.content -->

<?php  
  date_default_timezone_set('Asia/Jakarta');
  $now        = date('Y-m');
  $end        = date('Y-m', strtotime("-2 months", strtotime($now)));
  $x          = new DateTime($end, new DateTimeZone("Asia/Jakarta"));
  $interval   = new DateInterval('P1M');
  $v          = new DateTime('');
  $period     = new DatePeriod($x, $interval, $v);
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>
<script type="text/javascript">
  new Chart(document.getElementById("salesChart1"), {
    type: 'bar',
    data: {
      labels: [<?php $this->helper->bulan() ?>],
      datasets: [
        {
          label: "Pengunjung",
          backgroundColor: "#3e95cd",
          data: [<?php  
                    foreach ($period as $dt) {
                            $this->helper->grafikPengunjungBandungan($dt->format("Y-m"));
                    }
                  ?>]
        }, {
          label: "Pendapatan",
          backgroundColor: "#8e5ea2",
          data: [<?php  
                    foreach ($period as $dt) {
                            $this->helper->grafikPendapatanBandungan($dt->format("Y-m"));
                    }
                  ?>]
        }
      ]
    },
    options: {

    }
  });

  new Chart(document.getElementById("salesChart2"), {
    type: 'bar',
    data: {
      labels: [<?php $this->helper->bulan() ?>],
      datasets: [
        {
          label: "Pengunjung",
          backgroundColor: "#3e95cd",
          data: [<?php  
                    foreach ($period as $dt) {
                            $this->helper->grafikPengunjungKopeng($dt->format("Y-m"));
                    }
                  ?>]
        }, {
          label: "Pendapatan",
          backgroundColor: "#8e5ea2",
          data: [<?php  
                    foreach ($period as $dt) {
                            $this->helper->grafikPendapatanKopeng($dt->format("Y-m"));
                    }
                  ?>]
        }
      ]
    },
    options: {

    }
  });

  function filter(thn) {
    var url = "<?= $this->url->get('dashboard/home/') ?>"+ thn;
    go_page(url);
  }

</script>

