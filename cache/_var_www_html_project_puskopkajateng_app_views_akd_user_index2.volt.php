
<?php $tanggalIndo = $this->helper->dateBahasaIndo(date('Y-m-d')); ?>
<?php $urlPost = $this->url->get('user/addUser'); ?>


<style>#waktu {
    font-weight: 400;
}

.weight-600 {
    font-weight: 600;
}

.width-10 {
    width: 10px;
}

.width-30 {
    width: 30px;
}

.margin-top-20 {
    margin-top: 20px;
}

.fa {
    padding-right: 3px;
}

.img-murid {
    height: 3em;
    float: left;
    margin-right: 10px;
    border-radius: 100%;
    cursor: pointer;
}

#imageModalSource {
    max-width: 75%;
    display: block;
    margin: 0 auto;
}

#imageModalDescription {
    font-style: italic;
}

.red {
    color: red;
}</style>


<style>
#form_filter {
    margin-bottom: 1.5em
}

table a {
    font-weight: 600;
}
</style>
<!-- Header content -->
<section class="content-header">
    <h1>
        User Pengguna
        <small>
            <i class="fa fa-calendar-o"></i> <?= $tanggalIndo ?> 
            <i class="fa fa-clock-o"></i> <span id="waktu">00:00:00</span>
        </small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Setup Admin</a></li>
        <li class="active">User</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-4">
            <div class="box box-default" id="formCrud">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah User</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="<?= $urlPost ?>" data-remote>
                            <div class="form-group col-md-12 text-center">
                                <img src="img/user.png" alt="Foto User" height="85px" class="img-circle text-center" id="foto">
                            </div>
                            
                            <input type="hidden" name="id_unit" value="<?= $id_unit ?>" id="id_unit">
                            <input type="hidden" name="jenis" value="1">
                            <div class="form-group col-md-12">
                                <label for="id_jenis">Akses</label>
<<<<<<< HEAD
                                <select class="form-control" name="akses" id="akses">
=======
                                <select class="form-control" name="akses" id="akses" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                                    <option value="">** Pilih Akses</option>
                                    <?php foreach ($akses as $x) { ?>
                                        <option value="<?= $x->id ?>"><?= $x->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="nama">Nama Lengkap</label>
<<<<<<< HEAD
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap">
=======
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                            </div>

                            <div class="form-group col-md-12">
                                <label for="nama">NIP</label>
<<<<<<< HEAD
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP">
=======
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                            </div>  

                            <div class="form-group col-md-5">
                                <label for="uid">UID</label>
<<<<<<< HEAD
                                <input type="text" class="form-control" name="uid" placeholder="UID" id="uid">
=======
                                <input type="text" class="form-control" name="uid" placeholder="UID" id="uid" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                            </div>

                            <div class="form-group col-md-7">
                                <label for="password">Password</label>
                                <div class="input-group">
<<<<<<< HEAD
                                    <input type="password" class="form-control active" name="password" placeholder="Password" id="pass">
=======
                                    <input type="password" class="form-control active" name="password" placeholder="Password" id="pass" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                                    <a class="input-group-addon" href="#" id="showPassword"><i class="fa fa-eye-slash"></i></a>
                                </div>                                
                            </div> 

                            <div class="form-group col-md-12">
                                <label for="area">Area Akses Menu</label>
<<<<<<< HEAD
                                <select name="area" id="area" class="form-control select2" multiple="multiple" data-placeholder="Pilih area akses:" style="width: 100%;">
=======
                                <select name="area" id="area" class="form-control select2" multiple="multiple" data-placeholder="Pilih area akses:" style="width: 100%;" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                                    <?php foreach ($area as $opt) { ?>
                                    <option value="<?= $opt->id ?>"><?= $opt->label_menu ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="usergroup">Usergroup</label>
<<<<<<< HEAD
                                <select name="usergroup" id="usergroup" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup:" style="width: 100%;">
=======
                                <select name="usergroup" id="usergroup" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup:" style="width: 100%;" required="">
>>>>>>> f341aa2b11d60952f2aa5ce9625073cb3924a7b0
                                    <?php foreach ($usergroup as $opt) { ?>
                                    <option value="<?= $opt->id ?>"><?= $opt->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Upload Foto</label>
                                <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
                            </div>
                            
                            <div class="form-group col-md-12">
                                <center>
                                  <?= $this->tag->image(['img/sdm/man-2.png', 'width' => '230', 'id' => 'uploadPreview1', 'class' => 'img-responsive']) ?>
                                </center>                                   
                            </div>                                        

                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default" id="reset"><i class="fa fa-refresh"></i>&nbsp; Reset</button>
                                    <button type="submit" class="btn btn-primary" id="submit"><i class="fa fa-send"></i>&nbsp; Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data User</h3>
                </div>
                <div class="box-body">

                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 10px" class="text-center">No</th>
                                <th class="text-center">UID</th>
                                <th class="text-center">Nama / No Identitas</th>
                                <th class="text-center">Akses</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?> <?php foreach ($data as $v) { ?>
                            
                            <?php if (($v->id_jenis == 1)) { ?>
                                <?php $folder = 'sdm'; ?>
                                <?php $nama_jenis = 'Guru/Sdm'; ?>
                            <?php } else { ?>
                                <?php $folder = 'mhs'; ?>
                                <?php $nama_jenis = 'Murid'; ?>                                
                            <?php } ?>

                            <?php if (($v->id_status == 'A')) { ?>
                                <?php $color = 'green'; ?>
                            <?php } else { ?>
                                <?php $color = 'red'; ?>
                            <?php } ?>

                            <tr id="data_<?= $v->login ?>" class="middle-row">
                                <td><?= $no ?></td>
                                <td><span class="badge bg-dark"><?= $v->login ?></span></td>
                                <td>
                                    <img src="img/<?= $folder ?>/<?= $v->foto ?>" alt="<?= $v->nama ?>" style="height: 3em; float: left; margin-right: 10px; border-radius: 50px">
                                    <span style="font-weight: 600"><?= $v->nama ?></span> <br/> 
                                    <?php if (($v->nip != '')) { ?>
                                        <?php if (($v->id_jenis == 1)) { ?>
                                            <?php $identitas = 'NIP'; ?>
                                        <?php } else { ?>
                                            <?php $identitas = 'NIS'; ?>
                                        <?php } ?>                                      
                                    <span class="label label-default"><?= $identitas ?></span> 
                                    <span class="label label-primary"><?= $v->nip ?></span>
                                    <?php } ?>  
                                </td> 
                                <td><?= $v->akses ?></td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs btn-flat" onclick="edit_data('<?= $v->id_user ?>')"><i class="glyphicon glyphicon-edit"></i> </a>

                                    <a class="btn btn-danger btn-xs btn-flat" onclick="delete_data('<?= $v->id_user ?>')"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                            <?php $no = $no + 1; ?> <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<script>$(function () {
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 50,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

    $(".select2").select2();

    $("img").on("error", function() {
        $(this).attr('src', 'img/user.png');
    });

    $('#showPassword').on("click", function(e) {
        if ($('#pass').hasClass('active')) {
            $('#pass').attr('type', 'text');
            $('#pass').removeClass('active');
            $('#showPassword > i').attr('class', 'fa fa-eye');
        } else {
            $('#pass').attr('type', 'password');
            $('#pass').addClass('active');
            $('#showPassword > i').attr('class', 'fa fa-eye-slash');
        }

        e.preventDefault();        
    });

    $("#reset").on("click", function() {
        $("#form_title").text('Tambah User');
        $("#foto").attr('src', 'img/user.png');
        $("#jenis").removeAttr('disabled');
        $("#nama").removeAttr('disabled');
        $("#nama").empty().trigger("change");
        $("#uid").removeAttr('readonly');
        $("#area").val(null).trigger("change");
        $("#usergroup").val(null).trigger("change");
        $("#reset").attr('class', 'btn btn-default');
        $("#reset").html('<i class="fa fa-refresh"></i>&nbsp; Reset');
        $("#submit").attr('onclick', 'save_data(\'addUser\')');
    });

    $('#filter_jenis').on("change", function() {
        if (this.value == '2') {
            $('#filter_tingkat').removeAttr('disabled');
        } else {
            $('#filter_tingkat').attr('disabled', 'disabled');
        }
    });

    $('#proses').on("click", function() {
        var jenis = $('#filter_jenis').val();
            tingkat = $('#filter_tingkat').val();
            url = "<?= $this->url->get('user/index') ?>";

        if (jenis == '2') {
            url = "<?= $this->url->get('user/index/') ?>" + jenis + '/' + tingkat;
        }

        go_page(url);
    });
});

function changeJenis(element) {
    var id = element.value;    
    var img_folder = 'sdm';
    // reset uid dan nama
    $('#uid').removeAttr('readonly');
    $("#nama").empty().trigger("change");
    // jika jenisnya murid maka gk boleh ganti uid
    if (id == 2) {
        $('#uid').attr('readonly', 'readonly');
        img_folder = 'mhs';
    }    

    $.ajax({
        url       : "<?= $this->url->get('user/searchNama/') ?>"+id,
        type      : "POST",
        dataType  : "json",
        data      : {'name' : 'value'},
        cache     : false,
        success   : function(response){            
            $("#nama").select2({
                placeholder: 'Cari nama...',
                data: response
            }).on("select2:selecting", function(evt) {                
                var img_name = evt.params.args.data.foto;                
                var getValue = evt.params.args.data.id;                
                $('#uid').val(getValue);
                $('#nip').val(getValue);
                $('#foto').attr('src', 'img/'+img_folder+'/'+img_name);
            });
        }
    });
}

function filter(id) {
    var url = "<?= $this->url->get('User/index/') ?>" + id;
    go_page(url);
}


function edit_data(id_user) {
    $.ajax({
        type      : "GET",
        url       : "<?= $this->url->get('user/get/') ?>"+id_user,
        dataType  : "html",
        // cache     : false,
        success   : function(file){
            $('#formCrud').html(file)
        }
    })   
}


function delete_data(id_user) {
    var id_unit     = $("#id_unit").val();
    var url_reload  = "<?= $this->url->get('user/index/') ?>"+id_unit;
    var url_target = '<?= $this->url->get('user/deleteUser') ?>/' + id_user;
    (new PNotify({
        title: 'Pesan Konfirmasi',
        text: 'Apakah Anda Yakin menghapus data ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function () {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: url_target,
            success: function (data) {
                reload_page2(url_reload);
                new PNotify({
                    title: 'Sukses',
                    text: 'Data berhasil dihapus',
                    type: 'success'
                });
            }
        });
    }).on('pnotify.cancel', function () {
        console.log('batal');
    });
}


(function() {

  $('form[data-remote]').on('submit', function(e) {
    var form = $(this);
    var url = form.prop('action');
    var id_unit     = $("#id_unit").val();
    var url_reload  = "<?= $this->url->get('user/index/') ?>"+id_unit;

    $.ajax({
      type: 'POST',
      url: url,
      dataType:'json',
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function(response){
        reload_page2(url_reload);
        new PNotify({
            title: data.title,
            text: data.text,
            type: data.type
        });
      }
    });

    e.preventDefault();
  });

})();

function PreviewImage(id) {
  $('input[name="remove_image"]').val('');
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadImage"+id).files[0]);

  oFReader.onload = function (oFREvent) {
    document.getElementById("uploadPreview"+id).src = oFREvent.target.result;
  };
};</script>