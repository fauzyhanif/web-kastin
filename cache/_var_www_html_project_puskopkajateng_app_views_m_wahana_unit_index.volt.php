<!-- Header content -->
<section class="content-header">
    <h1>
        Wahana
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-coffee"></i> Admin</a></li>
        <li class="active">Wahana</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Wahana</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="<?= $this->url->get('M_wahana_unit/create') ?>" data-remote>
                            <input type="hidden" name="id_unit" value="<?= $id_unit ?>" id="id_unit">
                            <div class="form-group col-md-12">
                                <label>Nama Wahana</label>
                                <input type="text" name="nama" class="form-control" placeholder=" Nama Wahana" id="nama"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Tarif Individu</label>
                                <input type="text" name="harga_individu" class="form-control tarif" placeholder=" Tarif Individu" id="harga_individu"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Tarif Lokal</label>
                                <input type="text" name="harga_lokal" class="form-control tarif" placeholder=" Tarif Lokal" id="tarif_lokal"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Tarif Rombongan</label>
                                <input type="text" name="harga_rombongan" class="form-control tarif" placeholder=" Tarif Rombongan" id="harga_rombongan"> 
                            </div>
                            
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default btn-flat" onclick="return reload_page2('M_wahana_unit/index/<?= $id_unit ?>')">
                                        <i class="fa fa-refresh"></i>&nbsp; Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                        <i class="fa fa-send"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-9">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Wahana</h3>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%" rowspan="2">No</th>
                                <th class="text-center" rowspan="2">Nama Wahana</th>
                                <th class="text-center" width="45%" colspan="3">Tarif</th>
                                <th class="text-center" rowspan="2" width="5%">Aktif</th>
                                <th class="text-center" rowspan="2" width="5%">Aksi</th>
                            </tr>
                            <tr>
                                <th class="text-center" width="10">Individu</th>
                                <th class="text-center" width="10">Lokal</th>
                                <th class="text-center" width="10">Rombongan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?> <?php foreach ($data as $x) { ?>
                            <tr>
                                <td class="text-center"><?= $no ?>.</td>
                                <td><?= $x->nama ?></td>
                                <td class="text-right"><?= $this->helper->formatRupiah($x->harga_individu) ?></td>
                                <td class="text-right"><?= $this->helper->formatRupiah($x->harga_lokal) ?></td>
                                <td class="text-right"><?= $this->helper->formatRupiah($x->harga_rombongan) ?></td>
                                <td class="text-center">
                                    <?php if ($x->aktif == 'Y') { ?>
                                    <span class="badge bg-green"><?= $x->aktif ?></span>
                                    <?php } else { ?> 
                                    <span class="badge bg-red"><?= $x->aktif ?></span>
                                    <?php } ?>
                                </td>
                                <td class="text-center"> 
                                    <a  class="btn btn-primary btn-xs btn-flat" 
                                        onclick="edit_data('<?= $x->id ?>','<?= $x->nama ?>','<?= $x->harga_individu ?>','<?= $x->harga_lokal ?>','<?= $x->harga_rombongan ?>','<?= $x->aktif ?>')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <!-- <a  class="btn btn-danger btn-xs btn-flat" 
                                        onclick="delete_data('<?= $x->id_wahana ?>', '<?= $x->nama ?>')"
                                        data-toggle="modal" 
                                        data-target="#delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a> -->
                                </td>
                            </tr>
                            <?php $no = $no + 1; ?> <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Wahana</h4>
      </div>

      <form name="wahana" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group col-md-12">
              <label>Nama Wahana</label>
              <input type="text" name="nama" class="form-control" placeholder=" Nama Wahana" id="nama"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Individu</label>
              <input type="text" name="harga_individu" class="form-control tarif" placeholder=" Tarif Individu" id="harga_individu"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Lokal</label>
              <input type="text" name="harga_lokal" class="form-control tarif" placeholder=" Tarif Lokal" id="tarif_lokal"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Rombongan</label>
              <input type="text" name="harga_rombongan" class="form-control tarif" placeholder=" Tarif Rombongan" id="harga_rombongan"> 
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- formDelete.volt -->

<!-- include js file -->
<script>$(function () {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('m_wahana_unit/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, nama, harga_individu, harga_lokal, harga_rombongan, aktif) {
  var form = $('form[name="wahana"]').attr('action', '<?= $this->url->get('m_wahana_unit/update/') ?>' + id);
  form.find('[name="nama"]').val(nama);
  form.find('[name="harga_individu"]').val(harga_individu);
  form.find('[name="harga_lokal"]').val(harga_lokal);
  form.find('[name="harga_rombongan"]').val(harga_rombongan);
  form.find('select[name="aktif"]').val(aktif);
}

function delete_data(id, nama) {
    $('input#id').val(id);
    $('#nama').text(nama);
}


</script>