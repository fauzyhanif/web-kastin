<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" name="" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
    <h1>
        Transaksi Lain-lain 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-text-o"></i> Admin</a></li>
        <li class="active">Transaksi Lain-lain</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Tanggal : <?= $this->helper->konversi_tgl($date1) ?> <?php if ($date2 != '') { ?> - <?= $this->helper->konversi_tgl($date2) ?><?php } ?></h4> 
                    <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-filter"></i> Filter</button>
                    <?php if (!empty($username)) { ?>
                    <h4>Nama : <?= $username ?></h4>
                    <?php } ?>

                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-sm" role="document"">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Filter Laporan Transaksi</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                              </div> 
                            </div>

                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                              </div> 
                            </div>

                            <div class="form-group col-md-12">
                              <select class="form-control" id="id_user">
                                  <option value="all">** Pilih User</option>
                                  <?php foreach ($user as $x) { ?>
                                  <option value="<?= $x->id_user ?>"><?= $x->nama ?></option>
                                  <?php } ?>
                              </select> 
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="box-body" style="overflow: auto;" id="listView">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                        	<tr>
                        		<th class="text-center" width="5%">No</th>
                        		<th class="text-center" width="15%">Tgl Transaksi</th>
                        		<th class="text-center">Item</th>
                        		<th class="text-center" width="10%">Qty</th>
                        		<th class="text-center" width="20%">Jumlah</th>
                        	</tr>
                        </thead>
                        <div style="position: relative; overflow: auto; max-height: 100px">
                            
                        </div>
                    </table>

                    <div class="col-md-12 total">
                        <b>
                            <div class="col-md-6">
                                <i class="fa fa-dollar"></i>
                                Jumlah Pendapatan    
                            </div>
                            <div class="col-md-6">
                               : 
                            </div>
                        </b>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});

	$('#data_table').DataTable( {
		"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
	    dom: 'Bfrtip',
	    buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print'
	    ],
	    searching: false
	});

})

function filter()
{
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var id_user = $('#id_user').val();
    // alert(id_user,date1,date2);
    // alert(date2);
    // alert(date2);
	var url		= "<?= $this->url->get('keu_laporan_unit/index/') ?>" + id_unit + "/" + id_user + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
</script>
