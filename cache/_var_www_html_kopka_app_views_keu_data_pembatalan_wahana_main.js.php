
$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_data_pembatalan/index/') ?>" + id_unit + "/4/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,hargaSatuan,qty,hargaTotal,userCancel,cancelAt) {
	$('#idTransaksi').html(": "+idTransaksi);	
	$('#nmUser').html(": "+nmUser);
	$('#tglTransaksi').html(": "+tglTransaksi);
	$('#item').html(": "+item);
	$('#hargaSatuan').html(": Rp."+hargaSatuan);
	$('#qty').html(": "+qty);
	$('#hargaTotal').html(": Rp."+hargaTotal);
    $('#userCancel').html(": "+userCancel);
    $('#cancelAt').html(": "+cancelAt);
}



