<!-- Header content -->
<section class="content-header">
    <h1>
        Wahana
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-coffee"></i> Admin</a></li>
        <li class="active">Wahana</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Wahana</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="<?= $this->url->get('M_wahana_unit/create') ?>" data-remote>
                            <input type="hidden" name="id_unit" value="<?= $id_unit ?>" id="id_unit">
                            <div class="form-group col-md-12">
                                <label>Nama Wahana</label>
                                <input type="text" name="nama" class="form-control" placeholder=" Nama Wahana" id="nama"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Harga Weekday</label>
                                <input type="text" name="harga_weekday" class="form-control tarif" placeholder=" Harga Weekday" id="tarif_weekday"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Harga Weekend</label>
                                <input type="text" name="harga_weekend" class="form-control tarif" placeholder=" Harga Weekend" id="tarif_weekend"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Jenis Pengunjung</label>
                                <select class="form-control" name="id_jns_pengunjung">
                                    <option value="">** Pilih Jenis Pengunjung</option>
                                    <?php foreach ($jnsPengunjung as $x) { ?>
                                        <option value="<?= $x->id_jenis_pengunjung ?>"><?= $x->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default btn-flat" onclick="return reload_page2('M_wahana_unit/index/<?= $id_unit ?>')">
                                        <i class="fa fa-refresh"></i>&nbsp; Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                        <i class="fa fa-send"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-9">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Wahana</h3>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">No</th>
                                <th class="text-center">Nama Wahana</th>
                                <th class="text-center" width="15%">Tarif Weekday</th>
                                <th class="text-center" width="15%">Tarif Weekend</th>
                                <th class="text-center" width="15%">Jns Peng.</th>
                                <th class="text-center" width="10%">Aktif</th>
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?> <?php foreach ($data as $x) { ?>
                            <tr>
                                <td class="text-center"><?= $no ?>.</td>
                                <td><?= $x->nama ?></td>
                                <td class="text-right"><?= $this->helper->formatRupiah($x->harga_weekday) ?></td>
                                <td class="text-right"><?= $this->helper->formatRupiah($x->harga_weekend) ?></td>
                                <td><?= $x->pengunjung ?></td>
                                <td class="text-center">
                                    <?php if ($x->aktif == 'Y') { ?>
                                    <span class="badge bg-green"><?= $x->aktif ?></span>
                                    <?php } else { ?> 
                                    <span class="badge bg-red"><?= $x->aktif ?></span>
                                    <?php } ?>
                                </td>
                                <td class="text-center"> 
                                    <a  class="btn btn-primary btn-xs btn-flat" 
                                        onclick="edit_data('<?= $x->id ?>','<?= $x->nama ?>','<?= $x->harga_weekday ?>','<?= $x->harga_weekend ?>','<?= $x->id_jns_pengunjung ?>','<?= $x->aktif ?>')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <!-- <a  class="btn btn-danger btn-xs btn-flat" 
                                        onclick="delete_data('<?= $x->id_wahana ?>', '<?= $x->nama ?>')"
                                        data-toggle="modal" 
                                        data-target="#delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a> -->
                                </td>
                            </tr>
                            <?php $no = $no + 1; ?> <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Wahana</h4>
      </div>

      <form name="wahana" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group col-md-12">
              <label>Nama Wahana</label>
              <input type="text" name="nama" class="form-control" placeholder=" Nama Wahana" id="nama"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Weekday</label>
              <input type="text" name="harga_weekday" class="form-control tarif" placeholder=" Tarif Weekday" id="harga_weekday"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Weekend</label>
              <input type="text" name="harga_weekend" class="form-control tarif" placeholder=" Tarif Weekend" id="harga_weekend"> 
          </div>

          <div class="form-group col-md-12">
              <label>Jenis Pengunjung</label>
              <select class="form-control" name="id_jns_pengunjung">
                  <option value="">** Pilih Jenis Pengunjung</option>
                  <?php foreach ($jnsPengunjung as $x) { ?>
                      <option value="<?= $x->id_jenis_pengunjung ?>"><?= $x->nama ?></option>
                  <?php } ?>
              </select>
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- formDelete.volt -->

<!-- include js file -->
<script>$(function () {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('m_wahana_unit/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, nama, harga_weekday, harga_weekend, id_jns_pengunjung, aktif) {
  var form = $('form[name="wahana"]').attr('action', '<?= $this->url->get('m_wahana_unit/update/') ?>' + id);
  form.find('select[name="id_jns_pengunjung"]').val(id_jns_pengunjung);
  form.find('[name="nama"]').val(nama);
  form.find('[name="harga_weekday"]').val(harga_weekday);
  form.find('[name="harga_weekend"]').val(harga_weekend);
  form.find('select[name="aktif"]').val(aktif);
}

function delete_data(id, nama) {
    $('input#id').val(id);
    $('#nama').text(nama);
}


</script>