<!-- Header content -->
<section class="content-header">
    <h1>
        Jenis Kendaraan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-automobile"></i> Admin</a></li>
        <li class="active">Jenis Kendaraan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-4">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Jenis Kendaraan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="<?= $this->url->get('M_jenis_kendaraan/create') ?>" data-remote>
                            <div class="form-group col-md-12">
                                <label>Kode jenis kendaraan</label>
                                <input type="text" name="kode_kendaraan" class="form-control" placeholder=" Kode Jenis Kendaraan" id="kode_kendaraan"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Jenis kendaraan</label>
                                <input type="text" name="nama_kendaraan" class="form-control" placeholder=" Jenis Kendaraan" id="nama_kendaraan"> 
                            </div>
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default btn-flat" onclick="return reload_page2('M_jenis_kendaraan/index')">
                                        <i class="fa fa-refresh"></i>&nbsp; Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                        <i class="fa fa-send"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- right column -->
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Jenis Kendaraan</h3>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">No</th>
                                <th class="text-center" width="25%">Kode Kendaraan</th>
                                <th class="text-center">Jenis Kendaraan</th>
                                <th class="text-center" width="10%">Aktif</th>
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?> <?php foreach ($data as $x) { ?>
                            <tr>
                                <td class="text-center"><?= $no ?>.</td>
                                <td class="text-center"><?= $x->kode_kendaraan ?></td>
                                <td><?= $x->nama_kendaraan ?></td>
                                <td class="text-center">
                                    <?php if ($x->aktif == 'Y') { ?>
                                    <span class="badge bg-green"><?= $x->aktif ?></span>
                                    <?php } else { ?> 
                                    <span class="badge bg-red"><?= $x->aktif ?></span>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <a  class="btn btn-primary btn-xs btn-flat" 
                                        onclick="edit_data('<?= $x->id_jns_kendaraan ?>', '<?= $x->nama_kendaraan ?>', '<?= $x->kode_kendaraan ?>')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <!-- <a  class="btn btn-danger btn-xs btn-flat" 
                                        onclick="delete_data('<?= $x->id_jns_kendaraan ?>', '<?= $x->nama_kendaraan ?>')"
                                        data-toggle="modal" 
                                        data-target="#delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a> -->
                                </td>
                            </tr>
                            <?php $no = $no + 1; ?> <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clear_form()">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Jenis Kendaraan</h4>
      </div>

      <form name="jenis_kendaraan" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group">
            <label>Kode Kendaraan</label>
            <input type="text" name="kode_kendaraan" class="form-control" id="kode_kendaraan" placeholder=" Kode Kendaraan">
          </div>
          <div class="form-group">
            <label>Jenis Kendaraan</label>
            <input type="text" name="nama_kendaraan" class="form-control" id="nama_kendaraan" placeholder=" Jenis Kendaraan">
          </div>
          <div class="form-group">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Delete Jenis Kendaraan</h4>
      </div>

      <form name="unit" method="POST" action="<?= $this->url->get('M_jenis_kendaraan/delete') ?>" data-remote="data-remote">
        <input type="hidden" name="id" id="id">
        <div class="modal-body">
          <div class="form-group">
            <label>Anda yakin ingin menghapus jenis kendaraan <span id="nama"></span>?</label>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-trash"></i> Remove
          </button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- include js file -->
<script>$(function () {
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2('M_jenis_kendaraan/index');
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, nama_kendaraan,kode_kendaraan) {
  var form = $('form[name="jenis_kendaraan"]').attr('action', '<?= $this->url->get('M_jenis_kendaraan/update/') ?>' + id);
  form.find('[name="kode_kendaraan"]').val(kode_kendaraan);
  form.find('[name="nama_kendaraan"]').val(nama_kendaraan);
}

function delete_data(id, nama_kendaraan) {
    $('input#id').val(id);
    $('#nama').text(nama_kendaraan);
}

</script>