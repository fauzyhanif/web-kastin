<!-- Header content -->
<section class="content-header">
    <h1>
        Promo
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-coffee"></i> Admin</a></li>
        <li class="active">Promo</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
      <!--   <div class="col-md-4">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title" id="form_title">Tambah Promo</h3>
                </div>
                <div class="box-body pad">
                    <div class="row">
                        <form id="form_input" method="POST" action="<?= $this->url->get('M_promo/create') ?>" data-remote>
                            <input type="hidden" name="id_unit" value="<?= $id_unit ?>" id="id_unit">
                            <div class="form-group col-md-12">
                                <label>Item</label>
                                <select class="form-control" name="id_item">
                                    <option value="">** Pilih Items</option>
                                    <?php foreach ($item as $x) { ?>
                                    <option value="<?= $x->id ?>"><?= $x->nama ?></option>                                    
                                    <?php } ?>                                    
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Nama Promo</label>
                                <input type="text" name="text" class="form-control" placeholder=" Nama Promo" id="nama"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Dari Qty</label>
                                <input type="text" name="qty1" class="form-control tarif" placeholder=" Dari Qty" id="qty1"> 
                            </div>

                            <div class="form-group col-md-12">
                                <label>Sampai Qty</label>
                                <input type="text" name="qty2" class="form-control tarif" placeholder=" Sampai Qty" id="qty2"> 
                            </div>
                            
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default btn-flat" onclick="return reload_page2('M_promo/index/<?= $id_unit ?>')">
                                        <i class="fa fa-refresh"></i>&nbsp; Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                        <i class="fa fa-send"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- right column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Promo</h3>
                    <button class="btn btn-success box-tools pull-right btn-flat btn-sm" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle"></i> Add Promo</button>

                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-sm" role="document"">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Promo</h4>
                          </div>
                         
                         <form id="form_input" method="POST" action="<?= $this->url->get('M_promo/create') ?>" data-remote>
                          <div class="modal-body">
                                <input type="hidden" name="id_unit" value="<?= $id_unit ?>" id="id_unit">
                                <div class="form-group col-md-12">
                                    <label>Item</label>
                                    <select class="form-control" name="id_item">
                                        <option value="">** Pilih Item</option>
                                        <?php foreach ($item as $x) { ?>
                                        <option value="<?= $x->id ?>"><?= $x->nama ?></option>                                    
                                        <?php } ?>                                    
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Nama Promo</label>
                                    <input type="text" name="text" class="form-control" placeholder=" Nama Promo" id="nama"> 
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Dari Qty</label>
                                    <input type="text" name="qty1" class="form-control tarif" placeholder=" Dari Qty" id="qty1"> 
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Sampai Qty</label>
                                    <input type="text" name="qty2" class="form-control tarif" placeholder=" Sampai Qty" id="qty2"> 
                                </div>
                                
                                
                          </div>
                          <div class="modal-footer">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-default btn-flat" onclick="return reload_page2('M_promo/index/<?= $id_unit ?>')">
                                        <i class="fa fa-refresh"></i>&nbsp; Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                        <i class="fa fa-send"></i>&nbsp; Simpan
                                    </button>
                                </div>
                            </div>
                          </div>
                        </form>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No</th>
                                <th class="text-center">Nama Promo</th>
                                <th class="text-center">Item</th>
                                <th class="text-center" width="1%">Dari qty</th>
                                <th class="text-center" width="10%">Sampai qty</th>
                                <th class="text-center" width="5%">Aktif</th>
                                <th class="text-center" width="5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?> <?php foreach ($data as $x) { ?>
                            <tr>
                                <td class="text-center"><?= $no ?>.</td>
                                <td><?= $x->text ?></td>
                                <td><?= $x->nama ?></td>
                                <td class="text-center"><?= $x->qty1 ?></td>
                                <td class="text-center"><?= $x->qty2 ?></td>
                                <td class="text-center">
                                    <?php if ($x->aktif == 'Y') { ?>
                                    <span class="badge bg-green"><?= $x->aktif ?></span>
                                    <?php } else { ?> 
                                    <span class="badge bg-red"><?= $x->aktif ?></span>
                                    <?php } ?>
                                </td>
                                <td class="text-center"> 
                                    <a  class="btn btn-primary btn-xs btn-flat" 
                                        onclick="edit_data('<?= $x->id ?>','<?= $x->text ?>','<?= $x->qty1 ?>','<?= $x->qty2 ?>','<?= $x->id_unit ?>','<?= $x->aktif ?>','<?= $x->id_item ?>')" 
                                        data-toggle="modal" 
                                        data-target="#update">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $no = $no + 1; ?> <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include popup -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Paket</h4>
      </div>

      <form name="promo" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <input type="hidden" name="id_unit" value="">
          <div class="form-group col-md-12">
              <label>Item</label>
              <select class="form-control" name="id_item">
                  <option value="">** Pilih Item</option>
                  <?php foreach ($item as $x) { ?>
                  <option value="<?= $x->id ?>"><?= $x->nama ?></option>                                    
                  <?php } ?>                                    
              </select>
          </div>
          
          <div class="form-group col-md-12">
            <label>Nama Promo</label>
            <input type="text" name="text" class="form-control" placeholder=" Nama Promo" id="nama"> 
          </div>

          <div class="form-group col-md-12">
            <label>Dari Qty</label>
            <input type="text" name="qty1" class="form-control tarif" placeholder=" Dari Qty" id="qty1"> 
          </div>  

          <div class="form-group col-md-12">
            <label>Sampai Qty</label>
            <input type="text" name="qty2" class="form-control tarif" placeholder=" Sampai Qty" id="qty2"> 
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- include js file -->
<script>$(function () {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('m_promo/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, text, qty1,qty2, id_unit, aktif, id_item) {
  var form = $('form[name="promo"]').attr('action', '<?= $this->url->get('m_promo/update/') ?>' + id);
  form.find('[name="id_unit"]').val(id_unit);
  form.find('[name="text"]').val(text);
  form.find('[name="qty1"]').val(qty1);
  form.find('[name="qty2"]').val(qty2);
  form.find('select[name="id_item"]').val(id_item);
  form.find('select[name="aktif"]').val(aktif);
}

function delete_data(id, nama) {
    $('input#id').val(id);
    $('#nama').text(nama);
}


</script>