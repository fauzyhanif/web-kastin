<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Penginapan</h4>
      </div>

      <form name="penginapan" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <input type="hidden" name="id_unit" value="">
          <div class="form-group col-md-12">
              <label>Nama Kamar</label>
              <input type="text" name="nama" class="form-control" placeholder=" Nama Kamar" id="nama"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Weekday</label>
              <input type="text" name="tarif_weekday" class="form-control tarif" placeholder=" Tarif Weekday" id="tarif_weekday"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Weekend</label>
              <input type="text" name="tarif_weekend" class="form-control tarif" placeholder=" Tarif Weekend" id="tarif_weekend"> 
          </div>

          <div class="form-group col-md-12">
              <label>Kapasitas</label>
              <input type="text" name="kapasitas" class="form-control" placeholder=" Kapasitas" id="kapasitas"> 
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>