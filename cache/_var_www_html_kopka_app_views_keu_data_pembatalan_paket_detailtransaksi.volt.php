<!-- Modal -->
<div class="modal fade" id="detailTransaksi" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Transaksi Paket</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-sort-numeric-asc"></i>
              Id Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="idTransaksi"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-user"></i>
              User
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="nmUser"></div>
          
          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calendar-check-o"></i>
              Tgl Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="tglTransaksi"></div>


          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-bus"></i>
              Item
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="item"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Harga Satuan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="hargaSatuan"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calculator"></i>
              Qty
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="qty"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-scissors"></i>
              Diskon
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="hargaDiskon"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Total
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="hargaTotal"></div>

          <hr>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-user"></i>
              Yang membatalkan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="userCancel"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calendar-check-o"></i>
              Tgl Pembatalan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="cancelAt"></div>

        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
      </div>
    </div>
    
  </div>
</div>