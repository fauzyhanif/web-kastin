<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
  <h1>
    Edit Transaksi Parkir
  </h1>
  <ol class="breadcrumb">
    <li>
      <button class="btn bg-navy btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/<?= $id_unit ?>/2')">
        <i class="fa  fa-chevron-circle-left"></i>
        Back
      </button>
    </li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-6 col-md-offset-3">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Edit Transaksi Parkir</h3>
        </div>

        <div class="box-body">
          <div class="col-md-12">
            <div class="col-md-4">Terakhir update</div>
            <div class="col-md-8">: <?= $data[0]->a->updated_at ?></div>
            <div class="col-md-4">Oleh</div>
            <div class="col-md-8">: <?= $data[0]->user_update ?></div>
            <hr>
            <form name="editTransaksi" id="parkir" method="POST" data-remote action="<?= $this->url->get('keu_sejarah_transaksi/editTransaksi') ?>">
              <input type="hidden" name="id_unit" value="<?= $id_unit ?>">
              <input type="hidden" name="id_transaksi" value="<?= $data[0]->a->id_transaksi ?>">
              <input type="hidden" name="user_update" value="<?= $this->session->get('id_user') ?>">
              <input type="hidden" name="item" value="2">
              <div class="modal-body">
                <div class="form-group col-md-12">
                  <label>id Transaksi</label>
                  <input type="text" name="id_transaksi"  class="form-control" disabled="" id="idTransaksi" value="<?= $data[0]->a->id_transaksi ?>">
                </div>

                <div class="form-group col-md-12">
                    <label>User</label>
                    <input type="text"  class="form-control" disabled="" id="nmUser" value="<?= $data[0]->nm_user ?>"> 
                </div>

                <div class="form-group col-md-12">
                    <label>Tgl Transaksi</label>
                    <input type="text" class="form-control" disabled="" id="tglTransaksi" value="<?= $data[0]->a->tgl_transaksi ?>"> 
                </div>

                <div class="form-group col-md-12">
                  <label>Item</label>
                  <select class="form-control" name="id_item">
                    <?php foreach ($parkir as $x) { ?>

                      <option value="<?= $x->id ?>" <?php if ($data[0]->a->id_item == $x->id) { ?> selected="" <?php } ?>><?= $x->nama ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label>No Polisi</label>
                  <input type="text" name="no_polisi" class="form-control" id="no_polisi" value="<?= $data[0]->a->no_polisi ?>">
                </div>

                <div class="form-group col-md-12">
                  <label>Quantity</label>
                  <input type="text" name="qty" class="form-control" id="qty" value="<?= $data[0]->a->qty ?>">
                </div>

                <div class="form-group col-md-12">
                  <label>Harga Satuan</label>
                  <input type="text" name="harga_satuan" id="harga_satuan" class="form-control tarif" value="<?= $data[0]->a->harga_satuan ?>">
                </div>

                <div class="form-group col-md-12">
                  <label>Total Harga</label>
                  <input type="text" name="harga_total" id="harga_total" class="form-control tarif" value="<?= $data[0]->a->harga_total ?>">
                </div>
              </div>
              <div class="modal-footer" style="border-top: none;">
                <a class="btn btn-danger btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/<?= $id_unit ?>/2')">
                  <i class="fa fa-remove"></i> Cancel
                </a>
                <button type="submit" class="btn btn-primary btn-flat">
                <i class="fa fa-send"></i> Save Change
                </button>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});

$(function() {
    $('#harga_satuan').keyup(function() {
        harga = $(this).val().replace(/\./g, '');
        qty   = $('#qty').val();
        hasil = harga * qty;
        $('#harga_total').val(hasil);
    });
    
    $('#qty').keyup(function() {
        qty     = $(this).val();
        harga   = $('#harga_satuan').val().replace(/\./g, '');
        hasil   = qty * harga;
        $('#harga_total').val(hasil);
    });
    
});

(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit  	= $('input[name="id_unit"]').val();
        var urlReload 	= '<?= $this->url->get('keu_sejarah_transaksi/index/') ?>'+idUnit+'/2';

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_sejarah_transaksi/index/') ?>" + id_unit + "/2/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,noPolisi,hargaSatuan,qty,hargaTotal) {
	$('#idTransaksi').html(": "+idTransaksi);	
	$('#nmUser').html(": "+nmUser);
	$('#tglTransaksi').html(": "+tglTransaksi);
	$('#item').html(": "+item);
	$('#noPolisi').html(": "+noPolisi);
	$('#hargaSatuan').html(": Rp."+hargaSatuan);
	$('#qty').html(": "+qty);
	$('#hargaTotal').html(": Rp."+hargaTotal);
}


function editTransaksix(idUnit,idTransaksi,nmUser,tglTransaksi,item,noPolisi,hargaSatuan,qty,hargaTotal) {
	$('form[id="parkir"]').attr('action', '<?= $this->url->get('keu_sejarah_transaksi/editTransaksi/') ?>' + idTransaksi);
	$('input[name="id_unit"]').val(idUnit);
	$('input[id="idTransaksi"]').val(idTransaksi);	
	$('input[id="nmUser"]').val(nmUser);
	$('input[id="tglTransaksi"]').val(tglTransaksi);
	$('input[name="id_item"]').val(item);
	$('input[name="no_polisi"]').val(noPolisi);
	$('input[name="harga_satuan"]').val(hargaSatuan);
	$('input[name="qty"]').val(qty);
	$('input[name="harga_total"]').val(hargaTotal);
}

</script>