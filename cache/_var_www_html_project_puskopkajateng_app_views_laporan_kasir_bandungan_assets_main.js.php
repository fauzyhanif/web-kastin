$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});

	$('#data_table').DataTable( {
	    dom: 'Bfrtip',
	    buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print'
	    ],
	    searching: false
	});

})

function filter()
{
	var date1 	= $('#date1').val();
	var url		= "<?= $this->url->get('laporan_kasir_bandungan/index/') ?>" + date1 ;
	go_page(url);
}
