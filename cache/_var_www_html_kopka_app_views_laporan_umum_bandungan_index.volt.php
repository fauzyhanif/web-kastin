<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	margin-left: 220px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<!-- Header content -->
<section class="content-header">
    <h1>
        Laporan Umum Bandungan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-text-o"></i> Admin</a></li>
        <li class="active">Laporan Umum Bandungan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header text-center">
                	<div class="col-md-8 col-offset-2 form-filter">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                        </div>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                        </div>
                		<button onclick="filter()" class="btn btn-success btn-flat btn-filter"><i class="fa fa-filter"></i> Filter</button>
                	</div>
                </div>
                <div class="box-body" style="overflow: auto;" id="listView">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center kolom" rowspan="2" width="5%"  style="padding-bottom: 28px;">No</th>
                                <th class="text-center col-th" rowspan="2" width="13%" style="padding-bottom: 28px;">Tanggal</th>
                                <th class="text-center" colspan="2">Parkir</th>
                                <?php foreach ($column as $x) { ?>
                                <th class="text-center" colspan="2"><?= $x->nama ?></th>
                                <?php } ?>
                                <th class="text-center col-th" rowspan="2" width="13%" style="padding-bottom: 28px;">Total</th>
                            </tr>
                            <tr>
                            	<th class="text-center">Jml Kendaraan</th>
                                <th class="text-center">Uang</th>
                                <th class="text-center">Jml Pengunjung</th>
                            	<th class="text-center">Uang</th>
                            	<th class="text-center">Jml Pengunjung</th>
                            	<th class="text-center">Uang</th>
                            	<th class="text-center">Jml Pengunjung</th>
                                <th class="text-center">Uang</th>
                                <th class="text-center">Jml Pengunjung</th>
                            	<th class="text-center">Uang</th>
                            </tr>
                        </thead>
                        <tbody class="text-bold">
                            <?php $no = 1; ?>
                            <?php $ttl_org_tiket_masuk = 0; ?>
                            <?php $ttl_uang_tiket_masuk = 0; ?>
                            <?php $ttl_org_flying_fox = 0; ?>
                            <?php $ttl_uang_flying_fox = 0; ?>
                            <?php $ttl_org_mini_train = 0; ?>
                            <?php $ttl_uang_mini_train = 0; ?>
                            <?php $ttl_org_sepeda_udara = 0; ?>
                            <?php $ttl_uang_sepeda_udara = 0; ?>
                            <?php $ttl_jml_kendaraan = 0; ?>
                            <?php $ttl_uang_kendaraan = 0; ?>
                            <?php $ttl = 0; ?>
                            <?php foreach ($data as $x) { ?>
                                
                                <tr>
                                    <td class="text-center"><?= $no ?>.</td>
                                    <td class="text-center"><?= $this->helper->konversi_tgl($x->tgl) ?></td>
                                    <td class="text-center"><?= $x->jml_kendaraan ?></td>
                                    <td class="text-right"><?= number_format($x->uang_kendaraan,0,',','.'); ?></td>
                                    <td class="text-center"><?= $x->org_tiket_masuk ?></td>
                                    <td class="text-right"><?= number_format($x->uang_tiket_masuk,0,',','.'); ?></td>
                                    <td class="text-center"><?= $x->org_flying_fox ?></td>
                                    <td class="text-right"><?= number_format($x->uang_flying_fox,0,',','.'); ?></td>
                                    <td class="text-center"><?= $x->org_mini_train ?></td>
                                    <td class="text-right"><?= number_format($x->uang_mini_train,0,',','.'); ?></td>
                                    <td class="text-center"><?= $x->org_sepeda_udara ?></td>
                                    <td class="text-right"><?= number_format($x->uang_sepeda_udara,0,',','.'); ?></td>
                                    <td class="text-right"><?= number_format($x->total,0,',','.'); ?></td>
                                </tr>
                                
                            <?php $no = $no + 1; ?>
                            <?php $ttl_org_ftiket_masuk = $ttl_org_tiket_masuk + $x->org_tiket_masuk; ?>
                            <?php $ttl_uang_tiket_masuk = $ttl_uang_tiket_masuk + $x->uang_tiket_masuk; ?>
                            <?php $ttl_org_flying_fox = $ttl_org_flying_fox + $x->org_flying_fox; ?>
                            <?php $ttl_uang_flying_fox = $ttl_uang_flying_fox + $x->uang_flying_fox; ?>
                            <?php $ttl_org_mini_train = $ttl_org_mini_train + $x->org_mini_train; ?>
                            <?php $ttl_uang_mini_train = $ttl_uang_mini_train + $x->uang_mini_train; ?>
                            <?php $ttl_org_sepeda_udara = $ttl_org_sepeda_udara + $x->org_sepeda_udara; ?>
                            <?php $ttl_uang_sepeda_udara = $ttl_uang_sepeda_udara + $x->uang_sepeda_udara; ?>
                            <?php $ttl_jml_kendaraan = $ttl_jml_kendaraan + $x->jml_kendaraan; ?>
                            <?php $ttl_uang_kendaraan = $ttl_uang_kendaraan + $x->uang_kendaraan; ?>
                            <?php $ttl = $ttl + $x->total; ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr class="text-bold">
                                <td colspan="2">Sub total</td>
                                <td class="text-center"><?= $ttl_jml_kendaraan ?></td>
                                <td class="text-right"><?= number_format($ttl_uang_kendaraan,0,',','.'); ?></td>
                                <td class="text-center"><?= $ttl_org_tiket_masuk ?></td>
                                <td class="text-right"><?= number_format($ttl_uang_tiket_masuk,0,',','.'); ?></td>
                                <td class="text-center"><?= $ttl_org_flying_fox ?></td>
                                <td class="text-right"><?= number_format($ttl_uang_flying_fox,0,',','.'); ?></td>
                                <td class="text-center"><?= $ttl_org_mini_train ?></td>
                                <td class="text-right"><?= number_format($ttl_uang_mini_train,0,',','.'); ?></td>
                                <td class="text-center"><?= $ttl_org_sepeda_udara ?></td>
                                <td class="text-right"><?= number_format($ttl_uang_sepeda_udara,0,',','.'); ?></td>
                                <td class="text-right"><?= number_format($ttl,0,',','.'); ?></td>
                                
                            </tr>
                        </tfoot>
                    </table>

                    <div class="col-md-5 total">
                        <b>
                            <div class="col-md-6">
                                <i class="fa fa-car"></i>
                                Jumlah Kendaraan    
                            </div>
                            <div class="col-md-6">
                                <?php if (count($total) > 0) { ?>
                                    : <?= $total[0]->jml_kendaraan ?> kendaraan.
                                <?php } else { ?>
                                    : 0 kendaraan.
                                <?php } ?>
                            </div>

                            <div class="col-md-6">
                                <i class="fa fa-user"></i>
                                Jumlah Pengunjung    
                            </div>
                            <div class="col-md-6">
                                <?php if (count($total) > 0) { ?>
                                    : <?= $total[0]->qty ?> orang.
                                <?php } else { ?>
                                    : 0 orang.
                                <?php } ?>
                            </div>

                            <div class="col-md-6">
                                <i class="fa fa-dollar"></i>
                                Jumlah Pendapatan    
                            </div>
                            <div class="col-md-6">
                                <?php if (count($total) > 0) { ?>
                                    : Rp. <?= number_format($total[0]->harga_total,0,',','.'); ?>
                                <?php } else { ?>
                                    : Rp. 0.
                                <?php } ?>
                            </div>
                        </b>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});

	$('#data_table').DataTable( {
	    dom: 'Bfrtip',
	    buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print'
	    ],
	    searching: false
	});

})

function filter()
{
	var date1 	= $('#date1').val();
	var date2 	= $('#date2').val();
	var url		= "<?= $this->url->get('laporan_umum_bandungan/index/') ?>" + date1 + "/" + date2;
	go_page(url);
}
</script>
