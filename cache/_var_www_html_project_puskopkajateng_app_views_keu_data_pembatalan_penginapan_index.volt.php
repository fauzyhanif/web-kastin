<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
  <h1>
    Data Pembatalan Transaksi Penginapan
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-edit"></i> Admin </a></li>
    <li class="active">Pembatalan Transaksi</li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-8 col-md-offset-2">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Data Transaksi Penginapan</h3>
          <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-filter"></i> Filter
          </button>

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" role="document">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Filter Transaksi</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                    </div> 
                  </div>

                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="box-body" style="overflow: auto;" id="listView">
          <table id="data_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" width="9%">Id Transaksi</th>
                <th class="text-center" width="10%">User</th>
                <th class="text-center" width="15%">Tgl Transaksi</th>
                <th class="text-center" width="8%">Jumlah</th>
                <th class="text-center" width="8%">Action</th>
              </tr>
            </thead>
            <tbody>

              <?php foreach ($penginapan as $x) { ?>
              <tr>
                <td><?= $x->a->id_transaksi ?></td>
                <td><?= $x->nm_user ?></td>
                <td><?= $x->a->tgl_transaksi ?></td>
                <td class="text-right"><?= $this->helper->formatRupiah($x->a->total) ?></td>
                <td class="text-center">

                  <button class="btn btn-default btn-flat btn-xs" title="Edit"  onclick="return go_page('keu_data_pembatalan/formEdit/<?= $id_unit ?>/8/<?= $x->a->id_transaksi ?>')">
                    <i class="fa fa-bars"></i> Detail
                  </button>
                  
                </td>
              </tr>
              <?php } ?>

            </tbody>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$(".select2").select2();
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_data_pembatalan/index/') ?>" + id_unit + "/6/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
</script>

<!-- include popup -->
