<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" name="" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
    <h1>
        Laporan Keuangan 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-text-o"></i> Admin</a></li>
        <li class="active">Laporan Keuangan</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Tanggal : <?= $this->helper->konversi_tgl($date1) ?> <?php if ($date2 != '') { ?> - <?= $this->helper->konversi_tgl($date2) ?><?php } ?></h4> 
                    <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-filter"></i> Filter</button>
                    <?php if (!empty($username)) { ?>
                    <h4>Nama : <?= $username ?></h4>
                    <?php } ?>

                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-sm" role="document"">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Filter Laporan</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                              </div> 
                            </div>

                            <div class="form-group col-md-12">
                              <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                              </div> 
                            </div>

                            <div class="form-group col-md-12">
                              <select class="form-control" id="id_user">
                                  <option value="all">** Pilih User</option>
                                  <?php foreach ($user as $x) { ?>
                                  <option value="<?= $x->id_user ?>"><?= $x->nama ?></option>
                                  <?php } ?>
                              </select> 
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="box-body" style="overflow: auto;" id="listView">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                        	<tr>
                        		<th class="text-center">Item</th>
                        		<th class="text-center" width="10%">Qty</th>
                        		<th class="text-center" width="20%">Jumlah</th>
                        	</tr>
                        </thead>
                        <div style="position: relative; overflow: auto; max-height: 100px">
                            <tbody>
                            	<!-- parkir -->
                            	<?php $parkirItem = 0; ?>
                            	<?php $parkirJml = 0; ?>
                            	<?php foreach ($parkir as $x) { ?>
                            	<?php $parkirItem = $parkirItem + $x->qty; ?>
                            	<?php $parkirJml = $parkirJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Parkir</b></td>
                            		<td><b><?= $parkirItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($parkirJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($parkir as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end parkir -->

                            	<!-- tiket masuk -->
                            	<?php $tiketMasukItem = 0; ?>
                            	<?php $tiketMasukJml = 0; ?>
                            	<?php foreach ($tiketMasuk as $x) { ?>
                            	<?php $tiketMasukItem = $tiketMasukItem + $x->qty; ?>
                            	<?php $tiketMasukJml = $tiketMasukJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Tiket masuk</b></td>
                            		<td><b><?= $tiketMasukItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($tiketMasukJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($tiketMasuk as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end tiket masuk -->

                            	<!-- wahana -->
                            	<?php $wahanaItem = 0; ?>
                            	<?php $wahanaJml = 0; ?>
                            	<?php foreach ($wahana as $x) { ?>
                            	<?php $wahanaItem = $wahanaItem + $x->qty; ?>
                            	<?php $wahanaJml = $wahanaJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Wahana</b></td>
                            		<td><b><?= $wahanaItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($wahanaJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($wahana as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end wahana -->

                            	<!-- office -->
                            	<?php $officeItem = 0; ?>
                            	<?php $officeJml = 0; ?>
                            	<?php foreach ($office as $x) { ?>
                            	<?php $officeItem = $officeItem + $x->qty; ?>
                            	<?php $officeJml = $officeJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Office</b></td>
                            		<td><b><?= $officeItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($officeJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($office as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end office -->

                            	<!-- souvenir -->
                            	<?php $souvenirItem = 0; ?>
                            	<?php $souvenirJml = 0; ?>
                            	<?php foreach ($souvenir as $x) { ?>
                            	<?php $souvenirItem = $souvenirItem + $x->qty; ?>
                            	<?php $souvenirJml = $souvenirJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>souvenir </b></td>
                            		<td><b><?= $souvenirItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($souvenirJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($souvenir as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end souvenir -->

                            	<!-- cafe -->
                            	<?php $cafeItem = 0; ?>
                            	<?php $cafeJml = 0; ?>
                            	<?php foreach ($cafe as $x) { ?>
                            	<?php $cafeItem = $cafeItem + $x->qty; ?>
                            	<?php $cafeJml = $cafeJml + $x->jumlah; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Cafe </b></td>
                            		<td><b><?= $cafeItem ?></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($cafeJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($cafe as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end cafe -->

                            	<!-- paket -->
                                <?php $paketItem = 0; ?>
                                <?php $paketJml = 0; ?>
                                <?php foreach ($paket as $x) { ?>
                                <?php $paketItem = $paketItem + $x->qty; ?>
                                <?php $paketJml = $paketJml + $x->jumlah; ?>
                                <?php } ?>
                                <tr>
                                    <td><b>Paket </b></td>
                                    <td><b><?= $paketItem ?></b></td>
                                    <td class="text-right"><b><?= $this->helper->formatRupiah($paketJml) ?></b></td>
                                </tr>
                                <?php foreach ($paket as $x) { ?>
                                    <tr>
                                        <td><p class="tab-item"><?= $x->item ?></p></td>
                                        <td><?= $x->qty ?></td>
                                        <td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
                                    </tr>
                                <?php } ?>
                                <!-- end paket -->

                                <!-- Penginapan -->
                                <?php $penginapanItem = 0; ?>
                                <?php $penginapanJml = 0; ?>
                                <?php foreach ($penginapan as $x) { ?>
                                <?php $penginapanItem = $penginapanItem + $x->qty; ?>
                                <?php $penginapanJml = $penginapanJml + $x->jumlah; ?>
                                <?php } ?>
                                <tr>
                                    <td><b>Penginapan </b></td>
                                    <td><b><?= $penginapanItem ?></b></td>
                                    <td class="text-right"><b><?= $this->helper->formatRupiah($penginapanJml) ?></b></td>
                                </tr>
                                <?php foreach ($penginapan as $x) { ?>
                                    <tr>
                                        <td><p class="tab-item"><?= $x->item ?></p></td>
                                        <td><?= $x->qty ?></td>
                                        <td class="text-right"><?= $this->helper->formatRupiah($x->jumlah) ?></td>
                                    </tr>
                                <?php } ?>
                                <!-- end Penginapan -->

                                <!-- lain-lain -->
                            	<?php $dllItem = 0; ?>
                            	<?php $dllJml = 0; ?>
                            	<?php foreach ($dll as $x) { ?>
                            	<?php $dllItem = $dllItem + $x->qty; ?>
                            	<?php $dllJml = $dllJml + $x->harga_total; ?>
                            	<?php } ?>
                            	<tr>
                            		<td><b>Lain-lain </b></td>
                            		<td><b></b></td>
    	                        	<td class="text-right"><b><?= $this->helper->formatRupiah($dllJml) ?></b></td>
                            	</tr>
                            	<?php foreach ($dll as $x) { ?>
    	                        	<tr>
    	                        		<td><p class="tab-item"><?= $x->item ?></p></td>
    	                        		<td><?= $x->qty ?></td>
    	                        		<td class="text-right"><?= $this->helper->formatRupiah($x->harga_total) ?></td>
    	                        	</tr>
                            	<?php } ?>
                            	<!-- end lain-lain -->
                            </tbody>
                        </div>
                    </table>

                    <div class="col-md-12 total">
                        <b>
                            <div class="col-md-6">
                                <i class="fa fa-user"></i>
                                Jumlah Pengunjung    
                            </div>
                            <div class="col-md-6">
                               : <?= $tiketMasukItem ?> Pengunjung
                            </div>

                            <div class="col-md-6">
                                <i class="fa fa-dollar"></i>
                                Jumlah Pendapatan    
                            </div>
                            <div class="col-md-6">
                                <?php $jmlPendapatan = $paketJml + $cafeJml + $souvenirJml + $officeJml + $wahanaJml + $tiketMasukJml + $parkirJml + $dllJml; ?>
                                : Rp. <?= $this->helper->formatRupiah($jmlPendapatan) ?>,-
                            </div>
                        </b>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});

	$('#data_table').DataTable( {
		"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
	    dom: 'Bfrtip',
	    buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print'
	    ],
	    searching: false
	});

})

function filter()
{
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var id_user = $('#id_user').val();
	var url		= "<?= $this->url->get('keu_laporan_unit/index/') ?>" + id_unit + "/" + id_user + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
</script>
