<?php $urlPost = $this->url->get('sdm/editSdm'); ?>


<style>#waktu {
    font-weight: 400;
}

.weight-600 {
    font-weight: 600;
}

.width-10 {
    width: 10px;
}

.width-30 {
    width: 30px;
}

.margin-top-20 {
    margin-top: 20px;
}

.fa {
    padding-right: 3px;
}

.img-murid {
    height: 3em;
    float: left;
    margin-right: 10px;
    border-radius: 100%;
    cursor: pointer;
}

#imageModalSource {
    max-width: 75%;
    display: block;
    margin: 0 auto;
}

#imageModalDescription {
    font-style: italic;
}

.red {
    color: red;
}</style>

<style>.bootstrap-filestyle label {
    width: 9.3em;
    font-size: 1.1em;
}

.preview {
    width: 100%; 
    border-radius: 6px;
}</style>

<section class="content-header">
    <h1>
        <button type="button" class="btn bg-navy btn-flat" id="back"><i class="fa fa-arrow-circle-left"></i> &nbsp; Kembali</button>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>SDM</a></li>
        <li class="active">Ubah</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit SDM #<?= $data->id_sdm ?></h3>
                </div>

                <form method="post" id="form_input" action="<?= $urlPost ?>/<?= $data->id_sdm ?>" >
                    <div class="box-body" style="height:auto;">
                        <div class="row">
                            <!-- Profil SDM -->
                            <div class="col-lg-12">
                                <div class="callout callout-info custom">
                                    <h4><i class="fa fa-user"></i>&nbsp; Profil SDM</h4>
                                </div>

                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <div class="form-group">
                                                <label>Nama Lengkap <span class="red">*</span></label>
                                                <input name="nama" type="text" placeholder="Nama" class="form-control" value="<?= $data->nama ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" type="text" placeholder="Email" class="form-control" value="<?= $data->e_mail ?>">
                                            </div>                                            
                                        </div>
                                        <!-- /.col-lg-12 -->

                                        <div class="col-lg-7">
                                            <div class="form-group">
                                                <label>NIP <span class="red">*</span></label>
                                                <input type="text" name="nip" placeholder="Nomor Induk Pegawai" class="form-control" maxlength="18" value="<?= $data->nip ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>NUPTK</label>
                                                <input type="text" name="nuptk" placeholder="NUPTK" class="form-control" maxlength="10" value="<?= $data->nuptk ?>">
                                            </div>
                                        </div>
                                        <!-- /.col-lg-12 -->

                                        <div class="col-lg-7">
                                            <div class="form-group">
                                                <label>NIK</label>
                                                <input type="text" name="nik" placeholder="Nomor Induk Kewarganegaraan" class="form-control" maxlength="16" value="<?= $data->nik ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>NIY NIGK</label>
                                                <input name="niy_nigk" type="text" placeholder="NIY NIGK" class="form-control" value="<?= $data->niy_nigk ?>">
                                            </div>
                                        </div>

                                        <div class="col-lg-7">
                                            <div class="form-group">
                                                <label>Tempat Lahir  <span class="red">*</span></label>
                                                <input name="tempat_lahir" type="text" placeholder="Tempat Lahir" class="form-control" value="<?= $data->tmp_lahir ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>Tanggal Lahir  <span class="red">*</span></label>
                                                <input name="tanggal_lahir" type="text" placeholder="Tanggal Lahir" class="form-control" value="<?= $this->helper->dateBahasaIndo($data->tgl_lahir) ?>">
                                                <input type="hidden" name="tanggal_kirim" value="<?= $data->tgl_lahir ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <div>
                                            <img src="img/sdm/<?= $data->foto ?>" alt="foto sdm" class="preview" id="uploadPreview" />
                                        </div>
                                    </div>
                                    <input type="file" name="foto" id="uploadImage" accept="image/*">
                                    <input type="hidden" name="foto_lama" value="<?= $data->foto ?>">
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Jenjang <span class="red">*</span></label>
                                        <select class="form-control" name="jenjang">
                                            <option value="0">-- Pilih Jenjang --</option>
                                            <option value="KB-TK" <?= (($data->jenjang == 'KB-TK') ? 'selected' : '') ?>>KB-TK</option>
                                            <option value="SD" <?= (($data->jenjang == 'SD') ? 'selected' : '') ?>>SD</option>
                                            <option value="SMP" <?= (($data->jenjang == 'SMP') ? 'selected' : '') ?>>SMP</option>
                                            <option value="SMA" <?= (($data->jenjang == 'SMA') ? 'selected' : '') ?>>SMA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Agama </label>
                                        <select class="form-control" name="agama">
                                            <option value="0">-- Pilih Agama --</option>
                                            <?php foreach ($agama as $a) { ?>
                                            <?php if (($a->agama_id == $data->kode_agama)) { ?>
                                            <option value="<?= $a->agama_id ?>" selected="selected"><?= $a->nama ?></option>
                                            <?php } else { ?>
                                            <option value="<?= $a->agama_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Jenis Kelamin <span class="red">*</span></label>
                                        <div class="jk">
                                            <input name="jenis_kelamin" type="radio" id="jkl" value="l" <?= (($data->kelamin == 'l') ? 'checked' : '') ?>>
                                            <label for="jkl" style="font-weight: normal;"> Laki-Laki</label>  &nbsp; &nbsp;
                                            <input name="jenis_kelamin" type="radio" id="jkp" value="p" <?= (($data->kelamin == 'p') ? 'checked' : '') ?>>
                                            <label for="jkp" style="font-weight: normal;"> Perempuan</label>  
                                        </div>
                                    </div>
                                </div>                                        
                                <!-- /.col-lg-12 -->                                
                            </div>
                            <!-- Alamat & Kontak -->
                            <div class="col-lg-12">
                                <div class="callout callout-info custom">
                                    <h4><i class="fa fa-map-marker"></i>&nbsp; Alamat &amp; Kontak</h4>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Alamat Jalan</label>
                                        <input name="alamat" type="text" placeholder="Alamat Jalan" class="form-control" value="<?= $data->alamat ?>">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>RT</label>
                                        <input name="rt" type="number" maxlength=3"" placeholder="RT" class="form-control" value="<?= $data->rt ?>">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>RW</label>
                                        <input name="rw" type="number" maxlength=3"" placeholder="RW" class="form-control" value="<?= $data->rw ?>">
                                    </div>
                                </div>                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Dusun</label>
                                        <input name="dusun" type="text" placeholder="Dusun" class="form-control" value="<?= $data->nama_dusun ?>">
                                    </div>
                                </div>                                                            
                                <!-- /.col-lg-12 -->                                 

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Provinsi</label>
                                        <select class="form-control" name="provinsi">
                                            <option value="0">-- Pilih provinsi --</option>
                                            <?php foreach ($provinsi_list as $a) { ?>
                                            <?php if (($a->kode_wilayah == $provinsi->id)) { ?>
                                            <option value="<?= $a->kode_wilayah ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>
                                            <option value="<?= $a->kode_wilayah ?>"><?= $a->nama ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Kabupaten / Kota</label>
                                        <select class="form-control" name="kabupaten" readonly>
                                            <option value="<?= $kabupaten->id ?>" selected><?= $kabupaten->nama ?></option>
                                        </select>
                                    </div>
                                </div>                            
                                <!-- /.col-lg-12 -->                                 

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Kecamatan</label>
                                        <select class="form-control" name="kecamatan" readonly>
                                            <option value="<?= $kecamatan->id ?>" selected><?= $kecamatan->nama ?></option>
                                        </select>
                                    </div>
                                </div>  
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Kelurahan</label>
                                        <select class="form-control" name="kelurahan" readonly>
                                            <option value="<?= $data->kode_wilayah ?>" selected><?= $data->desa_kelurahan ?></option>
                                        </select>
                                        <input type="hidden" name="kode_wilayah" value="<?= $data->kode_wilayah ?>">
                                        <input type="hidden" name="desa_kelurahan" value="<?= $data->desa_kelurahan ?>">
                                    </div>
                                </div>    
                                <!-- /.col-lg-12 -->                                 
                                                               
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Kode Pos</label>
                                        <input name="kode_pos" type="text" placeholder="Kode Pos" class="form-control" value="<?= $data->kodepos ?>">
                                    </div>
                                </div>                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>No Telepon Rumah</label>
                                        <input name="no_telepon_rumah" type="text" placeholder="No Telepon Rumah" class="form-control" value="<?= $data->telpon ?>">
                                    </div>
                                </div>  
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>No Handphone</label>
                                        <input name="no_hp" type="text" placeholder="No Handphone" class="form-control" value="<?= $data->no_hp ?>">
                                    </div>
                                </div>                                   
                                <!-- /.col-lg-12 -->                                
                            </div>
                            <!-- Informasi Lain -->
                            <div class="col-lg-12">
                                <div class="callout callout-info custom">
                                    <h4><i class="fa fa-info-circle"></i>&nbsp; Informasi Lain</h4>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Status Kepegawaian</label>
                                        <select class="form-control" name="status_kepegawaian_id">
                                            <option value="0">-- Pilih Status Pegawai --</option>
                                            <?php foreach ($status_kepegawaian as $a) { ?>
                                            <?php if (($a->status_kepegawaian_id == $data->status_kepegawaian_id)) { ?>
                                            <option value="<?= $a->status_kepegawaian_id ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>
                                            <option value="<?= $a->status_kepegawaian_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Jenis SDM</label>
                                        <select class="form-control" name="jenis_ptk_id">
                                            <option value="0">-- Pilih Jenis SDM --</option>
                                            <?php foreach ($jenis_ptk as $a) { ?>
                                            <?php if (($a->jenis_ptk_id == $data->jenis_ptk_id)) { ?>
                                            <option value="<?= $a->jenis_ptk_id ?>" selected><?= $a->jenis_ptk ?></option>
                                            <?php } else { ?>
                                            <option value="<?= $a->jenis_ptk_id ?>"><?= $a->jenis_ptk ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col-lg-12 -->

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Status Keaktifan</label>
                                        <select class="form-control" name="status_keaktifan_id">
                                            <option value="1" <?= (($data->status_keaktifan_id == '1') ? 'selected' : '') ?>>Aktif</option>
                                            <option value="2" <?= (($data->status_keaktifan_id == '2') ? 'selected' : '') ?>>Tidak AKitf</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Lembaga Pengangkat</label>
                                        <select class="form-control" name="lembaga_pengangkat_id">
                                            <option value="0">-- Pilih Lembaga Pengangkat --</option>
                                            <?php foreach ($lembaga_pengangkat as $a) { ?>
                                            <?php if (($a->lembaga_pengangkat_id == $data->lembaga_pengangkat_id)) { ?>
                                            <option value="<?= $a->lembaga_pengangkat_id ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>                                            
                                            <option value="<?= $a->lembaga_pengangkat_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Pangkat Golongan</label>
                                        <select class="form-control" name="pangkat_golongan_id">
                                            <option value="0">-- Pilih Pangkat Golongan --</option>
                                            <?php foreach ($pangkat_golongan as $a) { ?>
                                            <?php if (($a->pangkat_golongan_id == $data->pangkat_golongan_id)) { ?>
                                            <option value="<?= $a->pangkat_golongan_id ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>                                            
                                            <option value="<?= $a->pangkat_golongan_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>                                            
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>                  
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Keahlian Laboratorium</label>
                                        <select class="form-control" name="keahlian_laboratorium_id">
                                            <option value="0">-- Pilih Keahlian --</option>
                                            <?php foreach ($keahlian_laboratorium as $a) { ?>
                                            <?php if (($a->keahlian_laboratorium_id == $data->keahlian_laboratorium_id)) { ?>
                                            <option value="<?= $a->keahlian_laboratorium_id ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>                                            
                                            <option value="<?= $a->keahlian_laboratorium_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>                                             
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>                                
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Sumber Gaji</label>
                                        <select class="form-control" name="sumber_gaji_id">
                                            <option value="0">-- Pilih Sumber Gaji --</option>
                                            <?php foreach ($sumber_gaji as $a) { ?>
                                            <?php if (($a->sumber_gaji_id == $data->sumber_gaji_id)) { ?>
                                            <option value="<?= $a->sumber_gaji_id ?>" selected><?= $a->nama ?></option>
                                            <?php } else { ?>                                            
                                            <option value="<?= $a->sumber_gaji_id ?>"><?= $a->nama ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>                          
                            </div>

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <div class="col-md-6">
                            <label style="padding: 10px 0;">&nbsp; &nbsp; Tanda <span style="color:red">*</span> wajib diisi!</label>
                        </div>                      
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-lg btn-danger pull-right" id="submit"><i class="fa fa-paper-plane"></i> Simpan Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<!-- bootstrap filestyle -->
<script src="js/bootstrap-filestyle.min.js"></script>
<!-- global script -->
<script>/**
* Digital clock with Indonesian format
*/
window.setTimeout("clockIndonesia()", 1000);

function clockIndonesia() {
    var tanggal = new Date();
    $('#waktu').html(
        tanggal.getHours() + ':' + 
        tanggal.getMinutes() + ':' + 
        tanggal.getSeconds()
    );
    setTimeout("clockIndonesia()", 1000);
}

/**
* Set page title
* @param {String} title
*/
function setPageTitle(title) {    
    var $pageTitle  = $('title');
    var mainTitle   = $pageTitle.html();
    
    if (mainTitle.indexOf('|') != -1) {        
        $pageTitle.html(title + ' | ' + mainTitle.split('|')[1]);
    } else{
        $pageTitle.html(title + ' | ' + mainTitle);        
    }    
}

/**
* Datatable config with custom length
* @param {Number} length
*/
function dataTableConfig(length = 5) {
    var displayLength = 5,
        lengthMenu = [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ];
        
    if (length == 10) {
        displayLength = 10;
        lengthMenu = [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "Semua"]
        ];
    }

    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": lengthMenu,
        "iDisplayLength": displayLength,
        "language": {
            "url": "js/Indonesian.json"
        }
    });
}

/**
* Save form data with AJAX
* @param {Mix} formData
* @param {String} url
* @param {String} reload
* @param {String} except
*/
function saveData(formData, url, reload, except = '') {
    var request;

    if (typeof formData === 'object' && except === '') {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: new FormData(formData),
            contentType: false,
            cache: false,
            processData: false
        });
    } else {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: formData
        }); 
    }

    request.done(function(response){            
        new PNotify({
            title: response.title,
            text: response.text,
            type: response.type
        });

        reload_page2(reload);                            
    });    
}

/**
* Delete form data with AJAX when confirm
* @param {String} url
* @param {String} reload
*/
function deleteData(url, reload) {
    var notify = new PNotify({
        title: 'Pesan Konfirmasi',
        text: 'Apakah Anda Yakin menghapus data ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    }).get();
    
    notify.on('pnotify.confirm', function () {
        var request = $.ajax({type: "POST", dataType: "JSON", url: url});
        
        request.done(function() {
            new PNotify({
                title: 'Sukses',
                text: 'Data berhasil dihapus',
                type: 'success'
            });

            reload_page2(reload);
        });
    });
}

/**
* Selected select2 value
* @param {Object} element
* @param {String} id
* @param {String} text
*/
function selectedSelect2(element, id, text) {
    var newOption = '<option value="' + id + '"' + ' selected="selected">' 
                    + text + '</option>';    

    element.val('').trigger('change').append(newOption).trigger('change');
}

/**
* Multiple selected select2 by id list
* @param {Object} element
* @param {String} listId
*/
function multipleSelect2(element, listId) {
    var setElement = (listId).slice(1, -1).split(',');
    element.select2().val(setElement).trigger("change");
}                

/**
* Selected selectbox by text
* @param {Object} element
* @param {String} text
*/
function selectedByText(element, text) {
    element.val( element.find('option:contains("'+text+'")').attr('value') );
}

/**
* Checked radio button
* @param {String} radioValue
* @param {String} value
* @param {Object} elementOne
* @param {Object} elementTwo
*/
function checkedRadio(radioValue, value, elementOne, elementTwo) {
    if (radioValue == value) {    
        elementOne.prop('checked', true);
    } else {
        elementTwo.prop('checked', true);    
    }
}

/**
* Preview Image when upload
* @param {String} fileInput
* @param {String} previewImg
*/
function previewImage(fileInput, previewImg) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById(fileInput).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById(previewImg).src = oFREvent.target.result;
    };
}

/**
* Convert date to Indonesian format
* @param {String} date
* @return {String}
*/
function tanggalIndonesia(date){
    var bulanList = [
        "Januari", 
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];

    var tanggal = new Date(date),
        _hari   = tanggal.getDate(),
        _bulan  = tanggal.getMonth(),
        _tahun  = tanggal.getYear(),
        bulan   = bulanList[_bulan],
        tahun   = (_tahun < 1000) ? _tahun + 1900 : _tahun;
    
    return _hari + '-' + bulan + '-' + tahun;
}

/**
* Set refresh page in float button
* @param {String} page
*/
function setRefreshPage(page) {
    $('.float-button').attr('onclick', 'return reload_page2(\''+page+'\')');
}

/**
* Change indonesian location option
* @param {String} kode
* @param {String} level
* @param {String} url
*/
function changeWilayah(kode, level, url) {        
    if (level !== '') {        
        $.ajax({
            url       : url + '/' + kode,
            type      : "POST",
            dataType  : "json",
            data      : {'name' : 'value'},
            cache     : false,
            success   : function(data){
                var i,
                    makeOption = "",
                    dataLength = data.length;

                for (i = 0; i < dataLength; i++) {
                    makeOption += '<option value='+data[i]["kode_wilayah"]+'>'
                                +data[i]["nama"]+'</option>';
                }

                $("select[name="+level+"]")
                    .removeAttr('readonly')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">-- Pilih '+level+': --</option>' + makeOption);
            }
        });
    } else {
        var getText = $("select[name=kelurahan]").find("option:selected").text();
        $('input[name=desa_kelurahan]').val(getText);
    }

    $('input[name=kode_wilayah]').val(kode);
}</script>
<!-- custom script -->
<script>(function (document, window, $) {
    // -------------------------------------------------------------------------------
    // > VARIABLES
    // -------------------------------------------------------------------------------
    var pageUrl         = "<?= $this->url->get('sdm') ?>",
        wilayahUrl      = "<?= $this->url->get('sdm/searchWilayah') ?>",
        imgPath         = "<?= $this->url->get('img/sdm/') ?>",
        pageReload      = "sdm/index";

    // Form elements object
    var $form           = $("#form_input"),   
        $back           = $("#back"),   
        $submit         = $("#submit"),
        $reset          = $("#reset"),
        $imagePreview   = $("#uploadPreview");

    // Input elements object            
    var $tanggal        = $('input[name=tanggal_lahir]'),
        $tglKirim       = $('input[name=tanggal_kirim]'),
        $foto           = $('input[name=foto]'),
        $provinsi       = $('select[name=provinsi]'),
        $kabupaten      = $('select[name=kabupaten]'),
        $kecamatan      = $('select[name=kecamatan]'),
        $kelurahan      = $('select[name=kelurahan]');

    // -------------------------------------------------------------------------------
    // > EVENTS
    // -------------------------------------------------------------------------------  
    // Image preview when upload
    $foto.on("change", function() {
        previewImage("uploadImage", "uploadPreview");
    });

    // Save data when submit
    $form.on("submit", function(e) {
        var saveUrl = $(this).prop('action');            
        
        saveData(this, saveUrl, pageReload);
        e.preventDefault();            
    });        

    // replace image when error
    $imagePreview.on("error", function() {
        $(this).attr('src', 'img/user.png');
    });
        
    // Reset form
    $reset.on("click", function() {
        $imagePreview.attr('src', 'img/user.png');
    });        

    // Back to previous page
    $back.on("click", function() {      
        var url,
            urlBack = '<?= $url_back ?>',
            urlDefault = 'sdm/index';

        (urlBack == '') ? url = urlDefault : url = urlBack;

        go_page(url);          
        setRefreshPage(urlDefault);
    });        

    // add kabupaten option when provinsi change
    $provinsi.on("change", function() {
        changeWilayah(this.value, 'kabupaten', wilayahUrl);
    });

    // add kecamatan option when kabupaten change
    $kabupaten.on("change", function() {
        changeWilayah(this.value, 'kecamatan', wilayahUrl);
    });

    // add kelurahan option when kecamatan change
    $kecamatan.on("change", function() {
        changeWilayah(this.value, 'kelurahan', wilayahUrl);
    });

    // add selected kelurahan to hidden input
    $kelurahan.on("change", function() {
        changeWilayah(this.value, '', wilayahUrl);
    });

    // -------------------------------------------------------------------------------
    // > OTHERS
    // -------------------------------------------------------------------------------  
    setPageTitle("SDM");

    // Initialize datatable
    dataTableConfig();

    // Initialize filestyle
    $foto.filestyle({
        input: false,
        buttonText: "Upload",
        buttonName: "btn-danger",
        iconName: "fa fa-cloud-upload"
    });

    // Initialize datepicker
    $tanggal.datepicker({
        language: 'id',
        format: 'dd-MM-yyyy',
        autoclose: true,
        startDate: `-80y`,
        endDate: '+1y',
        todayBtn: true,
        todayHighlight: true,
        title: "Pilih Tanggal"
    }).on('changeDate', function (ev) {
        var selectedDate = ev.format(0, "yyyy-mm-dd");
        $tglKirim.val(selectedDate);
    });

}(document, window, jQuery));</script>