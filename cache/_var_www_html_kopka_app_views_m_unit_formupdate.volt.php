<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clear_form()">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Unit</h4>
      </div>

      <form name="unit" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group col-md-12">
            <label>Nama Unit</label>
            <input type="text" name="nama" class="form-control" placeholder=" Nama Unit">
          </div>

          <div class="form-group col-md-12">
              <label>Nomor Telpon</label>
              <input type="text" name="telp" class="form-control" placeholder=" Nomor Telpon" id="telp"> 
          </div>

          <div class="form-group col-md-12">
              <label>Hotline</label>
              <input type="text" name="hotline" class="form-control" placeholder=" Hotline" id="hotline"> 
          </div>

          <div class="form-group col-md-12">
              <label>Alamat</label>
              <textarea class="form-control" name="alamat" placeholder=" Alamat"></textarea>
          </div>
          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>