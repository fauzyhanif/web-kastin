<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Wahana</h4>
      </div>

      <form name="wahana" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group col-md-12">
              <label>Nama Wahana</label>
              <input type="text" name="nama" class="form-control" placeholder=" Nama Wahana" id="nama"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Individu</label>
              <input type="text" name="harga_individu" class="form-control tarif" placeholder=" Tarif Individu" id="harga_individu"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Lokal</label>
              <input type="text" name="harga_lokal" class="form-control tarif" placeholder=" Tarif Lokal" id="tarif_lokal"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tarif Rombongan</label>
              <input type="text" name="harga_rombongan" class="form-control tarif" placeholder=" Tarif Rombongan" id="harga_rombongan"> 
          </div>

          <div class="form-group col-md-12">
            <label>Aktif</label>
            <select class="form-control" name="aktif">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>