(function() {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('.datepicker').datepicker({
        language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
    });

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('keu_transaksi_dll/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'danger') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter()
{
    var id_unit = $('#id_unit').val();
    var date1   = $('#date1').val();
    var date2   = $('#date2').val();

    var url     = "<?= $this->url->get('keu_transaksi_dll/index/') ?>" + id_unit + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
    go_page(url);
}
