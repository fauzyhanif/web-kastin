
<?php $tanggalIndo = $this->helper->dateBahasaIndo(date('Y-m-d')); ?>


<style>#waktu {
    font-weight: 400;
}

.weight-600 {
    font-weight: 600;
}

.width-10 {
    width: 10px;
}

.width-30 {
    width: 30px;
}

.margin-top-20 {
    margin-top: 20px;
}

.fa {
    padding-right: 3px;
}

.img-murid {
    height: 3em;
    float: left;
    margin-right: 10px;
    border-radius: 100%;
    cursor: pointer;
}

#imageModalSource {
    max-width: 75%;
    display: block;
    margin: 0 auto;
}

#imageModalDescription {
    font-style: italic;
}

.red {
    color: red;
}</style>

<!-- Header content -->
<section class="content-header">
    <h1>
        Ubah Akun
        <small>
            <i class="fa fa-calendar-o"></i> <?= $tanggalIndo ?> 
            <i class="fa fa-clock-o"></i> <span id="waktu">00:00:00</span>
        </small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
        <li class="active">Ubah Akun</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Table column -->
        <div class="col-md-offset-3 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ubah Akun Pengguna <?= $this->session->get('id_user');  ?></h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" id="username" value="<?= $this->session->get('uid'); ?>">
                    </div>
                    <div class="form-group">
                        <label>Password Lama</label>
                        <input type="password" id="pass" class="form-control">
                    </div>                        
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input type="password" id="pass_baru" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Ulangi Password Baru</label>
                        <input type="password" onChange="checkPasswordMatch();" id="ulangi" class="form-control ul2">

                        <p class="notip" style="color:#dd4b39; display:none;">Password tidak sama!</p>
                    </div>  
                    <div class="form-group">           
                        <input class="btn btn-xs btn-success btn-flat" id="show" type="checkbox" style="margin: 0">  
                        <label for="show" style="font-weight: normal">Tampilkan Password</label>
                    </div>                                                 

                    <div class="pull-right">
                        <button onclick="ganti_login()" class="pull-right btn bg-navy btn-flat margin">Simpan</button>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- global script -->
<script>/**
* Digital clock with Indonesian format
*/
window.setTimeout("clockIndonesia()", 1000);

function clockIndonesia() {
    var tanggal = new Date();
    $('#waktu').html(
        tanggal.getHours() + ':' + 
        tanggal.getMinutes() + ':' + 
        tanggal.getSeconds()
    );
    setTimeout("clockIndonesia()", 1000);
}

/**
* Set page title
* @param {String} title
*/
function setPageTitle(title) {    
    var $pageTitle  = $('title');
    var mainTitle   = $pageTitle.html();
    
    if (mainTitle.indexOf('|') != -1) {        
        $pageTitle.html(title + ' | ' + mainTitle.split('|')[1]);
    } else{
        $pageTitle.html(title + ' | ' + mainTitle);        
    }    
}

/**
* Datatable config with custom length
* @param {Number} length
*/
function dataTableConfig(length = 5) {
    var displayLength = 5,
        lengthMenu = [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ];
        
    if (length == 10) {
        displayLength = 10;
        lengthMenu = [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "Semua"]
        ];
    }

    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": lengthMenu,
        "iDisplayLength": displayLength,
        "language": {
            "url": "js/Indonesian.json"
        }
    });
}

/**
* Save form data with AJAX
* @param {Mix} formData
* @param {String} url
* @param {String} reload
* @param {String} except
*/
function saveData(formData, url, reload, except = '') {
    var request;

    if (typeof formData === 'object' && except === '') {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: new FormData(formData),
            contentType: false,
            cache: false,
            processData: false
        });
    } else {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: formData
        }); 
    }

    request.done(function(response){            
        new PNotify({
            title: response.title,
            text: response.text,
            type: response.type
        });

        reload_page2(reload);                            
    });    
}

/**
* Delete form data with AJAX when confirm
* @param {String} url
* @param {String} reload
*/
function deleteData(url, reload) {
    var notify = new PNotify({
        title: 'Pesan Konfirmasi',
        text: 'Apakah Anda Yakin menghapus data ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    }).get();
    
    notify.on('pnotify.confirm', function () {
        var request = $.ajax({type: "POST", dataType: "JSON", url: url});
        
        request.done(function() {
            new PNotify({
                title: 'Sukses',
                text: 'Data berhasil dihapus',
                type: 'success'
            });

            reload_page2(reload);
        });
    });
}

/**
* Selected select2 value
* @param {Object} element
* @param {String} id
* @param {String} text
*/
function selectedSelect2(element, id, text) {
    var newOption = '<option value="' + id + '"' + ' selected="selected">' 
                    + text + '</option>';    

    element.val('').trigger('change').append(newOption).trigger('change');
}

/**
* Multiple selected select2 by id list
* @param {Object} element
* @param {String} listId
*/
function multipleSelect2(element, listId) {
    var setElement = (listId).slice(1, -1).split(',');
    element.select2().val(setElement).trigger("change");
}                

/**
* Selected selectbox by text
* @param {Object} element
* @param {String} text
*/
function selectedByText(element, text) {
    element.val( element.find('option:contains("'+text+'")').attr('value') );
}

/**
* Checked radio button
* @param {String} radioValue
* @param {String} value
* @param {Object} elementOne
* @param {Object} elementTwo
*/
function checkedRadio(radioValue, value, elementOne, elementTwo) {
    if (radioValue == value) {    
        elementOne.prop('checked', true);
    } else {
        elementTwo.prop('checked', true);    
    }
}

/**
* Preview Image when upload
* @param {String} fileInput
* @param {String} previewImg
*/
function previewImage(fileInput, previewImg) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById(fileInput).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById(previewImg).src = oFREvent.target.result;
    };
}

/**
* Convert date to Indonesian format
* @param {String} date
* @return {String}
*/
function tanggalIndonesia(date){
    var bulanList = [
        "Januari", 
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];

    var tanggal = new Date(date),
        _hari   = tanggal.getDate(),
        _bulan  = tanggal.getMonth(),
        _tahun  = tanggal.getYear(),
        bulan   = bulanList[_bulan],
        tahun   = (_tahun < 1000) ? _tahun + 1900 : _tahun;
    
    return _hari + '-' + bulan + '-' + tahun;
}

/**
* Set refresh page in float button
* @param {String} page
*/
function setRefreshPage(page) {
    $('.float-button').attr('onclick', 'return reload_page2(\''+page+'\')');
}

/**
* Change indonesian location option
* @param {String} kode
* @param {String} level
* @param {String} url
*/
function changeWilayah(kode, level, url) {        
    if (level !== '') {        
        $.ajax({
            url       : url + '/' + kode,
            type      : "POST",
            dataType  : "json",
            data      : {'name' : 'value'},
            cache     : false,
            success   : function(data){
                var i,
                    makeOption = "",
                    dataLength = data.length;

                for (i = 0; i < dataLength; i++) {
                    makeOption += '<option value='+data[i]["kode_wilayah"]+'>'
                                +data[i]["nama"]+'</option>';
                }

                $("select[name="+level+"]")
                    .removeAttr('readonly')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">-- Pilih '+level+': --</option>' + makeOption);
            }
        });
    } else {
        var getText = $("select[name=kelurahan]").find("option:selected").text();
        $('input[name=desa_kelurahan]').val(getText);
    }

    $('input[name=kode_wilayah]').val(kode);
}</script>

<script type="text/javascript">

function checkPasswordMatch() {
    var pass_baru = $("#pass_baru").val();
    var ulangi = $("#ulangi").val();

    if (pass_baru != ulangi){
        $(".ul").css("color", "#dd4b39");
        $(".ul2").css("border-color", "#dd4b39");
        $(".notip").css("display", "block");
        // $(".notip").html("Passwords tidak sama.");
    }else{
        $(".ul").css("color", "#444");
        $(".ul2").css("border-color", "#d2d6de");
        $(".notip").css("display", "none");
    }
}

$(document).ready(function () {
   $("#ulangi").keyup(checkPasswordMatch);
   $("#pass_baru").keyup(checkPasswordMatch);
});

  function ganti_login() {
    var pass_lama   = $("#pass").val();
    var username    = $("#username").val();
    var pass_baru   = $("#pass_baru").val();
    var ulangi      = $("#ulangi").val();

    var datas = "pass_lama="+pass_lama+"&username="+username+"&pass_baru="+pass_baru;

    var urel = '<?= $this->url->get('user/resetSdm/') ?>';

    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Apakah Anda Yakin meRESET Password?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function() {

        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: urel,
            data: datas,
            success: function(data){ 
              if (data.status == true) {
                  (new PNotify({
                      title: 'Confirmation Needed',
                      text: 'Silahkan login kembali dengan Akun baru!',
                      icon: 'glyphicon glyphicon-question-sign',
                      hide: false,
                      confirm: {
                          confirm: true
                      },
                      buttons: {
                          closer: false,
                          sticker: false
                      },
                      history: {
                          history: false
                      },
                      addclass: 'stack-modal',
                      stack: {
                          'dir1': 'down',
                          'dir2': 'right',
                          'modal': true
                      }
                  })).get().on('pnotify.confirm', function() {
                      var login = "<?= $this->url->get('account/logout') ?>";
                      window.location.replace(login);
                  }).on('pnotify.cancel', function() {
                      var login = "<?= $this->url->get('account/logout') ?>";
                      window.location.replace(login);
                  });
              }else{
                new PNotify({
                  title: data.title,
                  text: data.text,
                  type: data.type
                });
              }
            }
        });
    }).on('pnotify.cancel', function() {
       console.log('batal');
    });

  }

  // Show Password
  $(document).on('click','#show',function() {
    $('#pass_baru').removeAttr('type').attr({type:'text'});
    $('#ulangi').removeAttr('type').attr({type:'text'});
    $('#show').removeAttr('id').attr({id:'hide'});

  });

  $(document).on('click','#hide',function() {
    $('#pass_baru').removeAttr('type').attr({type:'password'});
    $('#ulangi').removeAttr('type').attr({type:'password'});
    $('#hide').removeAttr('id').attr({id:'show'});
  });

  $(document).on('click','#profil_img',function() {
    $('#up_img').css({display:'block'});
  });

</script>
