$(function () {
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2('M_jenis_kendaraan/index');
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id, nama_kendaraan,kode_kendaraan) {
  var form = $('form[name="jenis_kendaraan"]').attr('action', '<?= $this->url->get('M_jenis_kendaraan/update/') ?>' + id);
  form.find('[name="kode_kendaraan"]').val(kode_kendaraan);
  form.find('[name="nama_kendaraan"]').val(nama_kendaraan);
}

function delete_data(id, nama_kendaraan) {
    $('input#id').val(id);
    $('#nama').text(nama_kendaraan);
}

