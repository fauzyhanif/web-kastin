<!-- Header content -->
<section class="content-header">
    <h1>
        Backup Database
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-database"></i> Admin</a></li>
        <li class="active">Backup Database</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-md-4 col-md-offset-4">
    		<button class="btn btn-success btn-flat btn-block" onclick="backup();" style="margin-top: 45px;">
    			<i class="fa fa-database"></i> Backup Database
    		</button>
    	</div>
    </div>
</section>

<!-- include js file -->
<script>function backup() {
	var url = "<?= $this->url->get('Backup_database/do') ?>";
	// $url = 'Backup_database/do';
	$.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function(data){

            new PNotify({
                title: data.title,
                text: data.text,
                type: data.type
            });
        }
    });
}</script>