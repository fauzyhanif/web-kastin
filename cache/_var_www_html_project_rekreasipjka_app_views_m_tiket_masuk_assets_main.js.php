$(function () {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});


// SAVE / UPDATE / DELETE
(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('m_tiket_masuk/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function edit_data(id,harga_weekday,harga_weekend,id_jns_pengunjung,aktif) {
  var form = $('form[name="tiket_masuk"]').attr('action', '<?= $this->url->get('m_tiket_masuk/update/') ?>' + id);
  form.find('select[name="id_jns_pengunjung"]').val(id_jns_pengunjung);
  form.find('[name="harga_weekday"]').val(harga_weekday);
  form.find('[name="harga_weekend"]').val(harga_weekend);
  form.find('select[name="aktif"]').val(aktif);
}

function delete_data(id, nama) {
    $('input#id').val(id);
    $('#nama').text(nama);
}


