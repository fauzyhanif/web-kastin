function backup() {
	var url = "<?= $this->url->get('Backup_database/do') ?>";
	// $url = 'Backup_database/do';
	$.ajax({
        type: 'GET',
        url: url,
        dataType:'json',
        success: function(data){

            new PNotify({
                title: data.title,
                text: data.text,
                type: data.type
            });
        }
    });
}