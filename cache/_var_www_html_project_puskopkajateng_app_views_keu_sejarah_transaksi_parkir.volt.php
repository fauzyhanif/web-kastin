<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<!-- Header content -->
<section class="content-header">
  <h1>
    Sejarah Transaksi Parkir
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-edit"></i> Admin</a></li>
    <li class="active">Sejarah Transaksi</li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Data Transaksi Parkir</h3>
          <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-filter"></i> Filter
          </button>

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" role="document"">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Filter Transaksi</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                    </div> 
                  </div>

                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="box-body" style="overflow: auto;" id="listView">
          <table id="data_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" width="9%">Id Transaksi</th>
                <th class="text-center" width="10%">User</th>
                <th class="text-center" width="15%">Tgl Transaksi</th>
                <th class="text-center" width="30%">Item</th>
                <th class="text-center" width="5%">Qty</th>
                <th class="text-center" width="8%">Jumlah</th>
                <th class="text-center" width="9%">Action</th>
              </tr>
            </thead>
            <tbody>

              <!-- parkir -->
              <?php foreach ($parkir as $x) { ?>
              <tr>
                <td><?= $x->a->id_transaksi ?></td>
                <td><?= $x->nm_user ?></td>
                <td><?= $this->helper->konversi_tgl($x->a->tgl_transaksi) ?></td>
                <td><?= $x->item ?></td>
                <td><?= $x->a->qty ?></td>
                <td class="text-right"><?= $this->helper->formatRupiah($x->a->harga_total) ?></td>
                <td class="text-center">
                  <button class="btn btn-default btn-flat btn-xs" title="detail" data-toggle="modal" data-target="#detailParkir">
                    <i class="fa fa-bars"></i>
                  </button>
                  <button class="btn btn-primary btn-flat btn-xs" title="edit">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger btn-flat btn-xs" title="batalkan transaksi">
                    <i class="fa fa-trash"></i>
                  </button>
                </td>
              </tr>
              <?php } ?>
              <!-- parkir -->

            </tbody>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- include popup -->
<!-- Modal -->
<div class="modal fade" id="detailParkir" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Transaksi Parkir</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-sort-numeric-asc"></i>
              Id Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: 321123123</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calendar-check-o"></i>
              Tgl Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Selasa, 20 April 2018</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-bus"></i>
              Jenis Kendaraan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Mobil</div>

          <div class="col-md-4">
            <b>
              <i class="fa fa-tags"></i>
              No. Polisi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: T4551WJ</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Harga Satuan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calculator"></i>
              Qty
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Total
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;">: Rp. 10.000</div>

        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
      </div>
    </div>
    
  </div>
</div>