$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});

	$('#data_table').DataTable( {
		"paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
	    dom: 'Bfrtip',
	    buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print'
	    ],
	    searching: false
	});

})

function filter()
{
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var id_user = $('#id_user').val();
	var url		= "<?= $this->url->get('keu_laporan_unit/index/') ?>" + id_unit + "/" + id_user + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
