
<?php $tanggalIndo = $this->helper->dateBahasaIndo(date('Y-m-d')); ?>
<?php $urlPost = $this->url->get('sdm/addSdm'); ?>


<style>#waktu {
    font-weight: 400;
}

.weight-600 {
    font-weight: 600;
}

.width-10 {
    width: 10px;
}

.width-30 {
    width: 30px;
}

.margin-top-20 {
    margin-top: 20px;
}

.fa {
    padding-right: 3px;
}

.img-murid {
    height: 3em;
    float: left;
    margin-right: 10px;
    border-radius: 100%;
    cursor: pointer;
}

#imageModalSource {
    max-width: 75%;
    display: block;
    margin: 0 auto;
}

#imageModalDescription {
    font-style: italic;
}

.red {
    color: red;
}</style>

<!-- Header content -->
<section class="content-header">
    <h1>
        SDM
        <small>
            <i class="fa fa-calendar-o"></i> <?= $tanggalIndo ?> 
            <i class="fa fa-clock-o"></i> <span id="waktu">00:00:00</span>
        </small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SDM</a></li>
        <li class="active">Index</li>
    </ol>   
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Table column -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data SDM</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <a href="#" onclick="go_page('sdm/formSdm')" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp; Tambah Data</a>
                    </div>

                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="width-10">No</th>
                                <th>SDM</th>
                                <th>NUPTK</th>
                                <th>Jenjang</th>
                                <th>Jenis Kelamin</th>
                                <th>Aktif</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?> 
                        <?php foreach ($data as $row) { ?>                                                     
                            <?php $id = $row->id_sdm; ?> 
                            <?php $nip = $row->nip; ?> 
                            <?php $nama = $row->nama; ?> 
                            <?php $nuptk = $row->nuptk; ?> 
                            <?php $jenjang = $row->jenjang; ?> 
                            <?php $foto = $row->foto; ?>
                            
                            <?php if (($row->status_keaktifan_id == 1)) { ?>
                                <?php $status = 'Y'; ?>
                                <?php $color = 'green'; ?>
                            <?php } else { ?>
                                <?php $status = 'T'; ?>
                                <?php $color = 'red'; ?>
                            <?php } ?>

                            <?php if (($row->kelamin == 'l')) { ?>
                                <?php $kelamin = 'Laki-laki'; ?>
                            <?php } elseif (($row->kelamin == 'p')) { ?>
                                <?php $kelamin = 'Perempuan'; ?>
                            <?php } ?>                            

                            <tr id="data_<?= $id ?>" class="middle-row">
                                <td><?= $no ?></td>
                                <td>
                                    <img src="img/sdm/<?= $foto ?>" alt="<?= $nama ?>" title="perbesar foto" class="img-murid" data-toggle="tooltip">
                                    <span class="nama weight-600"><?= $nama ?></span> <br/> 
                                    <span class="label label-default">NIP</span> 
                                    <span class="label label-primary"><?= $nip ?></span>
                                </td>
                                <td><?= $nuptk ?></td>
                                <td><?= $jenjang ?></td>
                                <td><?= $kelamin ?></td>
                                <td><span class="badge bg-<?= $color ?>"><?= $status ?></span></td>
                                <td>
                                    <a data-id="<?= $id ?>" class="btn btn-primary btn-xs btn-flat edit"><i class="glyphicon glyphicon-edit"></i> </a>

                                    <a data-id="<?= $id ?>" class="btn btn-danger btn-xs btn-flat delete"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        <?php $no += 1; ?> 
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- Image Modal -->
        <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="imageModalLabel"></h4>
            </div>
            <div class="modal-body text-center">
                <img src="img/user.png" id="imageModalSource" />
                <h4 id="imageModalDescription"></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
    </div>

</section>
<!-- /.content -->

<!-- global script -->
<script>/**
* Digital clock with Indonesian format
*/
window.setTimeout("clockIndonesia()", 1000);

function clockIndonesia() {
    var tanggal = new Date();
    $('#waktu').html(
        tanggal.getHours() + ':' + 
        tanggal.getMinutes() + ':' + 
        tanggal.getSeconds()
    );
    setTimeout("clockIndonesia()", 1000);
}

/**
* Set page title
* @param {String} title
*/
function setPageTitle(title) {    
    var $pageTitle  = $('title');
    var mainTitle   = $pageTitle.html();
    
    if (mainTitle.indexOf('|') != -1) {        
        $pageTitle.html(title + ' | ' + mainTitle.split('|')[1]);
    } else{
        $pageTitle.html(title + ' | ' + mainTitle);        
    }    
}

/**
* Datatable config with custom length
* @param {Number} length
*/
function dataTableConfig(length = 5) {
    var displayLength = 5,
        lengthMenu = [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "Semua"]
        ];
        
    if (length == 10) {
        displayLength = 10;
        lengthMenu = [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "Semua"]
        ];
    }

    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "lengthMenu": lengthMenu,
        "iDisplayLength": displayLength,
        "language": {
            "url": "js/Indonesian.json"
        }
    });
}

/**
* Save form data with AJAX
* @param {Mix} formData
* @param {String} url
* @param {String} reload
* @param {String} except
*/
function saveData(formData, url, reload, except = '') {
    var request;

    if (typeof formData === 'object' && except === '') {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: new FormData(formData),
            contentType: false,
            cache: false,
            processData: false
        });
    } else {
        request = $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: formData
        }); 
    }

    request.done(function(response){            
        new PNotify({
            title: response.title,
            text: response.text,
            type: response.type
        });

        reload_page2(reload);                            
    });    
}

/**
* Delete form data with AJAX when confirm
* @param {String} url
* @param {String} reload
*/
function deleteData(url, reload) {
    var notify = new PNotify({
        title: 'Pesan Konfirmasi',
        text: 'Apakah Anda Yakin menghapus data ini?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    }).get();
    
    notify.on('pnotify.confirm', function () {
        var request = $.ajax({type: "POST", dataType: "JSON", url: url});
        
        request.done(function() {
            new PNotify({
                title: 'Sukses',
                text: 'Data berhasil dihapus',
                type: 'success'
            });

            reload_page2(reload);
        });
    });
}

/**
* Selected select2 value
* @param {Object} element
* @param {String} id
* @param {String} text
*/
function selectedSelect2(element, id, text) {
    var newOption = '<option value="' + id + '"' + ' selected="selected">' 
                    + text + '</option>';    

    element.val('').trigger('change').append(newOption).trigger('change');
}

/**
* Multiple selected select2 by id list
* @param {Object} element
* @param {String} listId
*/
function multipleSelect2(element, listId) {
    var setElement = (listId).slice(1, -1).split(',');
    element.select2().val(setElement).trigger("change");
}                

/**
* Selected selectbox by text
* @param {Object} element
* @param {String} text
*/
function selectedByText(element, text) {
    element.val( element.find('option:contains("'+text+'")').attr('value') );
}

/**
* Checked radio button
* @param {String} radioValue
* @param {String} value
* @param {Object} elementOne
* @param {Object} elementTwo
*/
function checkedRadio(radioValue, value, elementOne, elementTwo) {
    if (radioValue == value) {    
        elementOne.prop('checked', true);
    } else {
        elementTwo.prop('checked', true);    
    }
}

/**
* Preview Image when upload
* @param {String} fileInput
* @param {String} previewImg
*/
function previewImage(fileInput, previewImg) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById(fileInput).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById(previewImg).src = oFREvent.target.result;
    };
}

/**
* Convert date to Indonesian format
* @param {String} date
* @return {String}
*/
function tanggalIndonesia(date){
    var bulanList = [
        "Januari", 
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];

    var tanggal = new Date(date),
        _hari   = tanggal.getDate(),
        _bulan  = tanggal.getMonth(),
        _tahun  = tanggal.getYear(),
        bulan   = bulanList[_bulan],
        tahun   = (_tahun < 1000) ? _tahun + 1900 : _tahun;
    
    return _hari + '-' + bulan + '-' + tahun;
}

/**
* Set refresh page in float button
* @param {String} page
*/
function setRefreshPage(page) {
    $('.float-button').attr('onclick', 'return reload_page2(\''+page+'\')');
}

/**
* Change indonesian location option
* @param {String} kode
* @param {String} level
* @param {String} url
*/
function changeWilayah(kode, level, url) {        
    if (level !== '') {        
        $.ajax({
            url       : url + '/' + kode,
            type      : "POST",
            dataType  : "json",
            data      : {'name' : 'value'},
            cache     : false,
            success   : function(data){
                var i,
                    makeOption = "",
                    dataLength = data.length;

                for (i = 0; i < dataLength; i++) {
                    makeOption += '<option value='+data[i]["kode_wilayah"]+'>'
                                +data[i]["nama"]+'</option>';
                }

                $("select[name="+level+"]")
                    .removeAttr('readonly')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">-- Pilih '+level+': --</option>' + makeOption);
            }
        });
    } else {
        var getText = $("select[name=kelurahan]").find("option:selected").text();
        $('input[name=desa_kelurahan]').val(getText);
    }

    $('input[name=kode_wilayah]').val(kode);
}</script>
<!-- custom script -->
<script>(function (document, window, $) {
    
    // Set page title
    setPageTitle("SDM");

    // Define variables
    var pageUrl         = "<?= $this->url->get('sdm') ?>",
        pageReload      = "sdm/index";

    // Form elements object
    var $form           = $("#form_input"),   
        $title          = $("#form_title"),   
        $delete         = $(".delete"),
        $edit           = $(".edit"),
        $submit         = $("#submit"),
        $reset          = $("#reset"),
        $foto           = $(".img-murid");

            
    // replace image when error
    $foto.on("error", function() {
        $(this).attr('src', 'img/user.png');
    });

    // ImageModal show when image click
    $foto.on("click", function() {
        var judul = $(this).attr('alt'),
            imgSource = $(this).attr('src');

        $('.modal-dialog').addClass('modal-sm');
        $('#imageModal').modal('show');            
        $('#imageModalLabel').html(judul);
        $('#imageModalSource').attr('src', imgSource)
    });

        
    // redirect to edit form
    $edit.on("click", function() {
        var editId = $(this).attr("data-id"),
            editUrl = pageUrl + '/formEditSdm/' + editId,
            data = 'url_back='+pageUrl;

        go_page_data(editUrl, data); 
    });

    // Delete data when confirmed
    $delete.on("click", function() {
        var deleteId = $(this).attr("data-id"),
            deleteUrl = pageUrl + '/deleteSdm/' + deleteId;

        deleteData(deleteUrl, pageReload);
    });

    // Intialize datatable
    dataTableConfig();

}(document, window, jQuery));</script>