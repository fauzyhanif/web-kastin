<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<!-- Header content -->
<section class="content-header">
    <h1>
        Update Transaksi 
    </h1>
    <ol class="breadcrumb">
        <li>
            <button class="btn bg-navy btn-flat" onclick="return go_page('keu_transaksi_dll/index/<?= $data->id_unit ?>')">
                <i class="fa  fa-chevron-circle-left"></i>
                Back
            </button>
        </li>
    </ol>   
</section>
<!-- Main content -->
<section class="content">
    <div class="row">

        <!-- column -->
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body" style="overflow: auto;" id="listView">
                    <form id="form_input" method="POST" action="<?= $this->url->get('keu_transaksi_dll/update') ?>" data-remote>
                    	<input type="hidden" name="id_transaksi" value="<?= $data->id_transaksi ?>" id="id_transaksi">
                    	<input type="hidden" name="id_unit" value="<?= $data->id_unit ?>" id="id_unit">
                    	<input type="hidden" name="user_update" value="<?= $this->session->get('id_user') ?>">
                        <div class="form-group col-md-12">
                            <label>Item/Keterangan</label>
                            <textarea class="form-control" name="item" required=""><?= $data->item ?></textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Qty</label>
                            <input type="text" name="qty" class="form-control tarif"  value="<?= $data->qty ?>"> 
                        </div>

                        <div class="form-group col-md-6">
                            <label>Satuan</label>
                            <input type="text" name="satuan" class="form-control"  value="<?= $data->satuan ?>"> 
                        </div>

                        <div class="form-group col-md-12">
                            <label>Harga Satuan</label>
                            <input type="text" name="harga_satuan" class="form-control tarif" value="<?= $data->harga_satuan ?>"> 
                        </div>

                        <div class="form-group col-md-12">
                            <label>Jumlah</label>
                            <input type="text" name="harga_total" class="form-control tarif" required="" value="<?= $data->harga_total ?>"> 
                        </div>
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="reset" class="btn btn-default btn-flat" id="reset" onclick="return reload_page2('keu_transaksi_dll/formAdd/<?= $data->id_unit ?>')">
                                    <i class="fa fa-refresh"></i>&nbsp; Reset
                                </button>
                                <button type="submit" class="btn btn-primary btn-flat" id="submit">
                                    <i class="fa fa-send"></i>&nbsp; Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- include js file -->
<script>(function() {
    $(".tarif").mask("000.000.000", {reverse:true});
    $('.datepicker').datepicker({
        language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
    });

    $('form[data-remote]').on('submit', function(e) {
        var form    = $(this);
        var url     = form.prop('action');
        var id_unit = $('#id_unit').val();
        var url_reload = "<?= $this->url->get('keu_transaksi_dll/index/') ?>"+id_unit;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'danger') {
                    reload_page2(url_reload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter()
{
    var id_unit = $('#id_unit').val();
    var date1   = $('#date1').val();
    var date2   = $('#date2').val();

    var url     = "<?= $this->url->get('keu_transaksi_dll/index/') ?>" + id_unit + "/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
    go_page(url);
}
</script>
