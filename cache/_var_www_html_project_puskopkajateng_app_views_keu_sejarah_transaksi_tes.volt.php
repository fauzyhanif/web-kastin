<div class="modal fade" id="editTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Transaksi</h4>
      </div>

      <form name="editTransaksi" id="parkir" method="POST" data-remote="data-remote">
        <div class="modal-body">
          <div class="form-group col-md-12">
            <label>id Transaksi</label>
            <input type="text"  class="form-control" disabled="" id="idTransaksi">
          </div>

          <div class="form-group col-md-12">
              <label>User</label>
              <input type="text"  class="form-control" disabled="" id="nmUser"> 
          </div>

          <div class="form-group col-md-12">
              <label>Tgl Transaksi</label>
              <input type="text" class="form-control" disabled="" id="tglTransaksi"> 
          </div>

          <div class="form-group col-md-12">
            <label>Item</label>
            <select class="form-control" name="id_item">
              <option value="Y">Ya</option>
              <option value="N">Tidak</option>
            </select>
          </div>

          <div class="form-group col-md-12">
            <label>Quantity</label>
            <input type="text" name="qty" class="form-control" id="qty" value="">
          </div>

          <div class="form-group col-md-12">
            <label>Harga Satuan</label>
            <input type="text" name="harga_satuan" class="form-control">
          </div>

          <div class="form-group col-md-12">
            <label>Total Harga</label>
            <input type="text" name="harga_total" class="form-control">
          </div>
        </div>
        <div class="modal-footer" style="border-top: none;">
          <a class="btn btn-danger btn-flat" data-dismiss="modal">
            <i class="fa fa-remove"></i> Cancel
          </a>
          <button type="submit" class="btn btn-primary btn-flat">
          <i class="fa fa-send"></i> Save Change
          </button>
        </div>
      </form>

    </div>
  </div>
</div>