<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
  <h1>
    Edit Transaksi Souvenir
  </h1>
  <ol class="breadcrumb">
    <li>
      <button class="btn bg-navy btn-flat" onclick="return go_page('keu_sejarah_transaksi/index/<?= $id_unit ?>/6')" style="margin-top: -11px;">
        <i class="fa  fa-chevron-circle-left"></i>
        Back
      </button>
    </li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header  with-border">
          <div class="col-md-12">
            <div class="col-md-3" style="font-size: 22px">Nomor Struk</div>
            <div class="col-md-4" style="font-size: 22px">: <?= $data[0]->a->id_transaksi ?>.</div>
            <div class="col-md-2" style="font-size: 22px">Last update</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->a->updated_at ?>.</div>

            <div class="col-md-3" style="font-size: 22px">Kasir</div>
            <div class="col-md-4" style="font-size: 22px">: <?= $data[0]->nm_user ?>.</div>
            <div class="col-md-2" style="font-size: 22px">Oleh</div>
            <div class="col-md-3" style="font-size: 22px">: <?= $data[0]->user_update ?>.</div>
            
            <div class="col-md-3" style="font-size: 22px">Tgl Transaksi</div>
            <div class="col-md-9" style="font-size: 22px">: <?= $data[0]->a->tgl_transaksi ?>.</div>
          </div>
        </div>

        <div class="box-body" style="overflow: auto;" id="listView">
          <form name="editTransaksi" id="souvenir" method="POST" action="<?= $this->url->get('keu_sejarah_transaksi/editTransaksi') ?>">
            <input type="hidden" name="id_unit" value="<?= $id_unit ?>">
            <input type="hidden" name="id_transaksi" value="<?= $data[0]->a->id_transaksi ?>">
            <input type="hidden" name="user_update" value="<?= $this->session->get('id_user') ?>">
            <input type="hidden" name="item" value="6">
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" width="5%">No</th>
                  <th class="text-center" width="30%">Item</th>
                  <th class="text-center" width="5%">Qty</th>
                  <th class="text-center" width="10%">Harga Satuan</th>
                  <th class="text-center" width="10%">Jumlah</th>
                </tr>
              </thead>
              <tbody>
                <?php $total = 0; ?>
                <?php $no = 1; ?>
                <?php foreach ($dataRinci as $x) { ?>

                <tr class="content"> 
                  <input type="hidden" name="id_item_lama[]" value="<?= $x->a->id_item ?>">
                  <td class="text-center"><?= $no ?>.</td>
                  <td>
                    <select class="form-control" name="id_item[]">
                      <?php foreach ($souvenir as $v) { ?>
                        <option value="<?= $v->id ?>" <?php if ($x->a->id_item == $v->id) { ?> selected="" <?php } ?>><?= $v->nama ?></option>
                      <?php } ?>
                    </select>
                  </td>
                  <td>
                    <input type="number" name="qty[]" class="form-control qty" value="<?= $x->a->qty ?>" style="width: 60px;">
                  </td>
                  <td class="text-right"> 
                    <input type="number" name="harga_satuan[]" class="form-control harga_satuan" value="<?= $x->a->harga_satuan ?>" style="width: 100px;">
                  </td>
                  <td class="text-right"> 
                    <input type="number" name="harga_total[]" class="form-control harga_total" value="<?= $x->a->harga_total ?>" style="width: 100px;">
                  </td>
                </tr>

                <?php $total = $total + $x->a->harga_total; ?>
                <?php $no = $no + 1; ?>
                <?php } ?>

              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-center"><h4><b>Total</b></h4></td>
                  <td class="text-right"><h4><b id="total"><?= $total ?></b></h4></td>
                  <input type="hidden" name="total" id="hargaTotal" value="<?= $total ?>">
                </tr>
              </tfoot>
            </table>

            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Save Change</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$(".select2").select2();
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        }
    });

});

// get harga dri db
$('select').on('change', function() {
 	var content = $(this).closest(".content");
	$.ajax({
        type: 'POST',
        url: '<?= $this->url->get('keu_sejarah_transaksi/hargaSouvenir/') ?>'+this.value,
        dataType:'json',
        data: "id="+this.value,
        success: function(data){
        	content.find(".harga_satuan").val(data);
        	qty 	= content.find(".qty").val();
        	hasil 	= qty * data;
        	content.find(".harga_total").val(hasil);
        	total();
        }
    });
});

$(function() {
    $('.harga_satuan').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.qty').keyup(function() {
 		content = $(this).closest(".content");
        harga   = content.find('.harga_satuan').val()
        qty     = content.find('.qty').val();
        hasil   = harga * qty;
        content.find('.harga_total').val(hasil);
        total();
    });

    $('.harga_total').keyup(function() {
        total();
    });
});

function total(){
    var sum = 0;
    $(".harga_total").each(function(){
        sum += +$(this).val();
    });
    $("#total").text(sum);
    $("#hargaTotal").val(sum);
}

(function() {

    $('form[name="editTransaksi"]').on('submit', function(e) {
        var form        = $(this);
        var url         = form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var idTransaksi = $('input[name="id_transaksi"]').val();
        var urlReload   = '<?= $this->url->get('keu_sejarah_transaksi/formEdit/') ?>'+idUnit+"/6/"+idTransaksi;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });

    $('form[name="batalTransaksi"]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit      = $('input[name="id_unit"]').val();
        var urlReload 	= '<?= $this->url->get('keu_sejarah_transaksi/index/') ?>'+idUnit+"/6";

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_sejarah_transaksi/index/') ?>" + id_unit + "/6/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}
</script>

<!-- include popup -->
