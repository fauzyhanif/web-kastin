<style>.input-group {
    width: 250px;
    float: left;
    margin-right: 20px;
}

.btn-filter {
    float: left;
}

.form-filter {
	/*margin-left: 220px;*/
}

.tab-item{
	margin-left: 15px;
}

.total {
	float: right;
	background-color: #67809F;
	color: #fff;
	padding: 10px;
	font-size: 15px;
}

#data_table {
	margin-bottom: 20px;
}

thead, tfoot {
	background-color: #BDC3C7;
}

tbody {
	overflow: auto;
}</style>
<input type="hidden" id="id_unit" value="<?= $id_unit ?>">
<!-- Header content -->
<section class="content-header">
  <h1>
    Sejarah Transaksi Parkir
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-edit"></i> Admin</a></li>
    <li class="active">Sejarah Transaksi</li>
  </ol>   
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" id="form_title">Data Transaksi Parkir</h3>
          <button class="btn btn-success box-tools pull-right btn-flat" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-filter"></i> Filter
          </button>

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm" role="document"">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Filter Transaksi</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date1" class="form-control pull-right datepicker" id="date1">
                    </div> 
                  </div>

                  <div class="form-group col-md-12">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="date2" class="form-control pull-right datepicker" id="date2">
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button onclick="filter()" class="btn btn-success btn-flat btn-filter pull-right"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="box-body" style="overflow: auto;" id="listView">
          <table id="data_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th class="text-center" width="10%">Id Transaksi</th>
                <th class="text-center" width="10%">User</th>
                <th class="text-center" width="15%">Tgl Transaksi</th>
                <th class="text-center" width="20%">Item</th>
                <th class="text-center" width="5%">Qty</th>
                <th class="text-center" width="8%">Jumlah</th>
                <th class="text-center" width="9%">Action</th>
              </tr>
            </thead>
            <tbody>

              <?php foreach ($parkir as $x) { ?>
              <tr>
                <td><?= $x->a->id_transaksi ?></td>
                <td><?= $x->nm_user ?></td>
                <td><?= $x->a->tgl_transaksi ?></td>
                <!-- <td><?= $this->helper->konversi_tgl($x->a->tgl_transaksi) ?></td> -->
                <td><?= $x->item ?></td>
                <td><?= $x->a->qty ?></td>
                <td class="text-right"><?= $this->helper->formatRupiah($x->a->harga_total) ?></td>
                <td class="text-center">
                  <button class="btn btn-default btn-flat btn-xs" title="Detail" data-toggle="modal" data-target="#detailTransaksi" onclick="detailTransaksi('<?= $id_unit ?>','<?= $x->a->id_transaksi ?>','<?= $x->nm_user ?>','<?= $x->a->tgl_transaksi ?>','<?= $x->item ?>','<?= $x->a->no_polisi ?>','<?= $this->helper->formatRupiah($x->a->harga_satuan) ?>','<?= $x->a->qty ?>','<?= $this->helper->formatRupiah($x->a->harga_total) ?>')">
                    <i class="fa fa-bars"></i>
                  </button>

                  <button class="btn btn-primary btn-flat btn-xs" title="Edit"  onclick="return go_page('keu_sejarah_transaksi/formEdit/<?= $id_unit ?>/2/<?= $x->a->id_transaksi ?>')">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger btn-flat btn-xs" title="Batalkan Transaksi" data-toggle="modal" data-target="#batalTransaksi<?= $x->a->id_transaksi ?>" >
                    <i class="fa fa-trash"></i>
                  </button>

                  <div class="modal fade batalTransaksi" id="batalTransaksi<?= $x->a->id_transaksi ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">Pembatalan Transaksi</h4>
                        </div>

                        <form name="batalTransaksi" method="POST" action="<?= $this->url->get('keu_sejarah_transaksi/batalTransaksi') ?>" data-remote>
                          <input type="hidden" name="id_unit" value="<?= $id_unit ?>">
                          <input type="hidden" name="id_transaksi" value="<?= $x->a->id_transaksi ?>">
                          <input type="hidden" name="item" value="2">
                          <input type="hidden" name="id_user" value="<?= $this->session->get('id_user') ?>">
                          <div class="modal-body">
                            <div class="form-group">
                              <label>Anda yakin ingin membatalkan transaksi dengan id transaksi <span id="id-transaksi"><?= $x->a->id_transaksi ?></span>?</label>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-danger btn-flat" data-dismiss="modal">
                              <i class="fa fa-remove"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary btn-flat">
                            <i class="fa fa-trash"></i> Batalkan
                            </button>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <?php } ?>

            </tbody>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(function () {
	$('.datepicker').datepicker({
	  	language: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: `-1y`,
        endDate: '0d',
        todayBtn: true,
        todayHighlight: true,
        title: "Filter Tanggal",
        btnClose: true
	});
    $(".tarif").mask("000.000.000", {reverse:true});
    $('#data_table').DataTable( {
        "paging": false,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "lengthMenu": [
            [30, 40, 50, -1],
            [30, 40, 50, "Semua"]
        ],
        "iDisplayLength": 30,
        "language": {
            "url": "js/Indonesian.json"
        },
        dom: 'Bfrtip',
        buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        searching: true
    });

});

$(function() {
    $('#harga_satuan').keyup(function() {
        harga = $(this).val().replace(/\./g, '');
        qty   = $('#qty').val();
        hasil = harga * qty;
        $('#harga_total').val(hasil);
    });
    
    $('#qty').keyup(function() {
        qty     = $(this).val();
        harga   = $('#harga_satuan').val().replace(/\./g, '');
        hasil   = qty * harga;
        $('#harga_total').val(hasil);
    });
    
});

(function() {

    $('form[data-remote]').on('submit', function(e) {
        var form    	= $(this);
        var url     	= form.prop('action');
        var idUnit  	= $('input[name="id_unit"]').val();
        var urlReload 	= '<?= $this->url->get('keu_sejarah_transaksi/index/') ?>'+idUnit+'/2';

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            complete: function () {
                $('#update').modal('hide');
                $('.batalTransaksi').modal('hide');
                $('body').removeClass('modal-open');
                $("body").css("padding-right", "0px");
                $('.modal-backdrop').remove();
            },
            success: function(data){
                if (data.type != 'warning') {
                    reload_page2(urlReload);
                }
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: data.type
                });
            }
        });

        e.preventDefault();
    });
})();

function filter(){
	var id_unit = $('#id_unit').val();
	var date1 	= $('#date1').val();
    var date2   = $('#date2').val();
	var url		= "<?= $this->url->get('keu_sejarah_transaksi/index/') ?>" + id_unit + "/2/" + date1 + "/" + date2;
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $("body").css("padding-right", "0px");
    $('.modal-backdrop').remove();
	go_page(url);
}

function detailTransaksi(idUnit,idTransaksi,nmUser,tglTransaksi,item,noPolisi,hargaSatuan,qty,hargaTotal) {
	$('#idTransaksi').html(": "+idTransaksi);	
	$('#nmUser').html(": "+nmUser);
	$('#tglTransaksi').html(": "+tglTransaksi);
	$('#item').html(": "+item);
	$('#noPolisi').html(": "+noPolisi);
	$('#hargaSatuan').html(": Rp."+hargaSatuan);
	$('#qty').html(": "+qty);
	$('#hargaTotal').html(": Rp."+hargaTotal);
}


function editTransaksix(idUnit,idTransaksi,nmUser,tglTransaksi,item,noPolisi,hargaSatuan,qty,hargaTotal) {
	$('form[id="parkir"]').attr('action', '<?= $this->url->get('keu_sejarah_transaksi/editTransaksi/') ?>' + idTransaksi);
	$('input[name="id_unit"]').val(idUnit);
	$('input[id="idTransaksi"]').val(idTransaksi);	
	$('input[id="nmUser"]').val(nmUser);
	$('input[id="tglTransaksi"]').val(tglTransaksi);
	$('input[name="id_item"]').val(item);
	$('input[name="no_polisi"]').val(noPolisi);
	$('input[name="harga_satuan"]').val(hargaSatuan);
	$('input[name="qty"]').val(qty);
	$('input[name="harga_total"]').val(hargaTotal);
}

</script>

<!-- include popup -->
<!-- Modal -->
<div class="modal fade" id="detailTransaksi" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Transaksi Parkir</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-sort-numeric-asc"></i>
              Id Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="idTransaksi"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-user"></i>
              User
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="nmUser"></div>
          
          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calendar-check-o"></i>
              Tgl Transaksi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="tglTransaksi"></div>


          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-bus"></i>
              Jenis Kendaraan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="item"></div>

          <div class="col-md-4">
            <b>
              <i class="fa fa-tags"></i>
              No. Polisi
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="noPolisi"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Harga Satuan
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="hargaSatuan"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-calculator"></i>
              Qty
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="qty"></div>

          <div class="col-md-4" style="margin-bottom: 10px;">
            <b>
              <i class="fa fa-dollar"></i>
              Total
            </b>
          </div>
          <div class="col-md-8" style="margin-bottom: 10px;" id="hargaTotal"></div>

        </div>
      </div>
      <div class="modal-footer" style="border-top: none;">
      </div>
    </div>
    
  </div>
</div>
