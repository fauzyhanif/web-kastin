<div class="box-header">
    <h3 class="box-title" id="form_title">Edit User</h3>
</div>
<!-- /.box-header -->
<div class="box-body pad">
    <div class="row">
        <form id="form_input" method="POST" data-remote action="<?= $this->url->get('user/editUser') ?>">
        	<input type="hidden" name="id_user" value="<?= $data[0]['id'] ?>" id="id_user">
            <div class="form-group col-md-12 text-center">
                <img src="img/user.png" alt="Foto User" height="85px" class="img-circle text-center" id="foto">
            </div>

            <div class="form-group col-md-12">
                <label for="id_jenis">Unit</label>
                <select class="form-control" name="id_unit" id="id_unit" required="">
                    <option value="">Pilih:</option>
                    <?php foreach ($unit as $x) { ?>
                        <option value="<?= $x->id_unit ?>" <?php if ($x->id_unit == $data[0]['id_unit']) { ?> selected <?php } ?>><?= $x->nama ?></option>
                    <?php } ?>
                </select>
            </div>  

            <div class="form-group col-md-12">
                <label for="id_jenis">Akses</label>
                <select class="form-control" name="akses" id="akses" required="">
                    <option value="">** Pilih Akses</option>
                    <?php foreach ($akses as $x) { ?>
                        <option value="<?= $x->id ?>" <?php if ($x->id == $data[0]['akses']) { ?> selected <?php } ?>><?= $x->nama ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group col-md-12">
                <label for="nama">Nama Lengkap</label>
                <input type="text" class="form-control" name="nama" id="nama" value="<?= $data[0]['nama'] ?>" required="">
            </div>

            <div class="form-group col-md-12">
                <label for="nama">NIP</label>
                <input type="text" class="form-control" name="nip" id="nip" value="<?= $data[0]['nip'] ?>" required="">
            </div>  

            <div class="form-group col-md-5">
                <label for="uid">UID</label>
                <input type="text" class="form-control" name="uid" value="<?= $data[0]['uid'] ?>" id="uid" required="">
                <input type="hidden" name="nip" id="nip">
            </div>

            <div class="form-group col-md-7">
                <label for="password">Password</label>
                <div class="input-group">
                    <input type="password" class="form-control active" name="password" placeholder="Password" id="pass" required="">
                    <a class="input-group-addon" href="#" id="showPassword"><i class="fa fa-eye-slash"></i></a>
                </div>                                
            </div> 

            <div class="form-group col-md-12">
                <label for="area">Area Akses Menu</label>
                <select name="area" id="area" class="form-control select2" multiple="multiple" data-placeholder="Pilih area akses:" style="width: 100%;" required="">
                	<?php $r = array_filter(explode(',', $data[0]['area']));?>
                    <?php foreach ($area as $opt) { ?>
                    <?php if (in_array($opt->id, $r)): ?>
                    	<option value="<?= $opt->id ?>" selected><?= $opt->label_menu ?></option>
	              	<?php else: ?>
                    	<option value="<?= $opt->id ?>"><?= $opt->label_menu ?></option>
	              	<?php endif ?>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group col-md-12">
                <label for="usergroup">Usergroup</label>
                <select name="usergroup" id="usergroup" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup:" style="width: 100%;" required="">
                	<?php $r = array_filter(explode(',', $data[0]['usergroup']));?>
                    <?php foreach ($usergroup as $opt) { ?>
                    <?php if (in_array($opt->id, $r)): ?>
                    	<option value="<?= $opt->id ?>" selected><?= $opt->nama ?></option>
	              	<?php else: ?>
                    	<option value="<?= $opt->id ?>"><?= $opt->nama ?></option>
	              	<?php endif ?>
                    <?php } ?>
                </select>
            </div> 

            <div class="form-group col-md-12">
              <label>Upload Foto</label>
              <input type="file" class="filestyle" name="image" data-size="sm" id="uploadImage1" onchange="PreviewImage(1)">
            </div>

            <div class="form-group col-md-12">
                <center>
                    <img src="img/sdm/<?= $data[0]['foto'] ?>" width="230" id="uploadPreview1" class="img-responsive">
                </center>                                   
            </div>                               

            <div class="col-md-12">
                <div class="pull-right">
                    <button type="reset" class="btn btn-default" id="reset"><i class="fa fa-refresh"></i>&nbsp; Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp; Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    (function() {

      $('form[data-remote]').on('submit', function(e) {
        var form = $(this);
        var url = form.prop('action');
        var id_unit     = $("#id_unit").val();
        var url_reload  = "<?= $this->url->get('user/index/') ?>"+id_unit;

        $.ajax({
          type: 'POST',
          url: url,
          dataType:'json',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData: false,
          success: function(response){
            go_page(url_reload);
            new PNotify({
                title: data.title,
                text: data.text,
                type: data.type
            });
          }
        });

        e.preventDefault();
      });

    })();

	$(function () {
		$(".select2").select2();
	})
</script>